﻿namespace Absolut_Model
{
    public class EventListenerAdapter : IEventListener
    {
        public virtual void OnPointsReset()
        {
            
        }

        public virtual void OnMainFunctionChanged()
        {
           
        }

        public virtual void OnSlicesChanged()
        {
            
        }

        public virtual void OnMethodChanged()
        {
           
        }

        public virtual void OnNextStep()
        {
            
        }

        public virtual void OnPrevStep()
        {
            
        }

        public virtual void OnExperimentAdded()
        {
            
        }

        public virtual void OnExperimentDeleted()
        {
            
        }

        public virtual void OnProblemChanged()
        {
            
        }
    }
}
