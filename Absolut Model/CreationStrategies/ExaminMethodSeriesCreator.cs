﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorithms;
using Functions;

namespace Absolut_Model.CreationStrategies
{
    public class ExaminMethodSeriesCreator : ISeriesCreator
    {
        private string _algNameForFabric = MethodsNames.Agp;
        private string _dllPath;
        private string _configPath;
        private string _examinPath;
       
        private int _count = 100;


        public SeriesExperiment Create()
        {
            List<ISearchAlg> algs = new List<ISearchAlg>(_count);
            string[] strings = Directory.GetFiles(_configPath);

            for (int i = 0; i < _count; i++)
            {
                algs.Add(AlgFactory.BuildExaminAlg(_dllPath, strings[i] , _examinPath));
                algs[i].Problem = ProblemFactory.BuildProblemFromDll(_dllPath, strings[i]);

            }
            return new SeriesExperiment(algs);
        }

        public void UseMethod(string name)
        {
            
        }

        public void UseExamin(string examinExaminPath, string examinDllPath, string examinConfigPath)
        {
            _dllPath = examinDllPath;
            _examinPath = examinExaminPath;
            _configPath = examinConfigPath;

        }

        public void SetCount(int count)
        {
            _count = count;
        }
    }
}
