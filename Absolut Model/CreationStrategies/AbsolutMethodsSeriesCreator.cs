﻿using System;
using System.Collections.Generic;
using Algorithms;
using Functions;

namespace Absolut_Model.CreationStrategies
{
    public class AbsolutMethodsSeriesCreator : ISeriesCreator
    {
        private string _algNameForFabric = MethodsNames.Agp;
        private string _dllPath;
        private string _configPath;

        private bool _useGkls = true;
        private bool _useDll;
        private int _count = 100;


        public SeriesExperiment Create()
        {
            List<ISearchAlg> algs = new List<ISearchAlg>(_count);

            for (int i = 0; i < _count; i++)
            {
                algs.Add(AlgFactory.Build(_algNameForFabric));
                if (_useGkls)
                    algs[i].Problem = GklsProblemsFactory.BuildGKLS(i + 1);
            }

            return new SeriesExperiment(algs);
        }

        public void UseMethod(string name)
        {
            _algNameForFabric = name;
        }

        public void UseExamin(string examinExaminPath, string examinDllPath, string examinConfigPath)
        {
            
        }

        public void UseGkls()
        {
            _useGkls = true;
        }

        public void SetCount(int expCount)
        {
            _count = expCount;
        }

        public void UseDll()
        {
            _useDll = true;
        }
    }
}