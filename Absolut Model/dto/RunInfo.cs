using System;

namespace Absolut_Model
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    public class RunInfo
    {
        [Display(Name = "Run")]
        public DateTime RunDate { get; set; }

        
        public string AlgName
        {
            get => _algName;
            set => _algName = value;
        }
       
        public bool WasMinimumReached
        {
            get => _wasMinimumReached;
            set => _wasMinimumReached = value;
        }
      
        public int? IterToMin
        {
            get => _iterToMin;
            set => _iterToMin =  value;
        }

        private String _algName;
        private bool _wasMinimumReached;
        private int? _iterToMin;
        private TimeSpan _elapsedTime;
        
        public TimeSpan ElapsedTime
        {
            get => _elapsedTime;
            set => _elapsedTime = value;
        }
    }
}