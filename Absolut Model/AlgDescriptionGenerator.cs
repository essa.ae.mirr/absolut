using System;
using System.Text;
using Algorithms;
using Algorithms.Methods;

namespace Absolut_Model
{
    public class AlgDescriptionGenerator
    {
        public static String generate(ISearchAlg alg)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("Current algorithm: " + alg.Name);
            builder.AppendLine("Iterations performed: " + alg.IterationsPerformed);
            builder.AppendLine("Problem: " + alg.Problem.Name);
            builder.AppendLine("Dimension: " + alg.Problem.Dimension);
            builder.AppendLine("Criteria count: " + alg.Problem.NumberOfCriterions);
            builder.AppendLine("Constraints count: " + alg.Problem.NumberOfConstraints);
            var min = alg.Problem.OptimalValue.HasValue ?  alg.Problem.OptimalValue.Value.ToString() : "none";
            builder.AppendLine("Known minimum: " + min);
            return builder.ToString();

        }
    }
}