﻿namespace Absolut_Model
{
    public interface IEventListener
    {
        void OnPointsReset();
        void OnSlicesChanged();
        void OnMethodChanged();
        void OnNextStep();
        void OnPrevStep();
        void OnExperimentAdded();
        void OnExperimentDeleted();
        void OnProblemChanged();
        
    }
}