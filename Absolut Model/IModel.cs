﻿using System;
using System.Collections.Generic;
using Algorithms;
using Algorithms.Methods;
using Functions;
using Properties;

namespace Absolut_Model
{
    public interface IModel
    {
        void Subscribe(IEventListener e);
        Dictionary<Guid, string> GetExperimentsNames();
        MethodPoint GetLastPoint(Guid experimentId);
        Guid AddExperiment(string name, ISearchAlg alg);
        void DeleteExperiment(Guid experimentId);
        void SubscribeExperiment(IEventListener e, Guid experimentId);
        ISearchAlg GetAlg(Guid experimentId); 
        IProblem GetProblem(Guid experimentId);
        void SetProblem(IProblem problem, Guid experimentId);
        void SetAlg(Guid experimentId, ISearchAlg alg);
        void NextStep(Guid experimentId);
        void PrevStep(Guid experimentId);
        object GetProperty(string name, Guid experimentId);
        List<PropertyInfo> GetPropsNames(Guid experimentId);
        MethodPoint GetBestPoint(Guid experimentId);
        int GetCurrentInter(Guid experimentId);
        void SetProperties(Guid experimentId, Dictionary<string, object> propsValues);
        bool HasNext(Guid experimentId);
        void SetSlicesDictionary(Dictionary<int, IFunction> slices, Guid expId);
        Dictionary<int, IFunction> GetSlicesDictionary(Guid expId);


        void ResetExperiment(Guid expId);
        List<RunInfo> GetHistory();
    }
}