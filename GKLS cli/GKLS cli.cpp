// This is the main DLL file.

#include "stdafx.h"

#include "GKLS cli.h"
using namespace GKLScli;
GKLSwrap::GKLSwrap()
{
    functionClass = new GKLSFunction();
    SetDefaultParameters();
}
GKLSwrap::~GKLSwrap()
{
    delete functionClass;
}

int GKLSwrap::SetFunctionNumber(int number)
{
    return functionClass->SetFunctionNumber(number);
}
int GKLSwrap::GetFunctionNumber()
{
    return functionClass->GetFunctionNumber();
}

unsigned GKLSwrap::GetDimension()
{
    return functionClass->GetDimension();
}
void GKLSwrap::SetDimension(unsigned value)
{
    functionClass->SetDimension(value);
}

double GKLSwrap::GetOptimalValue()
{
    return functionClass->GetOptimalValue();
}

void GKLSwrap::SetOptimalValue(double value)
{
    functionClass->SetOptimalValue(value);
}

unsigned GKLSwrap::GetNumberOfLocalMinima()
{
    return functionClass->GetNumberOfLocalMinima();
}

void GKLSwrap::SetNumberOfLocalMinima(unsigned value)
{
    functionClass->SetNumberOfLocalMinima(value);
}

double GKLSwrap::GetGlobalDistance()
{
    return functionClass->GetGlobalDistance();
}
void GKLSwrap::SetGlobalDistance(double value)
{
    functionClass->SetGlobalDistance(value);
}

double GKLSwrap::GetGlobalRadius()
{
    return functionClass->GetGlobalRadius();
}
void GKLSwrap::SetGlobaRadius(double value)
{
    functionClass->SetGlobalRadius(value);
}

GKLSFuncionType GKLSwrap::GetFunctionType()
{
    return functionClass->GetType();
}
void GKLSwrap::SetFunctionType(GKLSFuncionType value)
{
    functionClass->SetType(value);
}

GKLSParameters GKLSwrap::GetParameters()
{
    return functionClass->GetParameters();
}
void GKLSwrap::SetParameters(GKLSParameters value)
{
    functionClass->SetParameters(value);
}

void GKLSwrap::SetDefaultParameters()
{
    functionClass->SetDefaultParameters();
}
int GKLSwrap::CheckParameters()
{
    return functionClass->CheckParameters();
}
void GKLSwrap::SetFunctionClass(GKLSClass type, unsigned dimension)
{
    functionClass->SetFunctionClass(type, dimension);
}

double GKLSwrap::Calculate(array<double>  ^ argmin)
{

    pin_ptr<double> p = &argmin[0];
    return   functionClass->Calculate(p);
}

double GKLSwrap::CalculateNDFunction(array<double>  ^ argmin)
{
    pin_ptr<double> p = &argmin[0];
    return functionClass->CalculateNDFunction(p);
}
double GKLSwrap::CalculateDFunction(array<double>  ^ argmin)
{
    pin_ptr<double> p = &argmin[0];
    return functionClass->CalculateDFunction(p);
}
double GKLSwrap::CalculateD2Function(array<double>  ^ argmin)
{
    pin_ptr<double> p = &argmin[0];
    return functionClass->CalculateD2Function(p);
}

double GKLSwrap::CalculateDFunctionDeriv(unsigned var_j, array<double>  ^ argmin)
{
    pin_ptr<double> p = &argmin[0];
    return functionClass->CalculateDFunctionDeriv(var_j, p);
}
double GKLSwrap::CalculateD2FunctionDeriv1(unsigned var_j, array<double>  ^ argmin)
{
    pin_ptr<double> p = &argmin[0];
    return functionClass->CalculateD2FunctionDeriv1(var_j, p);
}
double GKLSwrap::CalculateD2FunctionDeriv2(unsigned var_j, unsigned var_k, const array<double>  ^ argmin)
{
    pin_ptr<double> p = &argmin[0];
    return functionClass->CalculateD2FunctionDeriv2(var_j, var_k, p);
}


int GKLSwrap::CalculateDFunctionGradient(const array<double>^ x, array<double>^ g)
{
    pin_ptr<double> p = &x[0];
    pin_ptr<double> p1 = &g[0];
    return  functionClass->CalculateDFunctionGradient(p, p1);
}
int GKLSwrap::CalculateD2FunctionGradient(const array<double>^ x, array<double>^ g)
{
    pin_ptr<double> p = &x[0];
    pin_ptr<double> p1 = &g[0];
    return  functionClass->CalculateDFunctionGradient(p, p1);
}

int GKLSwrap::CalculateD2FunctionHessian(const array<double>^ x, array<double, 2> ^  h)
{
    pin_ptr<double> p = &x[0];
    int dimOuter = h->GetLength(0);
    int dimInner = h->GetLength(1);

    //This pins the *entire* array!
    double **ptr = (double**)malloc(sizeof(double*) * dimOuter);


    for (int i = 0; i < dimOuter; i++)
    {
        pin_ptr<double> pinned = &h[i, 0];

        double *basePtr = pinned;
        ptr[i] = basePtr;
        basePtr += dimInner;
    }


    return  functionClass->CalculateD2FunctionHessian(p, ptr);
}

int GKLSwrap::GetOptimumCoordinates(array<double>  ^ argmin)
{
    pin_ptr<double> p = &argmin[0];
    return functionClass->GetOptimumCoordinates(p);

}
void GKLSwrap::GetDomainBounds(array<double>  ^ lowerBound, array<double>  ^ upperBound)
{
    pin_ptr<double> lb = &lowerBound[0];
    pin_ptr<double> ub = &upperBound[0];
    functionClass->GetDomainBounds(lb, ub);
}

int GKLSwrap::SetGlobalMinimumPoint(array<double>  ^ argmin)
{
    pin_ptr<double> p = &argmin[0];
    return functionClass->SetGlobalMinimumPoint(p);
}

