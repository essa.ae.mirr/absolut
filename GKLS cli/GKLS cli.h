// GKLS cli.h

#pragma once
#include "gkls_function.h"
#include "stdlib.h"
using namespace System;
using namespace gkls;
namespace GKLScli {

    public ref struct GKLSwrapParameters
    {
        private:
        GKLSParameters * params;
        public:
        unsigned dimension;
        double globalMinimumValue;
        unsigned numberOfLocalMinima;
        double globalDistance;
        double globalRadius;
        GKLSFuncionType type;

        GKLSwrapParameters() {}
    GKLSwrapParameters(unsigned _dimension, double _globalMinimumValue,
        unsigned _numberOfLocalMinima, double _globalDistance,
        double _globalRadius, GKLSFuncionType _type) :
    dimension(_dimension),
    globalMinimumValue(_globalMinimumValue),
    numberOfLocalMinima(_numberOfLocalMinima),
    globalDistance(_globalDistance),
    globalRadius(_globalRadius),
    type(_type)
    {}

    };
    public ref class GKLSwrap
    {
        private:
        GKLSFunction * functionClass;
        public:
            GKLSwrap();
        
            ~GKLSwrap();
        

            int SetFunctionNumber(int number);
       
            int GetFunctionNumber();
        

            unsigned GetDimension();
       
            void SetDimension(unsigned value);
        

            double GetOptimalValue();
        

            void SetOptimalValue(double value);
        

            unsigned GetNumberOfLocalMinima();
        

            void SetNumberOfLocalMinima(unsigned value);
        

            double GetGlobalDistance();
        
            void SetGlobalDistance(double value);
        

            double GetGlobalRadius();
        
            void SetGlobaRadius(double value);
       
            GKLSFuncionType GetFunctionType();

            void SetFunctionType(GKLSFuncionType value);
        

        GKLSParameters GetParameters();
       
        void SetParameters(GKLSParameters value);
        

        void SetDefaultParameters();
       
        int CheckParameters();
        
        void SetFunctionClass(GKLSClass type, unsigned dimension);
        

        double Calculate(array<double>  ^ argmin);
        
        double CalculateNDFunction(array<double>  ^ argmin);
        
        double CalculateDFunction(array<double>  ^ argmin);
        
        double CalculateD2Function(array<double>  ^ argmin);
        

        double CalculateDFunctionDeriv(unsigned var_j, array<double>  ^ argmin);
       
        double CalculateD2FunctionDeriv1(unsigned var_j, array<double>  ^ argmin);
       
        double CalculateD2FunctionDeriv2(unsigned var_j, unsigned var_k, const array<double>  ^ argmin);
        


        int CalculateDFunctionGradient(const array<double>^ x, array<double>^ g);
        
        int CalculateD2FunctionGradient(const array<double>^ x, array<double>^ g);
        
        int CalculateD2FunctionHessian(const array<double>^ x, array<double, 2> ^  h);
        

        int GetOptimumCoordinates(array<double>  ^ argmin);
        
        void GetDomainBounds(array<double>  ^ lowerBound, array<double>  ^ upperBound);
        

        int SetGlobalMinimumPoint(array<double>  ^ argmin);
      

        };
        }
