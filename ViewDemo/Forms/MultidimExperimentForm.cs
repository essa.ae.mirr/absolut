﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using Absolut_Model;
using Algorithms;
using Algorithms.Methods;
using Functions;
using Functions.Functions;
using Graphics;
using ViewDemo.Presenters;
using ViewDemo.Views;
using ParametersDialog = ViewDemo.Dialogs.ParametersDialog;

namespace ViewDemo.Forms
{
    public partial class MultidimExperimentForm : Form, IProblemaView, ISearchInformationView, IBestPointView
    {
        private ProblemaPresenter _p;
        private SearchInformationPresenter _expPresenter;
        private BestPointsPresenter _bp;
        private IProblem _problem;
        private Dictionary<string, SliceEditorForm> _childForms = new Dictionary<string, SliceEditorForm>();
        private bool _is2d = false;
        private List<MethodPoint> _points = new List<MethodPoint>();
        private Guid _expId;
        private MethodPresenter _methodPresenter;

        public MultidimExperimentForm(Guid id)
        {
            InitializeComponent();
            _expId = id;

            _p = new ProblemaPresenter(this);
            _expPresenter = new SearchInformationPresenter(this);
            _p.ExpId = id;
            _expPresenter.ExpId = id;
            _bp = new BestPointsPresenter(id);
            _bp.AddView(this);
        }


        public void SetProblem(IProblem problem)
        {
            RestoreInitialState();
            _problem = problem;

            var i = 0;
            FillDefaultFunctionalsToolStripItems();
            FillProblemMenuItems(problem, i);
            InitDataGrid(problem);
        }

        private void InitDataGrid(IProblem problem)
        {
            for (var j = 0; j < problem.Dimension; j++)
                dataGridView2.Columns.Add("x" + j, "x" + j);

            dataGridView2.Columns.Add("y", "y");
            dataGridView2.Columns.Add("i", "i");
        }

        private void FillProblemMenuItems(IProblem problem, int i)
        {
            if (problem.Constraints.Count != 0)
            {
                var showAfterConstraints = new ToolStripMenuItem()
                {
                    Text = "Criteria After Costraint"
                };
                showAfterConstraints.Click += (sender, args) =>
                {
                    var item = sender as ToolStripMenuItem;
                    var functionalName = item.Text;
                    var sliceEditorForm =
                        new SliceEditorForm(_problem) {MdiParent = this, Text = functionalName, Mode2D = _is2d};
                    _childForms[functionalName] = sliceEditorForm;
                    sliceEditorForm.Disposed += (sender1, args1) => { item.Checked = false; };
                    _childForms[functionalName].Show();
                };
                functionalsMenuButton.DropDownItems.Add(showAfterConstraints);
            }

            foreach (var p in problem.Constraints)
            {
                var functionalName = p.Name == null ? "Constraint " + i : p.Name;
                var toolStripMenuItem = BuildMenuItem(functionalName, i, false);
                functionalsMenuButton.DropDownItems.Add(toolStripMenuItem);
                i++;
            }

            var k = 0;
            foreach (var p in problem.Criterions)
            {
                var functionalName = p.Name == null ? "Criteria " + k : p.Name;
                var toolStripMenuItem = BuildMenuItem(functionalName, i, true);
                functionalsMenuButton.DropDownItems.Add(toolStripMenuItem);
                toolStripMenuItem.Checked = true;
                i++;
                k++;
            }
        }

        private void FillDefaultFunctionalsToolStripItems()
        {
            var showAllItem = new ToolStripMenuItem()
            {
                Text = "Show All Functionals",
                CheckOnClick = false
            };
            showAllItem.Click += ShowAllItemOnClick;
            var hideAllItem = new ToolStripMenuItem()
            {
                Text = "Hide All Functionals",
                CheckOnClick = false
            };
            hideAllItem.Click += HideAllItemOnClick;
            functionalsMenuButton.DropDownItems.Add(showAllItem);
            functionalsMenuButton.DropDownItems.Add(hideAllItem);
        }

        private void HideAllItemOnClick(object o, EventArgs eventArgs)
        {
            foreach (var child in _childForms) child.Value.Hide();

            foreach (ToolStripMenuItem dropDownItem in functionalsMenuButton.DropDownItems)
                if (dropDownItem.CheckOnClick)
                    dropDownItem.Checked = false;
        }

        private void ShowAllItemOnClick(object o, EventArgs eventArgs)
        {
            foreach (var child in _childForms) child.Value.Show();
            foreach (ToolStripMenuItem dropDownItem in functionalsMenuButton.DropDownItems)
                if (dropDownItem.CheckOnClick)
                    dropDownItem.Checked = true;
        }

        private ToolStripMenuItem BuildMenuItem(string functionalName, int i, bool drawPoints)
        {
            var toolStripMenuItem = new ToolStripMenuItem()
            {
                Text = functionalName,
                CheckOnClick = true
            };

            SliceEditorForm CreateSliceEditorForm()
            {
                var sliceEditorForm = new SliceEditorForm(_problem.Functionals[i])
                {
                    MdiParent = this,
                    Text = functionalName,
                    Mode2D = _is2d,
                    NeedToShowPoint = drawPoints
                };
                _childForms[functionalName] = sliceEditorForm;
                sliceEditorForm.Disposed += (sender, args) => { toolStripMenuItem.Checked = false; };
                return sliceEditorForm;
            }


            toolStripMenuItem.CheckedChanged += (sender, args) =>
            {
                var item = sender as ToolStripMenuItem;
                if (item.Checked)
                {
                    if (!_childForms.ContainsKey(item.Text)) CreateSliceEditorForm();
                    if (_childForms[item.Text].IsDisposed) CreateSliceEditorForm();

                    _childForms[item.Text].Show();
                }


                else
                {
                    _childForms[item.Text].Hide();
                }
            };
            return toolStripMenuItem;
        }

        private void RestoreInitialState()
        {
            foreach (var child in MdiChildren) child.Close();

            _childForms.Clear();
            functionalsMenuButton.DropDownItems.Clear();
            dataGridView2.Columns.Clear();
            dataGridView2.Rows.Clear();
        }

        public void ClearPoints()
        {
            dataGridView2.Rows.Clear();
        }

        public void AddPoint(MethodPoint p)
        {
            dataGridView2.Rows.Add();
            for (var i = 0; i < p.X.Count; i++)
                dataGridView2["x" + i, dataGridView2.RowCount - 1].Value = Math.Round(p.X[i], 5);

            dataGridView2["i", dataGridView2.RowCount - 1].Value = p.Iteration;
            dataGridView2["y", dataGridView2.RowCount - 1].Value = Math.Round(p.Criterions.FirstOrDefault(), 5);
            foreach (var childForm in _childForms)
            {
                childForm.Value.AddPoint(p);
            }
        }

        public void DeleteLastPoint(MethodPoint p)
        {
            dataGridView2.Rows.RemoveAt(dataGridView2.RowCount - 1);
            foreach (var childForm in _childForms)
            {
                childForm.Value.DeleteLastPoint(p);
            }
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void mosaicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalTilesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void iconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {
            pointsTimer.Start();
        }

        public void UpdateBestPoint(MethodPoint bestPoint)
        {
            if (bestPoint == null) return;

            dataGridView2.Rows[bestPoint.Iteration].DefaultCellStyle.BackColor = Color.Gold;
        }

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
            foreach (var childForm in _childForms)
            {
                childForm.Value.SetBounds(dims, min, max, center);
            }
        }

        private void pointsTimer_Tick(object sender, EventArgs e)
        {
            _expPresenter.Next();
        }

        private void toolStripLabel2_Click(object sender, EventArgs e)
        {
            pointsTimer.Stop();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            _expPresenter.Prev();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            _expPresenter.Next();
        }

        private void dViewToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            var item = sender as ToolStripMenuItem;
            _is2d = item.Checked;
            foreach (var sliceEditorForm in _childForms)
                sliceEditorForm.Value.Mode2D = item.Checked;

            ;
        }

        private void changeMethodToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var methodChoose = new MethodChooseDialog(AlgFactory.Options))
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
             
                _methodPresenter.SetMethodWithOldFunction(methodChoose.ChoosedOption);
            }
        }

        private void methodParametersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var values = _methodPresenter.GetParamsInfo().ToDictionary(i => i, i => _methodPresenter.GetProperty(i.name));
            using (var paramsChoose = new ParametersDialog(values))
            {
                var dr = paramsChoose.ShowDialog();
                if (dr == DialogResult.OK)
                    _methodPresenter.SetProperties(paramsChoose.changedValues);
            }
        }

        private void notConstrainedToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var methodChoose = new MethodChooseDialog(ProblemFactory.NotConstrainedOptionsList))
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
                _methodPresenter.SetMainFunction(methodChoose.ChoosedOption);
            }
        }

        private void dLLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            using (var methodChoose = new DLLSettings())
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
                using (var xml = new OpenFileDialog() { Title = "Open .dll file" })
                {
                   
                  try
                    {
                        _methodPresenter.SetMainFunction(methodChoose.dllPath, methodChoose.configPath);
                    }
                    catch (Exception exception)
                    {
                        Console.Write(exception);
                        MessageBox.Show("Cant open dll");
                    }
                }
            }
        }
    }
}