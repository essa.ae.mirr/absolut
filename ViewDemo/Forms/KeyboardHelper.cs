﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Graphics;

namespace ViewDemo
{
    using System.Diagnostics;
    using System.Runtime.InteropServices;

    public class KeyboardHelper
    {
        private double _lastUpdateTime;
        public List<DrawablesControlThreading> Drawables { get; set; }

        public KeyboardHelper()
        {
        }

        public KeyboardHelper(List<DrawablesControlThreading> d)
        {
            Drawables = d;
        }

        public static bool ApplicationIsActivated()
        {
            var activatedHandle = GetForegroundWindow();
            if (activatedHandle == IntPtr.Zero)
            {
                return false;       // No window is currently activated
            }

            var procId = Process.GetCurrentProcess().Id;
            int activeProcId;
            GetWindowThreadProcessId(activatedHandle, out activeProcId);

            return activeProcId == procId;
        }


        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowThreadProcessId(IntPtr handle, out int processId);

        public void KeyboardCheckerOnTick(object sender, EventArgs eventArgs)
        {
            if(!ApplicationIsActivated()) return;
            var currentTime = Environment.TickCount / 1000.0;
            var delta = currentTime - _lastUpdateTime;
            _lastUpdateTime = currentTime;
            const double RotationSpeed = Math.PI;

            if (Keyboard.IsKeyDown(Key.W))
                foreach (var control in Drawables)
                {
                    control.Camera.RotateAroundX(RotationSpeed * delta);
                    control.NeedRedraw = true;
                }

            if (Keyboard.IsKeyDown(Key.A))
                foreach (var control in Drawables)
                {
                    control.Camera.RotateAroundY(-RotationSpeed * delta);
                    control.NeedRedraw = true;
                }

            if (Keyboard.IsKeyDown(Key.S))
                foreach (var control in Drawables)
                {
                   control.Camera.RotateAroundX(-RotationSpeed * delta);
                    control.NeedRedraw = true;
                }

            if (Keyboard.IsKeyDown(Key.D))
                foreach (var control in Drawables)
                {
                    control.Camera.RotateAroundY(RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
        }

        public void KeyboardCheckerOnKeyDown(object sender, System.Windows.Forms.KeyEventArgs eventArgs)
        {
            if (!ApplicationIsActivated()) return;
            var currentTime = Environment.TickCount / 1000.0;
            var delta = currentTime - _lastUpdateTime;
            _lastUpdateTime = currentTime;
            const double RotationSpeed = Math.PI / 2;

            if (Keyboard.IsKeyDown(Key.W))
                foreach (var control in Drawables)
                {
                    control.Camera.RotateAroundX(RotationSpeed * delta);
                    control.NeedRedraw = true;
                }

            if (Keyboard.IsKeyDown(Key.A))
                foreach (var control in Drawables)
                {
                    control.Camera.RotateAroundY(-RotationSpeed * delta);
                    control.NeedRedraw = true;
                }

            if (Keyboard.IsKeyDown(Key.S))
                foreach (var control in Drawables)
                {
                    control.Camera.RotateAroundX(-RotationSpeed * delta);
                    control.NeedRedraw = true;
                }

            if (Keyboard.IsKeyDown(Key.D))
                foreach (var control in Drawables)
                {
                    control.Camera.RotateAroundY(RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
        }
    }
}