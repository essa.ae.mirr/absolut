﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Input;
using Graphics;
using ViewDemo.Labels;
using ViewDemo.Views;
using ViewDemo.Views.Labels;

namespace ViewDemo.Forms
{
    public partial class DashboardForm : Form
    {
        private readonly List<DrawablesControlThreading> _d = new List<DrawablesControlThreading>();
        private double _lastUpdateTime;

        public DashboardForm()
        {
            InitializeComponent();
            DynamicInitialization();
        }

        private void KeyboardCheckerOnTick(object sender, EventArgs eventArgs)
        {
            var currentTime = Environment.TickCount / 1000.0;
            var delta = currentTime - _lastUpdateTime;
            _lastUpdateTime = currentTime;
            const double RotationSpeed = Math.PI / 2;
            if (Keyboard.IsKeyDown(Key.W))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundX(RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
            if (Keyboard.IsKeyDown(Key.A))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundY(-RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
            if (Keyboard.IsKeyDown(Key.S))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundX(-RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
            if (Keyboard.IsKeyDown(Key.D))
                foreach (var control in _d)
                {
                    control.Camera.RotateAroundY(RotationSpeed * delta);
                    control.NeedRedraw = true;
                }
        }

        private void DashboardForm_Activated(object sender, EventArgs e)
        {
            _keyboardCheckerTimer.Start();
        }

        private void DashboardForm_Deactivate(object sender, EventArgs e)
        {
            _keyboardCheckerTimer.Stop();
        }

        protected void RegisterOpenGLWidget(Control widget)
        {
            var openGLControl = widget as DrawablesControlThreading;
            _d.Add(openGLControl);
        }


        protected void AddWidgetAtLeftUpper(Control widget, string groupBoxName)
        {
            widget.Dock = DockStyle.Fill;
            widget.Location = new Point(3, 16);
            widget.Margin = new Padding(5);
            widget.Size = new Size(366, 248);
            widget.TabIndex = 11;
            leftUpperGroupBox.Controls.Clear();
            leftUpperGroupBox.Controls.Add(widget);
            leftUpperGroupBox.Text = groupBoxName;
            Refresh();
        }
        protected void AddWidgetAtLeftBottom(Control widget, string groupBoxName)
        {
            widget.Dock = DockStyle.Fill;
            widget.Location = new Point(3, 16);
            widget.Margin = new Padding(5);
            widget.Size = new Size(366, 250);
            widget.TabIndex = 2;
            leftBottomGroupBox.Controls.Clear();
            leftBottomGroupBox.Controls.Add(widget);
            leftBottomGroupBox.Text = groupBoxName;
            Refresh();
        }

        protected void AddWidgetAtRightUpper(Control widget, string groupBoxName)
        {

            widget.Dock = DockStyle.Fill;
            widget.Location = new Point(3, 16);
            widget.Margin = new Padding(5);
            widget.Size = new Size(367, 248);
            widget.TabIndex = 2;
            rightUpperGroupBox.Controls.Clear();
            leftBottomGroupBox.Controls.Add(widget);
            leftBottomGroupBox.Text = groupBoxName;
            Refresh();
        }

        private void DynamicInitialization()
        {
            components = new Container();
            timer2 = new Timer(components);
            _keyboardChecker = new Timer(components);
            problemInfoGroupBox = new GroupBox();
            bestPointLabel2 = new BestPointLabel();
            currentPointLabel1 = new CurrentPointLabel();
            bestPointLabel1 = new BestPointLabel();
            currentIterationLabel1 = new CurrentIterationLabel();
            iter = new Label();
            Navigation = new GroupBox();
            navigationFlowLayoutPanel = new FlowLayoutPanel();
            backButton = new Button();
            startButton = new Button();
            pauseButton = new Button();
            forwardButton = new Button();
            rightUpperGroupBox = new GroupBox();
            leftBottomGroupBox = new GroupBox();
            tableLayoutPanel1 = new TableLayoutPanel();
            leftUpperGroupBox = new GroupBox();
            menuStrip1 = new MenuStrip();
            functionToolStripMenuItem = new ToolStripMenuItem();
            problemInfoGroupBox.SuspendLayout();
            Navigation.SuspendLayout();
            navigationFlowLayoutPanel.SuspendLayout();
            rightUpperGroupBox.SuspendLayout();
            leftBottomGroupBox.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            leftUpperGroupBox.SuspendLayout();
            menuStrip1.SuspendLayout();
            SuspendLayout();
            //
            // timer2
            //
            timer2.Interval = 25;
           // this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            //
            // _keyboardChecker
            //
            _keyboardChecker.Interval = 32;
            //
            // groupBox1
            //
            problemInfoGroupBox.AutoSize = true;
            problemInfoGroupBox.Controls.Add(bestPointLabel2);
            problemInfoGroupBox.Controls.Add(currentPointLabel1);
            problemInfoGroupBox.Controls.Add(bestPointLabel1);
            problemInfoGroupBox.Controls.Add(currentIterationLabel1);
            problemInfoGroupBox.Controls.Add(iter);
            problemInfoGroupBox.Dock = DockStyle.Fill;
            problemInfoGroupBox.Location = new Point(381, 315);
            problemInfoGroupBox.Name = "groupBox1";
            problemInfoGroupBox.Size = new Size(373, 130);
            problemInfoGroupBox.TabIndex = 10;
            problemInfoGroupBox.TabStop = false;
            problemInfoGroupBox.Text = "Problem info";
            //
            // bestPointLabel2
            //
            bestPointLabel2.AutoSize = true;
            bestPointLabel2.Location = new Point(8, 69);
            bestPointLabel2.Name = "bestPointLabel2";
            bestPointLabel2.Size = new Size(76, 17);
            bestPointLabel2.TabIndex = 9;
            //
            // currentPointLabel1
            //
            currentPointLabel1.AutoSize = true;
            currentPointLabel1.Location = new Point(8, 45);
            currentPointLabel1.Margin = new Padding(2, 2, 2, 2);
            currentPointLabel1.Name = "currentPointLabel1";
            currentPointLabel1.Size = new Size(88, 19);
            currentPointLabel1.TabIndex = 8;
            //
            // bestPointLabel1
            //
            bestPointLabel1.AutoSize = true;
            bestPointLabel1.Location = new Point(7, 38);
            bestPointLabel1.Margin = new Padding(4);
            bestPointLabel1.Name = "bestPointLabel1";
            bestPointLabel1.Size = new Size(79, 0);
            bestPointLabel1.TabIndex = 6;
            //
            // currentIterationLabel1
            //
            currentIterationLabel1.AutoSize = true;
            currentIterationLabel1.Location = new Point(7, 20);
            currentIterationLabel1.Margin = new Padding(4);
            currentIterationLabel1.Name = "currentIterationLabel1";
            currentIterationLabel1.Size = new Size(118, 21);
            currentIterationLabel1.TabIndex = 5;
            //
            // iter
            //
            iter.AutoSize = true;
            iter.Location = new Point(67, 18);
            iter.Name = "iter";
            iter.Size = new Size(0, 13);
            iter.TabIndex = 4;
            //
            // Navigation
            //
            Navigation.Controls.Add(navigationFlowLayoutPanel);
            Navigation.Dock = DockStyle.Fill;
            Navigation.Location = new Point(381, 451);
            Navigation.Name = "Navigation";
            Navigation.Size = new Size(373, 133);
            Navigation.TabIndex = 9;
            Navigation.TabStop = false;
            Navigation.Text = "Navigation";
            //
            // flowLayoutPanel1
            //
            navigationFlowLayoutPanel.Controls.Add(backButton);
            navigationFlowLayoutPanel.Controls.Add(startButton);
            navigationFlowLayoutPanel.Controls.Add(pauseButton);
            navigationFlowLayoutPanel.Controls.Add(forwardButton);
            navigationFlowLayoutPanel.Dock = DockStyle.Fill;
            navigationFlowLayoutPanel.Location = new Point(3, 16);
            navigationFlowLayoutPanel.Name = "flowLayoutPanel1";
            navigationFlowLayoutPanel.Size = new Size(367, 114);
            navigationFlowLayoutPanel.TabIndex = 5;
            //
            // button1
            //
            backButton.Anchor = AnchorStyles.Left;
            backButton.Location = new Point(3, 3);
            backButton.Name = "button1";
            backButton.Size = new Size(75, 23);
            backButton.TabIndex = 1;
            backButton.Text = "<<";
            backButton.UseVisualStyleBackColor = true;
           // this.button1.Click += new System.EventHandler(this.button1_Click);
            //
            // button3
            //
            startButton.Anchor = AnchorStyles.Left;
            startButton.Location = new Point(84, 3);
            startButton.Name = "button3";
            startButton.Size = new Size(75, 23);
            startButton.TabIndex = 3;
            startButton.Text = "start";
            startButton.UseVisualStyleBackColor = true;
           // this.button3.Click += new System.EventHandler(this.button3_Click);
            //
            // button4
            //
            pauseButton.Anchor = AnchorStyles.Left;
            pauseButton.Location = new Point(165, 3);
            pauseButton.Name = "button4";
            pauseButton.Size = new Size(75, 23);
            pauseButton.TabIndex = 4;
            pauseButton.Text = "pause";
            pauseButton.UseVisualStyleBackColor = true;
          //  this.button4.Click += new System.EventHandler(this.button4_Click);
            //
            // button2
            //
            forwardButton.Anchor = AnchorStyles.Left;
            forwardButton.Location = new Point(246, 3);
            forwardButton.Name = "button2";
            forwardButton.Size = new Size(75, 23);
            forwardButton.TabIndex = 2;
            forwardButton.Text = ">>";
            forwardButton.UseVisualStyleBackColor = true;
           // this.button2.Click += new System.EventHandler(this.button2_Click);
            //
            // groupBox4
            //
            rightUpperGroupBox.BackColor = SystemColors.Control;
            rightUpperGroupBox.Dock = DockStyle.Fill;
            rightUpperGroupBox.Location = new Point(381, 42);
            rightUpperGroupBox.Name = "groupBox4";
            rightUpperGroupBox.Size = new Size(373, 267);
            rightUpperGroupBox.TabIndex = 10;
            rightUpperGroupBox.TabStop = false;

            //
            // groupBox3
            //
            leftBottomGroupBox.Dock = DockStyle.Fill;
            leftBottomGroupBox.Location = new Point(3, 315);
            leftBottomGroupBox.Name = "groupBox3";
            tableLayoutPanel1.SetRowSpan(leftBottomGroupBox, 2);
            leftBottomGroupBox.Size = new Size(372, 269);
            leftBottomGroupBox.TabIndex = 3;
            leftBottomGroupBox.TabStop = false;
            //
            // tableLayoutPanel1
            //
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Controls.Add(leftBottomGroupBox, 0, 2);
            tableLayoutPanel1.Controls.Add(leftUpperGroupBox, 0, 0);
            tableLayoutPanel1.Controls.Add(rightUpperGroupBox, 1, 0);
            tableLayoutPanel1.Controls.Add(Navigation, 1, 3);
            tableLayoutPanel1.Controls.Add(menuStrip1, 0, 0);
            tableLayoutPanel1.Controls.Add(problemInfoGroupBox, 1, 2);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 4;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 6.667073F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 46.66646F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 23.33323F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 23.33323F));
            tableLayoutPanel1.Size = new Size(757, 587);
            tableLayoutPanel1.TabIndex = 11;
            //
            // groupBox2
            //
            leftUpperGroupBox.BackColor = SystemColors.Control;
            leftUpperGroupBox.Dock = DockStyle.Fill;
            leftUpperGroupBox.Location = new Point(3, 42);
            leftUpperGroupBox.Name = "groupBox2";
            leftUpperGroupBox.Size = new Size(372, 267);
            leftUpperGroupBox.TabIndex = 12;
            leftUpperGroupBox.TabStop = false;
            leftUpperGroupBox.Text = "Criteria surface";
            //
            // menuStrip1
            //
            menuStrip1.AllowMerge = false;
            tableLayoutPanel1.SetColumnSpan(menuStrip1, 2);
            menuStrip1.Items.AddRange(new ToolStripItem[] {
            functionToolStripMenuItem});
            menuStrip1.Location = new Point(0, 0);
            menuStrip1.Name = "menuStrip1";
            menuStrip1.Size = new Size(757, 24);
            menuStrip1.TabIndex = 9;
            menuStrip1.Text = "menuStrip1";
            //
            // functionToolStripMenuItem
            //
            functionToolStripMenuItem.Name = "functionToolStripMenuItem";
            functionToolStripMenuItem.Size = new Size(66, 20);
            functionToolStripMenuItem.Text = "Function";
           // this.functionToolStripMenuItem.Click += new System.EventHandler(this.functionToolStripMenuItem_Click);
            //
            // ExperimentForm
            //
            AutoScaleDimensions = new SizeF(6F, 13F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(757, 587);
            Controls.Add(tableLayoutPanel1);
            MainMenuStrip = menuStrip1;
            Name = "ExperimentForm";
            Text = "DemoVersion";
            //this.Deactivate += new System.EventHandler(this.DemoVersion_Deactivate);
            //this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DemoVersion_FormClosing);
            //this.Leave += new System.EventHandler(this.DemoVersion_Leave);
            problemInfoGroupBox.ResumeLayout(false);
            problemInfoGroupBox.PerformLayout();
            Navigation.ResumeLayout(false);
            navigationFlowLayoutPanel.ResumeLayout(false);
            rightUpperGroupBox.ResumeLayout(false);
            leftBottomGroupBox.ResumeLayout(false);
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            leftUpperGroupBox.ResumeLayout(false);
            menuStrip1.ResumeLayout(false);
            menuStrip1.PerformLayout();
            ResumeLayout(false);

        }


        protected Timer timer2;
        protected Timer _keyboardChecker;
        protected GroupBox problemInfoGroupBox;
        protected BestPointLabel bestPointLabel2;
        protected CurrentPointLabel currentPointLabel1;
        protected BestPointLabel bestPointLabel1;
        protected CurrentIterationLabel currentIterationLabel1;
        protected Label iter;
        protected TableLayoutPanel tableLayoutPanel1;
        protected GroupBox leftBottomGroupBox;
        protected GroupBox rightUpperGroupBox;
        protected GroupBox Navigation;
        protected FlowLayoutPanel navigationFlowLayoutPanel;
        protected Button backButton;
        protected Button startButton;
        protected Button pauseButton;
        protected Button forwardButton;
        protected GroupBox leftUpperGroupBox;
        protected MenuStrip menuStrip1;
        protected ToolStripMenuItem functionToolStripMenuItem;
    }
}
