﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using Algorithms;
using Algorithms.Methods;
using Functions;
using Functions.Functions;
using Graphics;
using OpenTK;
using Properties;

namespace ViewDemo
{
    public partial class SliceEditorForm : Form
    {
        private readonly int _criteriaIndex;
        private readonly Dictionary<string, int> variablesNames = new Dictionary<string, int>();
        private int _freeVar1;
        private int _freeVar2;
        private IFunction _functionToSlice;
        private Dictionary<int, double> _fixedVars = new Dictionary<int, double>();
        private bool _isFreeVariablesInputedCorrectly;
        public Dictionary<int, IFunction> SlicePlanes;
        private IProblem _problemToSlice;
        private double[] _center;

        public bool NeedToShowPoint { get; set; } = true;

        private bool _isBoundsSet;
        private double[] _max;
        private double[] _min;
        private int _gridSize = 64;
        private readonly KeyboardHelper _keyboardHelper;

        public SliceEditorForm()
        {
            _keyboardHelper = new KeyboardHelper();
            InitializeComponent();
            FunctionToSlice = new Squares();
            _keyboardHelper.Drawables = new List<DrawablesControlThreading>() {customProblem3dPlot1};
            timer1.Start();
        }

        public bool Mode2D
        {
            get => customProblem3dPlot1.Mode2d;
            set
            {
                customProblem3dPlot1.Mode2d = value;
                customProblem3dPlot1.NeedRedraw = true;
            }
        }

        public bool Isobars
        {
            get => this.customProblem3dPlot1.Isobars;
            set
            {
                customProblem3dPlot1.Isobars = value;
                customProblem3dPlot1.NeedRedraw = true;
            }
        }

        public SliceEditorForm(IFunction functionToSlice, int criteriaIndex)
        {
            _criteriaIndex = criteriaIndex;
            _keyboardHelper = new KeyboardHelper();
            InitializeComponent();
            FunctionToSlice = functionToSlice;
            _keyboardHelper.Drawables = new List<DrawablesControlThreading>() {customProblem3dPlot1};
            timer1.Start();
        }

        public SliceEditorForm(IFunction functionToSlice)
        {
           
            _keyboardHelper = new KeyboardHelper();
            InitializeComponent();
            FunctionToSlice = functionToSlice;
            _keyboardHelper.Drawables = new List<DrawablesControlThreading>() { customProblem3dPlot1 };
            timer1.Start();
        }

        public SliceEditorForm(IProblem problemToSlice)
        {
            InitializeComponent();
            ProblemToSlice = problemToSlice;
            var d = new List<DrawablesControlThreading>() {customProblem3dPlot1};
            _keyboardHelper = new KeyboardHelper(d);
            timer1.Start();
        }

        public SliceEditorForm(IProblem problemToSlice, int criteriaIndex)
        {
            InitializeComponent();
            this.customProblem3dPlot1.CriteriaIndex = criteriaIndex;
            ProblemToSlice = problemToSlice;
            var d = new List<DrawablesControlThreading>() { customProblem3dPlot1 };
            _keyboardHelper = new KeyboardHelper(d);
            timer1.Start();
        }

        public IFunction FunctionToSlice
        {
            get => _functionToSlice;
            set
            {
                _functionToSlice = value;
                if (value.Left.Count == 2)
                {
                    InitForTwoDimensionalFunction(value);
                }

                else
                {
                    UpdateFreeVarsControls();
                }
            }
        }

        private void InitForTwoDimensionalFunction(IFunction value)
        {
            var newFunction = value;
            double[] x = Utils.FillCoordArrays(newFunction, 0, _gridSize),
                y = Utils.FillCoordArrays(newFunction, 1, _gridSize);
            var z = Utils.FillValues(newFunction, _gridSize);
            Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, newFunction);
            double[] min, max, center;
            Utils.GetInfo(x, y, z, out min, out max, out center);
            SetBounds(2, min, max, center);
            HideVarsControls();
            customProblem3dPlot1.ProblemaMode = false;
            customProblem3dPlot1.SetFunction(_functionToSlice);
            Refresh();
        }

        public IProblem ProblemToSlice
        {
            get { return _problemToSlice; }
            set
            {
                _problemToSlice = value;

                customProblem3dPlot1.ProblemaMode = true;
                if (value.UpperBound.Count == 2)
                {
                    InitForTwoDimensionalProblem(value);
                }

                else
                {
                    UpdateFreeVarsControls();
                }
            }
        }

        private void InitForTwoDimensionalProblem(IProblem value)
        {
            var newFunction = value.Criterions[0];
            double[] x = Utils.FillCoordArrays(newFunction, 0, _gridSize),
                y = Utils.FillCoordArrays(newFunction, 1, _gridSize);
            var z = Utils.FillValues(newFunction, _gridSize);
            Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, newFunction);
            double[] min, max, center;
            Utils.GetInfo(x, y, z, out min, out max, out center);
            SetBounds(2, min, max, center);
            HideVarsControls();
            customProblem3dPlot1.SetProblem(_problemToSlice);
            Refresh();
        }


        private void HideVarsControls()
        {
            _mainGrid.ColumnStyles[0].Width = 0;
            Refresh();
        }

        private void UpdateFreeVarsControls()
        {
            variablesNames.Clear();
            if (_functionToSlice != null)
            {
                for (var i = 0; i < _functionToSlice.Left.Count; i++)
                    variablesNames.Add("x" + i, i);
            }
            else
            {
                for (var i = 0; i < _problemToSlice.LowerBound.Count; i++)
                    variablesNames.Add("x" + i, i);
            }

            firstFreeVarComboBox.DataSource = variablesNames.Keys.ToList();
            secondFreeVarComboBox.DataSource = variablesNames.Keys.ToList();
            if (this.variablesNames.Count >= 2)
            {
                firstFreeVarComboBox.SelectedIndex = 0;
                secondFreeVarComboBox.SelectedIndex = 1;
            }

        }

        private void FreeVarsButtonClick(object sender, EventArgs e)
        {
            var selectedFreeVar1 = variablesNames[firstFreeVarComboBox.Text];
            var selectedFreeVar2 = variablesNames[secondFreeVarComboBox.Text];
            xLabel.Text = firstFreeVarComboBox.Text;
            yLabel.Text = secondFreeVarComboBox.Text;
            if (selectedFreeVar2 != selectedFreeVar1)
            {
                if (selectedFreeVar1 < selectedFreeVar2)
                {
                    _freeVar1 = selectedFreeVar1;
                    _freeVar2 = selectedFreeVar2;
                }
                else
                {
                    _freeVar1 = selectedFreeVar2;
                    _freeVar2 = selectedFreeVar1;
                }

                _isFreeVariablesInputedCorrectly = true;
                fixedVarsTable.Controls.Clear();
                UpdateFixedVarsControls();
                createSliceButton_Click(sender, e);
            }
            else
            {
                _isFreeVariablesInputedCorrectly = false;
                fixedVarsTable.Controls.Clear();
                SlicePlanes = null;
            }
        }

        private void UpdateFixedVarsControls()
        {
            _fixedVars.Clear();
            var left = FunctionToSlice != null ? FunctionToSlice.Left : ProblemToSlice.LowerBound;
            var right = FunctionToSlice != null ? FunctionToSlice.Right : ProblemToSlice.UpperBound;
            for (var i = 0; i < left.Count; i++)
            {
                if (i == _freeVar1 || i == _freeVar2)
                    continue;


                var propertyInfo = new DoubleProperty("x " + i, left[i], right[i], 10,
                    DoubleDeltaType.Step, (right[i] + left[i]) / 2);
                var control =
                    new DoubleInput(propertyInfo, (right[i] + left[i]) / 2)
                    {
                        Dock = DockStyle.Fill
                    
                    };
                _fixedVars.Add(i, (right[i] + left[i]) / 2);
                control.OnValueChanged += (sender, args) =>
                {
                    var contr = sender as DoubleInput;
                    _fixedVars[Convert.ToInt32(contr.PropertyName.Substring(1))] = contr.GetValue();
                    createSliceButton_Click(sender, args);
                };
                fixedVarsTable.Controls.Add(control);
            }

            Refresh();
        }

        private void createSliceButton_Click(object sender, EventArgs e)
        {
            if (customProblem3dPlot1.ProblemaMode)
            {
                customProblem3dPlot1.SetProblem(ProblemFactory.BuildProblemWithFixedVars(_fixedVars, ProblemToSlice));
            }
            else
                customProblem3dPlot1.SetFunction(new FixedByValuesFunction(FunctionToSlice, _fixedVars));
            customProblem3dPlot1.NeedRedraw = true;
        }

        private void tableLayoutPanel1_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            if (e.Column != 0) return;
            switch (e.Row)
            {
                case 0:
                    e.Graphics.FillRectangle(Brushes.Blue, e.CellBounds);
                    break;
                case 1:
                    e.Graphics.FillRectangle(Brushes.Green, e.CellBounds);
                    break;
                case 2:
                    e.Graphics.FillRectangle(Brushes.Red, e.CellBounds);
                    break;
            }
        }

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
            _min = min;
            _max = max;
            _center = center;
            _isBoundsSet = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            _keyboardHelper.KeyboardCheckerOnTick(sender, e);
        }

        public void DeleteLastPoint(MethodPoint methodPoint)
        {
            customProblem3dPlot1.DeleteLastPoint(methodPoint);
            Refresh();
        }

        public void AddPoint(MethodPoint p)
        {
            if (!NeedToShowPoint) return;
            if (!_isBoundsSet) return;

            if (p.Criterions.Count == 0)
            {
                var vector3D = new Vector3d(2 * (p.X[0] - _center[0]) / (_max[0] - _min[0]),
                    (double) 1,
                    2 * (p.X[1] - _center[1]) / (_max[1] - _min[1]));
                customProblem3dPlot1.AddPoint(vector3D, 0);
            }
            else
            {
                var vector3D = new Vector3d(2 * (p.X[0] - _center[0]) / (_max[0] - _min[0]),
                    (double) (2 * (p.Criterions[_criteriaIndex] - _center[2]) / (_max[2] - _min[2])),
                    2 * (p.X[1] - _center[1]) / (_max[1] - _min[1]));
                customProblem3dPlot1.AddPoint(vector3D, p.IdProcess + 1);
            }

            customProblem3dPlot1.NeedRedraw = true;
        }

        public void ClearPoints()
        {
            if (!NeedToShowPoint) return;
            customProblem3dPlot1.ClearPoints();
            customProblem3dPlot1.NeedRedraw = true;
        }

        private void firstFreeVarComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           FreeVarsButtonClick(sender, e);
        }

        private void secondFreeVarComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            FreeVarsButtonClick(sender,e);
        }
    }
}