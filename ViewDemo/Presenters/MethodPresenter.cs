﻿using System;
using System.Collections.Generic;
using System.Linq;
using Absolut_Model;
using Algorithms;
using Functions;
using NLog;
using Properties;

namespace ViewDemo.Presenters
{
    public class MethodPresenter : EventListenerAdapter
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private Guid _expId;

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                _expId = value;
                _m.SubscribeExperiment(this, _expId);
            }
        }

        private readonly IModel _m;
        private readonly IMethodView _view;

        public MethodPresenter(IMethodView view)
        {
            _view = view;
            _m = ModelFactory.Build();
        }

        public override void OnPointsReset()
        {
            UpdateIterationViews();
        }

        public override void OnMethodChanged()
        {
            UpdatePropertyViews();
        }

        public override void OnNextStep()
        {
            UpdateIterationViews();
        }

        public override void OnPrevStep()
        {
            UpdateIterationViews();
        }


        public List<PropertyInfo> GetParamsInfo()
        {
            return _m.GetPropsNames(_expId);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            if (propsValues.Count > 0)
                _m.SetProperties(_expId, propsValues);
        }

        public void SetCustomFunctionProperties(Dictionary<string, object> propsValues)
        {
            if (propsValues.Count <= 0) return;
            var f = (CustomFunctionDLL) _m.GetProblem(_expId).Criterions[0];
            var clone = (CustomFunctionDLL) f.Clone();
            clone.SetProperties(propsValues);

            _view.ShowAdditionalFunction(clone);
        }

        public void SetExaminMethod(string dllPath, string configPath, string examinPath)
        {
            _m.SetAlg(_expId, AlgFactory.BuildExaminAlg(dllPath, configPath, examinPath));
            _m.SetProblem(ProblemFactory.BuildProblemFromDll(dllPath, configPath), _expId);
        }

        public void ShowFunction()
        {
            _view.ShowAdditionalFunction(_m.GetProblem(_expId).Criterions[0]);
        }

        public void SetMainFunction(string name)
        {
            _m.SetProblem(ProblemFactory.BuildNotConstrainedProblem(name), _expId);
        }

        public void SetMainFunction(List<double> left, List<double> right, string formula)
        {
            _m.SetProblem(ProblemFactory.BuildFromTextInput(left, right, formula), _expId);
        }

        public void SetMainFunction(string dllpath, string xmlpath)
        {
            _m.SetProblem(ProblemFactory.BuildProblemFromDll(dllpath, xmlpath), _expId);
        }

        public void SetMethodWithOldFunction(string methodName)
        {
            var oldProblem = _m.GetProblem(_expId);
            _m.SetAlg(_expId, AlgFactory.Build(methodName));
            _m.SetProblem(oldProblem, _expId);
        }

        public object GetProperty(string propertyName)
        {
            return _m.GetProperty(propertyName, _expId);
        }

        private void UpdatePropertyViews()
        {
            _view.UpdateMethodProperties(_m.GetAlg(_expId));
        }

        private void CreateFunctionWindow()
        {
            _view.СreateUsualFunctionMenu();
        }

        private void UpdateIterationViews()
        {
            _view.UpdateIterarion(_m.GetCurrentInter(_expId));
        }

        public void SetDirectMethod(string dllPath, string configPath, string directPath)
        {
            _m.SetAlg(_expId, AlgFactory.BuildDiRectAlg(dllPath, configPath, directPath));
            _m.SetProblem(ProblemFactory.BuildProblemFromDll(dllPath, configPath), _expId);
        }


        public override void OnProblemChanged()
        {
            UpdatePropertyViews();
            if (!(_m.GetProblem(_expId).Criterions[0] is CustomFunctionDLL f))
                CreateFunctionWindow();
           
        }

        public void SetConstrainedProblem(string methodChooseChoosedOption)
        {
            _m.SetAlg(_expId, AlgFactory.BuildConstrainedAlg(AlgFactory.ConstrainedOptions.FirstOrDefault()));
            _m.SetProblem(ProblemFactory.BuildStandartConstrainedProblem(methodChooseChoosedOption), _expId);
        }

        public void SetExaminMcoMethod(string examinSettingsExaminPath, string examinSettingsDllPath,
            string examinSettingsConfigPath)
        {
            _logger.Info("Setting alg");
            var buildExaminMcoAlg = AlgFactory.BuildExaminMcoAlg(examinSettingsDllPath, examinSettingsConfigPath,
                examinSettingsExaminPath);
            _m.SetAlg(_expId,
                buildExaminMcoAlg);
            if(!buildExaminMcoAlg.GetSupportedFeatures().Contains(SupportedFeatures.InternalProblemGeneration))
                _m.SetProblem(ProblemFactory.BuildProblemFromDll(examinSettingsDllPath, examinSettingsConfigPath), _expId);
        }

        public ISearchAlg GetAlg()
        {
            return _m.GetAlg(_expId);
        }
    }
}