﻿using System.Windows.Forms;
using Properties;

namespace ViewDemo
{
    public partial class StringInput : UserControl
    {
        private StringProperty info;

        public StringInput()
        {
            InitializeComponent();
        }

        public StringInput(StringProperty inf, string startValue)
        {
            InitializeComponent();
            info = inf;
            textBox1.Text = startValue.ToString();
            groupBox1.Text = info.name;
        }

        public string PropertyName => info.name;

        public void SetInfo(StringProperty inf)
        {
            info = inf;
            groupBox1.Text = info.name;
        }

        public string GetValue()
        {
            return textBox1.Text;
        }
    }
}