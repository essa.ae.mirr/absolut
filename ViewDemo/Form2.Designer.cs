﻿namespace ViewDemo
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dislinViewer1 = new Graphics.DislinViewer();
            this.SuspendLayout();
            // 
            // dislinViewer1
            // 
            this.dislinViewer1.Location = new System.Drawing.Point(33, 72);
            this.dislinViewer1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dislinViewer1.Name = "dislinViewer1";
            this.dislinViewer1.Size = new System.Drawing.Size(135, 102);
            this.dislinViewer1.TabIndex = 0;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.dislinViewer1);
            this.Name = "Form2";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private Graphics.DislinViewer dislinViewer1;
    }
}