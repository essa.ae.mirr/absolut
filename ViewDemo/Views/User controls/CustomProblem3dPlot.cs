﻿namespace ViewDemo.Views.User_controls
{
    using System;
    using System.Collections.Generic;
    using System.Windows.Forms;

    using Algorithms;
    using Algorithms.Methods;

    using Functions;

    using global::Properties;

    using Graphics;
    using Graphics.Drawables;

    using OpenTK;

    using ViewDemo.Views.Drawables;

    /// <summary>
    /// The custom problem 3 d plot.
    /// </summary>
    public partial class CustomProblem3DPlot : DrawablesControlThreading, IProblemaView
    {
        private readonly ProblemaSurfaces _problemaSurface = new ProblemaSurfaces();

        private Surface _functionSurface = new Surface();

        private IDrawable _problem2dRepresentation = new ProblemaTemperatureMap();

        private IDrawable _function2dRepresentation = new TemperatureMap();

        private CustomPointsSet3d _pointsSet3 = new CustomPointsSet3d();

        private CustomPointsSet2d _pointsSet2 = new CustomPointsSet2d();

        private bool _problemaMode = false;

        private bool _2dMode = false;

        private bool _isobars = true;

        private int _gridSize = 64;

        private ProblemaIsobars problemaIsobar = new ProblemaIsobars();

        private Isobars isobar = new Isobars();

        private ProblemaTemperatureMap problemaTemperatureMap = new ProblemaTemperatureMap();

        private TemperatureMap temperatureMap = new TemperatureMap();

        public int CriteriaIndex { get; set; } = 0;

        private readonly ToolStripMenuItem twoDimModeMenuItem =
            new ToolStripMenuItem { Text = "2dMode", Checked = true, CheckOnClick = true };

        private readonly ToolStripMenuItem temperatureMapMenu =
            new ToolStripMenuItem { Text = "Isobars/Temperature Map", Checked = true, CheckOnClick = true };
        

        public bool Isobars
        {
            get
            {
                return this._isobars;
            }

            set
            {
                if (this.temperatureMapMenu.Checked != value)
                    this.temperatureMapMenu.Checked = value;
                this._isobars = value;
                Drawables.Clear();
                if (this._isobars)
                {
                    this._problem2dRepresentation = this.problemaIsobar;
                    this._function2dRepresentation = this.isobar;
                }
                else
                {
                    this._problem2dRepresentation = this.problemaTemperatureMap;
                    this._function2dRepresentation = this.temperatureMap;
                }

                this.SetCurrentDrawable();
            }
        }

        public bool Mode2d
        {
            get
            {
                return _2dMode;
            }

            set
            {
                _2dMode = value;
                if (this.twoDimModeMenuItem.Checked != value)
                    this.twoDimModeMenuItem.Checked = value;
                Drawables.Clear();
                SetCurrentDrawable();
            }
        }

        public bool ProblemaMode
        {
            get
            {
                return _problemaMode;
            }

            set
            {
                Drawables.Clear();
                _problemaMode = value;
                SetCurrentDrawable();
            }
        }

        public CustomProblem3DPlot()
        {
            InitializeComponent();
            this.twoDimModeMenuItem.CheckedChanged += this.TwoDimModeMenuItemChanged;
            this.temperatureMapMenu.CheckedChanged += TemperatureMapMenuOnCheckedChanged;
            this.contextMenuStrip1.Items.Add(this.twoDimModeMenuItem);
            this.contextMenuStrip1.Items.Add(this.temperatureMapMenu);
        }

        private void TemperatureMapMenuOnCheckedChanged(object sender, EventArgs eventArgs)
        {
            Isobars = this.temperatureMapMenu.Checked;
        }

        public void SetProblem(IProblem problem)
        {
            if (ProblemaMode)
            {
                _problemaSurface.SurfaceModel = SurfaceModelFactory.BuildProblemaModel(problem);
                IFunction function = problem.Criterions[CriteriaIndex];
                double[] x = Utils.FillCoordArrays(function, 0, _gridSize),
                         y = Utils.FillCoordArrays(function, 1, _gridSize);
                var z = Utils.FillValues(function, _gridSize);
                Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, function);
                double[] min, max, center;
                Utils.GetInfo(x, y, z, out min, out max, out center);
                Utils.AdjustTo01(x, y, z, _gridSize);
                this.problemaIsobar.SetSurface(_gridSize, x, y, z);
                this.problemaIsobar.SetNewProblem(problem);
                this.problemaTemperatureMap.SetSurface(_gridSize, x, y, z);
                this.problemaTemperatureMap.SetNewProblem(problem);
            }
        }

        public void SetFunction(IFunction function)
        {
            if (ProblemaMode) return;
            _functionSurface.SurfaceModel = SurfaceModelFactory.BuildFunctionModel();
            _functionSurface.SurfaceModel.Function = function;
            double[] x = Utils.FillCoordArrays(function, 0, _gridSize),
                     y = Utils.FillCoordArrays(function, 1, _gridSize);
            var z = Utils.FillValues(function, _gridSize);
            Utils.FillZMatrix(_gridSize, _gridSize, z, x, y, function);
            double[] min, max, center;
            Utils.GetInfo(x, y, z, out min, out max, out center);
            Utils.AdjustTo01(x, y, z, _gridSize);
            this.isobar.SetDrawedFunction(function);
            this.isobar.SetSurface(_gridSize, x, y, z);
            this.temperatureMap.SetSurface(_gridSize, x, y, z);
        }

        public void DeleteLastPoint(MethodPoint methodPoint)
        {
            _pointsSet2.DeleteLastPoint(methodPoint);
            _pointsSet3.RemoveLastPoint();
        }

        public void AddPoint(Vector3d newPoint, int i)
        {
            _pointsSet3.AddPoint(newPoint, i);
            _pointsSet2.AddPoint(newPoint, i);
        }

        private void SetCurrentDrawable()
        {
            if (ProblemaMode && _2dMode)
            {
                AddDrawableObject(new IProblemAdapter(this._problem2dRepresentation));
                AddDrawableObject(_pointsSet2);
            }
            else if (ProblemaMode && !_2dMode)
            {
                AddDrawableObject(_problemaSurface);
                AddDrawableObject(_pointsSet3);
            }
            else if (!ProblemaMode && _2dMode)
            {
                AddDrawableObject(new IFunctionAdapter(this._function2dRepresentation));
                AddDrawableObject(_pointsSet2);
            }
            else if (!ProblemaMode && !_2dMode)
            {
                AddDrawableObject(_functionSurface);
                AddDrawableObject(_pointsSet3);
            }

            this.Refresh();
        }

        public void ClearPoints()
        {
            _pointsSet2.ClearPoints();
            _pointsSet3.ClearPoints();
        }

        private void TwoDimModeMenuItemChanged(object sender, EventArgs eventArgs)
        {
            Mode2d = this.twoDimModeMenuItem.Checked;
        }

        private class IFunctionAdapter : IDrawable, IPropertyUser
        {
            private IDrawable _represenation;

            private IPropertyUser _propertyUser;

            public IFunctionAdapter(IDrawable represenation)
            {
                this._represenation = represenation;
                if (represenation is IPropertyUser)
                {
                    this._propertyUser = (IPropertyUser)represenation;
                }
            }

            private string name = string.Empty;

            public void Draw()
            {
                this._represenation.Draw();
            }

            public string Name => this._represenation.Name;

            public void SetProperty(string name, object value)
            {
                this._propertyUser.SetProperty(name, value);
            }

            public void SetProperties(Dictionary<string, object> propsValues)
            {
                this._propertyUser.SetProperties(propsValues);
            }

            public object GetProperty(string name)
            {
                return this._propertyUser.GetProperty(name);
            }

            public List<PropertyInfo> GetPropertiesInfo()
            {
                return this._propertyUser.GetPropertiesInfo();
            }

            public void RegisterProperty(PropertyInfo p)
            {
                this._propertyUser.RegisterProperty(p);
            }
        }

        private class IProblemAdapter : IDrawable, IPropertyUser
        {
            private IDrawable _represenation;
            private IPropertyUser _propertyUser;

            public IProblemAdapter(IDrawable represenation)
            {
                this._represenation = represenation;
                if (represenation is IPropertyUser)
                {
                    this._propertyUser = (IPropertyUser)represenation;
                }
            }
            

            public void Draw()
            {
                this._represenation.Draw();
            }
            
            public string Name => this._represenation.Name;

            public void SetProperty(string name, object value)
            {
                this._propertyUser.SetProperty(name, value);
            }

            public void SetProperties(Dictionary<string, object> propsValues)
            {
                this._propertyUser.SetProperties(propsValues);
            }

            public object GetProperty(string name)
            {
                return this._propertyUser.GetProperty(name);
            }

            public List<PropertyInfo> GetPropertiesInfo()
            {
                return this._propertyUser.GetPropertiesInfo();
            }

            public void RegisterProperty(PropertyInfo p)
            {
                this._propertyUser.RegisterProperty(p);
            }
        }

        

        
    }
}