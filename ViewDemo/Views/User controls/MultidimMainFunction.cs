﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Algorithms;
using Functions;
using Functions.Functions;
using Properties;

namespace ViewDemo.Views.User_controls
{
    public partial class MultidimMainFunction : UserControl, IProblemaView
    {
        private readonly Dictionary<string, int> variablesNames = new Dictionary<string, int>();
        private int _freeVar1;
        private int _freeVar2;
        private IFunction _functionToShow;
        private IFunction _functionToSlice;
        private Guid _id;
        private Dictionary<int, double> _fixedVars = new Dictionary<int, double>();
        private bool isFreeVariablesInputedCorrectly;
        public Dictionary<int, IFunction> SlicePlanes;
        private ProblemaPresenter _p;

        public Guid Id
        {
            get { return _id; }
            set
            {
                multidimFunction3dPlot1.ExpId = value;
                _id = value;
            }
        }
        public IFunction FunctionToSlice
        {
            get { return _functionToSlice; }
            set
            {
                _functionToSlice = value;
                UpdateFreeVarsControls();
            }
        }
        public MultidimMainFunction()
        {
            InitializeComponent();
            _p = new ProblemaPresenter(this);
        }

        private void HideMultiDimControls()
        {
            grid.ColumnStyles[0].Width = 0;
        }
        private void UpdateFreeVarsControls()
        {
            variablesNames.Clear();
            for (var i = 0; i < _functionToSlice.Left.Count; i++)
                variablesNames.Add("x" + i, i);

            firstFreeVarComboBox.DataSource = variablesNames.Keys.ToList();
            secondFreeVarComboBox.DataSource = variablesNames.Keys.ToList();
        }

        private void FreeVarsButtonClick(object sender, EventArgs e)
        {
            var selectedFreeVar1 = variablesNames[firstFreeVarComboBox.Text];
            var selectedFreeVar2 = variablesNames[secondFreeVarComboBox.Text];
            if (selectedFreeVar2 != selectedFreeVar1)
            {
                if (selectedFreeVar1 < selectedFreeVar2)
                {
                    _freeVar1 = selectedFreeVar1;
                    _freeVar2 = selectedFreeVar2;
                }
                else
                {
                    _freeVar1 = selectedFreeVar2;
                    _freeVar2 = selectedFreeVar1;
                }
                isFreeVariablesInputedCorrectly = true;
                var button = sender as Button;
                button.BackColor = Color.Green;
                UpdateFixedVarsControls();
            }
            else
            {
                isFreeVariablesInputedCorrectly = false;
                var button = sender as Button;
                button.BackColor = Color.Red;
                fixedVarTable.Controls.Clear();
                SlicePlanes = null;
            }
        }

        private void UpdateFixedVarsControls()
        {
            for (var i = 0; i < _functionToSlice.Left.Count; i++)
            {
                if (i == _freeVar1 || i == _freeVar2)
                    continue;
                var propertyInfo = new DoubleProperty("x " + i, FunctionToSlice.Left[i], FunctionToSlice.Right[i], 2, DoubleDeltaType.Step, (FunctionToSlice.Right[i] + FunctionToSlice.Left[i]) / 2);
                var control = new DoubleInput(propertyInfo, (FunctionToSlice.Right[i] + FunctionToSlice.Left[i]) / 2) { Dock = DockStyle.Fill };
                _fixedVars.Add(i, (FunctionToSlice.Right[i] + FunctionToSlice.Left[i]) / 2);
                control.OnValueChanged += (sender, args) =>
                {
                    var contr = sender as DoubleInput;
                    _fixedVars[Convert.ToInt32(contr.PropertyName.Substring(1))] = contr.GetValue();
                    // _fixedVars.Add(Convert.ToInt32(contr.PropertyName.Substring(1)), contr.GetValue());
                    createSliceButton_Click(sender, args);
                };
                fixedVarTable.Controls.Add(control);
            }
            Refresh();
        }

        private void createSliceButton_Click(object sender, EventArgs e)
        {
            _functionToShow = new FixedByValuesFunction(FunctionToSlice, _fixedVars);
           //.SetFunction(_functionToShow);
            Refresh();
        }

        public void SetProblem(IProblem problem)
        {
            if (problem.Dimension == 2)
            {
                HideMultiDimControls();
            }
            
        }
    }
}
