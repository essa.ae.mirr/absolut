﻿using System;
using System.Collections.Generic;
using Graphics;

namespace ViewDemo.Views
{
    internal class MultidimFunction3dPlot : DrawablesControlThreading, ISlicesUser
    {
        private readonly MultidimMainFunction3dView _function3DView = new MultidimMainFunction3dView();
        private readonly MultidimPointsOnPlot _points = new MultidimPointsOnPlot();
        private Guid _expId;


        public MultidimFunction3dPlot()
        {
            AddDrawableObject(_points);
            AddDrawableObject(_function3DView);
        }

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                if (value == Guid.Empty) return;
                _expId = value;
                _function3DView.SetExperimentId(_expId);
                _points.SetExperimentId(_expId);
            }
        }

        public void SetFixedVars(Dictionary<int, double> fixedVars)
        {
            _function3DView.SetFixedVars(fixedVars);
            _points.SetFixedVars(fixedVars);
        }
    }
}