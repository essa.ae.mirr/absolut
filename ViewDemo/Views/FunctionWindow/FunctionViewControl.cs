﻿using Functions;
using Graphics;

namespace ViewDemo
{
    public class FunctionViewControl : DrawablesControl
    {
        private  FunctionView points = new FunctionView();

        public FunctionViewControl()
        {
            AddDrawableObject(points);
        }

        public void SetFunction(IFunction f)
        {
            points.SurfaceModel = SurfaceModelFactory.BuildFunctionModel(f);
           
        }

    }
}