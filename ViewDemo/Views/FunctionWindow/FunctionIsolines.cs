﻿using Graphics;

namespace ViewDemo
{
    public class FunctionIsolines : Isobars
    {
        public FunctionIsolines(int gridPointsCount, double[] x, double[] y, double[,] z)
        {
            Utils.AdjustTo01(x, y, z, gridPointsCount);
            SetSurface(gridPointsCount, x, y, z);
        }
    }
}