﻿using Functions;
using Graphics;
using Graphics.Drawables;


namespace ViewDemo
{
    public class FunctionView : Surface
    {
        public FunctionView(IFunction f)
        {
            SurfaceModel = SurfaceModelFactory.BuildFunctionModel();
            SurfaceModel.Function = f;
        }
        public FunctionView()
        {
            
        }
    }
}