﻿using System;
using System.Windows.Forms;
using Algorithms;
using Algorithms.Methods;

namespace ViewDemo.Views.Labels
{
    public partial class CurrentPointLabel : UserControl, IExperimentPointsView
    {
        public CurrentPointLabel()
        {
            InitializeComponent();
        }

        public void AddPoint(MethodPoint p)
        {
            curPointLabel.Text = p.ToString();
        }

        public void ClearPoints()
        {
            curPointLabel.Text = "";
        }

        public void DeleteLastPoint(MethodPoint p)
        {
            curPointLabel.Text = p.ToString();
        }

        public void SetProcesses(int procNum)
        {
            
        }

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
           
        }

        public void SetProblem(IProblem problem)
        {
            throw new NotImplementedException();
        }
    }
}
