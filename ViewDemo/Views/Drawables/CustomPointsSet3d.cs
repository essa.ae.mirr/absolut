﻿using System.Collections.Generic;
using Graphics;
using OpenTK;

namespace ViewDemo.Views.Drawables
{
    public class CustomPointsSet3d : PointsSet3D
    {
        private int lastBin = -1;
        public void AddPoint(Vector3d point, int bin = 0)
        {
            if(!Bins.ContainsKey(bin)) Bins.Add(bin, new List<Vector3d>());
            Bins[bin].Add(point);
            lastBin = bin;
        }

        public void RemoveLastPoint()
        {
            if (lastBin != -1)
                Bins[lastBin].RemoveAt(Bins[lastBin].Count - 1);
        }


        public void ClearPoints()
        {
            Bins.Clear();
            lastBin = -1;
        }
    }
}