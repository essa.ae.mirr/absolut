﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Algorithms;
using Algorithms.Methods;

namespace ViewDemo.Views.Drawables
{
    public class MethodPointsDataSource : IExperimentPointsView
    {
        private BindingList<MethodPoint> _points = new BindingList<MethodPoint>();

        public BindingList<MethodPoint> Points
        {
            get { return Points1; }
        }

        private double? _bestValue;
        private List<double> _optimalPoint;
        private Guid _expId;
        private ExperimentPointsPresenter _experimentPointsPresenter;

        public Guid ExpId
        {
            get { return _expId; }
            set
            {
                if (value == Guid.Empty) return;
                if (_experimentPointsPresenter != null)
                    throw new InvalidOperationException();
                _expId = value;
                _experimentPointsPresenter = new ExperimentPointsPresenter(_expId, 64);
                _experimentPointsPresenter.AddView(this);
            }
        }

        public BindingList<MethodPoint> Points1 { get => _points; set => _points = value; }

        public void SetBounds(int dims, double[] min, double[] max, double[] center)
        {
        }

        public void SetProblem(IProblem problem)
        {
            _bestValue = problem.OptimalValue;
            _optimalPoint = problem.OptimalPoint;
        }

        public void ClearPoints()
        {
            Points1.Clear();
        }

        public void AddPoint(MethodPoint p)
        {
           Points1.Add(p);
        }

        public void DeleteLastPoint(MethodPoint p)
        {
            Points1.Remove(p);
        }
    }
}