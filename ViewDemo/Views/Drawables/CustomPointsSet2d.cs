﻿using System;
using System.Collections.Generic;
using Algorithms.Methods;
using Graphics;
using OpenTK;

namespace ViewDemo.Views.Drawables
{
    public class CustomPointsSet2d : PointsSet2D
    {
       
        private int lastBin = -1;

        public void AddPoint(Vector3d newPoint, int bin)
        {
            if(!Bins.ContainsKey(bin))
                Bins.Add(bin, new List<Vector3d>());
            var copy = new Vector3d(newPoint) {Y = 0};
            Bins[bin].Add(copy);

        }
      

        public void DeleteLastPoint(MethodPoint p)
        {
            if (lastBin != -1)
                Bins[lastBin].RemoveAt(Bins[lastBin].Count - 1);
        }


        public void ClearPoints()
        {
            Bins.Clear();
            lastBin = -1;
        }
    }
}