﻿using Algorithms;

namespace ViewDemo
{
    public interface IProblemaView
    {
        void SetProblem(IProblem problem);
    }
}