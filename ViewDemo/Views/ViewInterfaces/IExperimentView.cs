﻿using System;

namespace ViewDemo
{
    public interface IExperimentView
    {
        void ShowNewExperiment(Guid expId, string expName);
    }
}