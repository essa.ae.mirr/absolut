﻿using System;
using Algorithms;

namespace ViewDemo
{
    public interface ISeriesExperimentsView
    {
        void ShowNewSeriesExperiment(Guid id, string expName, int expCount);
        void HideDeletedSeriesExperiment(Guid id);
        void AddNewExperiment(ISearchAlg alg);
    }
}