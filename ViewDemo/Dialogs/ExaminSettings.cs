﻿using System;
using System.Windows.Forms;

namespace ViewDemo
{
    public partial class ExaminSettings : Form
    {
        private string _examinPath;
        private string _dllPath;

        private string algOption;

        public string AlgOption
        {
            get => this.algOption;
            set => this.algOption = value;
        }

        public string DllPath
        {
            get => _dllPath;
            set
            {
                _dllPath = value;
                textBox1.Text = value;
            }
        }

        public string ConfigPath { get; set; }

        public string ExaminPath
        {
            get => _examinPath;
            set
            {
                _examinPath = value;
                textBox3.Text = value;
            }
        }

        public ExaminSettings()
        {
            InitializeComponent();
           
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (DllPath == "" || ConfigPath == "" || ExaminPath == "") return;
            DllPath = textBox1.Text;
            ConfigPath = textBox2.Text;
            ExaminPath = textBox3.Text;
            DialogResult = DialogResult.OK;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog f = new OpenFileDialog())
            {
                f.Filter = "Dynamic Library Files (DLL)|*.DLL;";
                DialogResult dr = f.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox1.Text = f.FileName;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog f = new OpenFileDialog())
            {
                f.Filter = "Configuration Files (CONF)|*.CONF;";
                DialogResult dr = f.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox2.Text = f.FileName;
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog f = new OpenFileDialog())
            {
                f.Filter = "Executable Files (EXE)|*.EXE;";
                DialogResult dr = f.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    textBox3.Text = f.FileName;
                }
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            var radioButton = (RadioButton)sender;
            if (radioButton.Checked)
            {
                this.algOption = radioButton.Text;
            }
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            var radioButton = (RadioButton)sender;
            if (radioButton.Checked)
            {
                this.algOption = radioButton.Text;
            }
        }
    }
}
