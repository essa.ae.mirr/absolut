﻿namespace ViewDemo.Dialogs
{
    partial class AbsolutMethodSettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.seriesTypeGroupBox = new System.Windows.Forms.GroupBox();
            this.typeRadioButtonsPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.absolutMethodsRadioButton = new System.Windows.Forms.RadioButton();
            this.examinMethod = new System.Windows.Forms.RadioButton();
            this.experimentsCountGroupBox = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.seriesTypeGroupBox.SuspendLayout();
            this.typeRadioButtonsPanel.SuspendLayout();
            this.experimentsCountGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.00599F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.99401F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.seriesTypeGroupBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.experimentsCountGroupBox, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.92337F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.23956F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.44847F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.87739F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(284, 261);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.button2);
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 232);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(278, 26);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Right;
            this.button2.Location = new System.Drawing.Point(200, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(119, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.groupBox1, 2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(5);
            this.groupBox1.Size = new System.Drawing.Size(278, 46);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Series  name";
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBox1.Location = new System.Drawing.Point(5, 18);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(241, 20);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "New experiment";
            // 
            // seriesTypeGroupBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.seriesTypeGroupBox, 2);
            this.seriesTypeGroupBox.Controls.Add(this.typeRadioButtonsPanel);
            this.seriesTypeGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.seriesTypeGroupBox.Location = new System.Drawing.Point(3, 55);
            this.seriesTypeGroupBox.Name = "seriesTypeGroupBox";
            this.seriesTypeGroupBox.Size = new System.Drawing.Size(278, 115);
            this.seriesTypeGroupBox.TabIndex = 3;
            this.seriesTypeGroupBox.TabStop = false;
            this.seriesTypeGroupBox.Text = "Series Type";
            // 
            // typeRadioButtonsPanel
            // 
            this.typeRadioButtonsPanel.Controls.Add(this.absolutMethodsRadioButton);
            this.typeRadioButtonsPanel.Controls.Add(this.examinMethod);
            this.typeRadioButtonsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.typeRadioButtonsPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.typeRadioButtonsPanel.Location = new System.Drawing.Point(3, 16);
            this.typeRadioButtonsPanel.Name = "typeRadioButtonsPanel";
            this.typeRadioButtonsPanel.Padding = new System.Windows.Forms.Padding(2);
            this.typeRadioButtonsPanel.Size = new System.Drawing.Size(272, 96);
            this.typeRadioButtonsPanel.TabIndex = 0;
            // 
            // absolutMethodsRadioButton
            // 
            this.absolutMethodsRadioButton.AutoSize = true;
            this.absolutMethodsRadioButton.Checked = true;
            this.absolutMethodsRadioButton.Location = new System.Drawing.Point(5, 5);
            this.absolutMethodsRadioButton.Name = "absolutMethodsRadioButton";
            this.absolutMethodsRadioButton.Size = new System.Drawing.Size(104, 17);
            this.absolutMethodsRadioButton.TabIndex = 0;
            this.absolutMethodsRadioButton.TabStop = true;
            this.absolutMethodsRadioButton.Text = "Absolut Methods";
            this.absolutMethodsRadioButton.UseVisualStyleBackColor = true;
            // 
            // examinMethod
            // 
            this.examinMethod.AutoSize = true;
            this.examinMethod.Location = new System.Drawing.Point(5, 28);
            this.examinMethod.Name = "examinMethod";
            this.examinMethod.Size = new System.Drawing.Size(59, 17);
            this.examinMethod.TabIndex = 1;
            this.examinMethod.Text = "Examin";
            this.examinMethod.UseVisualStyleBackColor = true;
            // 
            // experimentsCountGroupBox
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.experimentsCountGroupBox, 2);
            this.experimentsCountGroupBox.Controls.Add(this.numericUpDown1);
            this.experimentsCountGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.experimentsCountGroupBox.Location = new System.Drawing.Point(3, 176);
            this.experimentsCountGroupBox.Name = "experimentsCountGroupBox";
            this.experimentsCountGroupBox.Padding = new System.Windows.Forms.Padding(5);
            this.experimentsCountGroupBox.Size = new System.Drawing.Size(278, 50);
            this.experimentsCountGroupBox.TabIndex = 4;
            this.experimentsCountGroupBox.TabStop = false;
            this.experimentsCountGroupBox.Text = "Experiments Count";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Dock = System.Windows.Forms.DockStyle.Left;
            this.numericUpDown1.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(5, 18);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            250,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 0;
            this.numericUpDown1.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // AbsolutMethodSettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "AbsolutMethodSettingsForm";
            this.Text = "AbsolutMethodSettingsForm";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.seriesTypeGroupBox.ResumeLayout(false);
            this.typeRadioButtonsPanel.ResumeLayout(false);
            this.typeRadioButtonsPanel.PerformLayout();
            this.experimentsCountGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        protected System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        protected System.Windows.Forms.Button button2;
        protected System.Windows.Forms.Button button1;
        protected System.Windows.Forms.GroupBox groupBox1;
        protected System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.GroupBox seriesTypeGroupBox;
        private System.Windows.Forms.FlowLayoutPanel typeRadioButtonsPanel;
        private System.Windows.Forms.RadioButton absolutMethodsRadioButton;
        private System.Windows.Forms.RadioButton examinMethod;
        private System.Windows.Forms.GroupBox experimentsCountGroupBox;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
    }
}