﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorithms;
using Algorithms.Methods;
using Functions;

namespace Executors.Impl
{
    public class AlgExecutor
    {
     
        public  ExecutorWithAlgorithm WithAlgorithm(string algorithmName)
        {
           return new ExecutorWithAlgorithm(AlgFactory.Build(algorithmName), algorithmName);
        }

        public static AlgExecutor Create()
        {
            return new AlgExecutor();
        }
    }
}