﻿using Algorithms;
using Functions;

namespace Executors
{
    public class ExecutorWithAlgorithm
    {
        private ISearchAlg _alg;
        private string _algName;

       public  ExecutorWithAlgorighmAndProblem WithNotConstrainedProblem(string problemName)
        {
            return new ExecutorWithAlgorighmAndProblem(_alg, ProblemFactory.BuildNotConstrainedProblem(problemName),
                _algName, problemName);
        }

        public ExecutorWithAlgorighmAndProblem WithConstrainedProblem(string problemName)
        {
            return new ExecutorWithAlgorighmAndProblem(_alg, ProblemFactory.BuildStandartConstrainedProblem(problemName),
                _algName, problemName);
        }

        public ExecutorWithAlgorighmAndProblem WithDllProblem(string pathToLib, string pathToConfig)
        {
            //TODO get name of problem ples
            return new ExecutorWithAlgorighmAndProblem(_alg, ProblemFactory.BuildProblemFromDll(pathToLib, pathToConfig), _algName, pathToConfig);
        }

        public ExecutorWithAlgorighmAndProblem WithProblem(IProblem problem, string name = "undefined")
        {
            return new ExecutorWithAlgorighmAndProblem(_alg, problem, _algName, name);
        }

        internal ExecutorWithAlgorithm(ISearchAlg alg, string algName)
        {
            _alg = alg;
            _algName = algName;
        }
    }
}