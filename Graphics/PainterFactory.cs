﻿namespace Graphics
{
    public static class PainterFactory
    {
        private static IPainter p = new SimplePainter();
        public static IPainter BuildPainter()
        {
            return p;
        }
    }
}