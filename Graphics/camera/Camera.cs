﻿using System;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    public class Camera
    {
        /// <summary> Вектор направления 'Вверх'. </summary>
        private Vector3d _upDirection = Vector3d.UnitY;

        private double _rotateXangle;
        private double _rotateYangle;

        /// <summary> Положение камеры в мировом пространстве. </summary>
        public Vector3d Position { get; set; }

        /// <summary> Параметры окна отображения. </summary>
        public Viewport Viewport { get; set; } = new Viewport(512, 512);

        /// <summary>
        /// Куда камера смотрит?
        /// </summary>
        public Vector3d Target => Vector3d.Zero;

        /// <summary> Создает новую камеру по заданному положению</summary>
        public Camera(Vector3d pos)
        {
            Position = pos;
        }
        
        #region Rotation
        public void RotateAroundX(double k)
        {
            if (k > 0)
            {
                if (_rotateXangle < Math.PI / 2 && _rotateXangle + k > Math.PI / 2)
                    _upDirection = -1 * _upDirection;
                else if (_rotateXangle < 3 * Math.PI / 2 && _rotateXangle + k > 3 * Math.PI / 2)
                    _upDirection = -1 * _upDirection;
                else if (_rotateXangle < -3 * Math.PI / 2 && _rotateXangle + k > -3 * Math.PI / 2)
                    _upDirection = -1 * _upDirection;
                else if (_rotateXangle < -Math.PI / 2 && _rotateXangle + k > -Math.PI / 2)
                    _upDirection = -1 * _upDirection;
            }
            else
            {
                if (_rotateXangle > -Math.PI / 2 && _rotateXangle + k < -Math.PI / 2)
                    _upDirection = -1 * _upDirection;
                else if (_rotateXangle > -3 * Math.PI / 2 && _rotateXangle + k < -3 * Math.PI / 2)
                    _upDirection = -1 * _upDirection;
                else if (_rotateXangle > Math.PI / 2 && _rotateXangle + k < Math.PI / 2)
                    _upDirection = -1 * _upDirection;
                else if (_rotateXangle > 3 * Math.PI / 2 && _rotateXangle + k < 3 * Math.PI / 2)
                    _upDirection = -1 * _upDirection;
            }
            _rotateXangle += (k);
            if (_rotateXangle >= 2 * Math.PI) _rotateXangle -= 2 * Math.PI;
            if (_rotateXangle <= -2 * Math.PI) _rotateXangle += 2 * Math.PI;
            UpdatePosition();
        }

        public void RotateAroundY(double k)
        {
            _rotateYangle += (k);
            if (_rotateYangle >= 2 * Math.PI) _rotateYangle -= 2 * Math.PI;
            UpdatePosition();
        }

        private void UpdatePosition()
        {
            Vector4d initialDirection = new Vector4d(0, 0, 1, 1);
            var tempPos = Vector4d.Transform(initialDirection, Matrix4d.Rotate(Vector3d.UnitX, _rotateXangle));
            tempPos = Vector4d.Transform(tempPos, Matrix4d.Rotate(Vector3d.UnitY, _rotateYangle));
            Position = new Vector3d(tempPos * Position.Length);
        }
        #endregion

        /// <summary> Загружает настройки камеры в параметры состояния API OpenGL. </summary>
        public void Setup()
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            var mPrespective = Matrix4.CreatePerspectiveFieldOfView(Viewport.FieldOfView,
                Viewport.Aspect, 1.0f, 1000.0f);
            GL.LoadMatrix(ref mPrespective);

            var modelView = Matrix4d.LookAt(Position, Target, _upDirection);

            GL.MatrixMode(MatrixMode.Modelview);
            GL.LoadMatrix(ref modelView);
        }
    }
}