﻿using System.Collections.Generic;
using System.Drawing;

using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    using Properties;
    public class PointsSet2D : IDrawable, IPropertyUser
    {
        protected readonly Dictionary<int, List<Vector3d>> Bins;

        private readonly List<Color> _allColors = new List<Color>()
                                                      {
                                                          Color.Red,
                                                          Color.Blue,
                                                          Color.Indigo,
                                                          Color.DeepPink,
                                                          Color.DodgerBlue,
                                                          Color.LightSeaGreen,
                                                          Color.Cyan,
                                                          Color.Magenta
                                                      };

        private IPainter _p = PainterFactory.BuildPainter();

        private PropertyProvider _provider = new PropertyProvider();

        public PointsSet2D()
        {
            Bins = new Dictionary<int, List<Vector3d>>();
            RegisterProperty(new DoubleProperty("Point size", 0.01, 0.05, 10, DoubleDeltaType.Step, 0.025));
        }

        public void Draw()
        {
            foreach (var binsKey in this.Bins.Keys)
            {
                foreach (var t in Bins[binsKey])
                {
                    this._p.DrawColorPoint(t, binsKey >= this._allColors.Count ? Color.Red : this._allColors[binsKey],
                        (double)GetProperty("Point size"));
                }
            }
        }

        public string Name => "Points2d";

        public void SetProperty(string name, object value)
        {
            this._provider.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            this._provider.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return this._provider.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return this._provider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            this._provider.RegisterProperty(p);
        }
    }
}