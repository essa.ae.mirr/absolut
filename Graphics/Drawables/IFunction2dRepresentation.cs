﻿namespace Graphics
{
    public interface IFunction2dRepresentation
    {
        string Name { get; }

        void Draw();
    }
}