using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;

namespace Graphics.Drawables
{
    public class Sphere : IDrawable
    {
// ants //////////////////////////////////////////////////////////////////
        int MIN_SECTOR_COUNT = 3;
        int MIN_STACK_COUNT = 2;

        private float radius;
        private int sectorCount;
        private int stackCount;
        private bool smooth;

        private List<Vector3> _vertex = new List<Vector3>();
        private List<Vector3> _indicies = new List<Vector3>();
        private List<Vector2> _lineIndicies = new List<Vector2>();
        private string _name;
        private IPainter _painter = PainterFactory.BuildPainter();


        public Sphere(float radius, int sectors, int stacks, bool smooth)
        {
            set(radius, sectors, stacks, smooth);
        }


///////////////////////////////////////////////////////////////////////////////
// setters
///////////////////////////////////////////////////////////////////////////////
        void set(float radius, int sectors, int stacks, bool smooth)
        {
           
            this.radius = radius;
            this.sectorCount = sectors;
            if (sectors < MIN_SECTOR_COUNT)
                this.sectorCount = MIN_SECTOR_COUNT;
            this.stackCount = stacks;
            if (sectors < MIN_STACK_COUNT)
                this.sectorCount = MIN_STACK_COUNT;
            this.smooth = smooth;
            buildVerticesSmooth();
        }

        void setRadius(float radius)
        {
            this.radius = radius;
            updateRadius();
        }

        void setSectorCount(int sectors)
        {
            set(radius, sectors, stackCount, smooth);
        }

        void setStackCount(int stacks)
        {
            set(radius, sectorCount, stacks, smooth);
        }

        void setSmooth(bool smooth)
        {
            if (this.smooth == smooth)
                return;

            this.smooth = smooth;
            buildVerticesSmooth();
        }


        public void Draw(Color color)
        {
            foreach (var i in _indicies)
            {
                _painter.DrawTriangle(_vertex[(int) i.X], color, _vertex[(int) i.Y], color,_vertex[(int) i.Z], color);
            }
        }



        void updateRadius()
        {
            float scale = (float) Math.Sqrt(radius * radius /
                                            (_vertex[0].X * _vertex[0].X + _vertex[0].Y * _vertex[0].Y +
                                             _vertex[0].Z * _vertex[0].Z));


            int count = _vertex.Count;
            for (int i = 0, j = 0; i < count; i++, j += 8)
            {
                _vertex[i] = Vector3.Multiply(_vertex[i], scale);
            }
        }


///////////////////////////////////////////////////////////////////////////////
// build vertices of sphere with smooth shading using parametric equation
// x = r * cos(u) * cos(v)
// y = r * cos(u) * sin(v)
// z = r * sin(u)
// where u: stack(latitude) angle (-90 <= u <= 90)
//       v: sector(longitude) angle (0 <= v <= 360)
///////////////////////////////////////////////////////////////////////////////
        void buildVerticesSmooth()
        {
            float PI = 3.1415926f;

            float x, y, z, xy; // vertex position


            float sectorStep = 2 * PI / sectorCount;
            float stackStep = PI / stackCount;
            float sectorAngle, stackAngle;

            for (int i = 0; i <= stackCount; ++i)
            {
                stackAngle = PI / 2 - i * stackStep; // starting from pi/2 to -pi/2
                xy = (float) (radius * Math.Cos(stackAngle)); // r * cos(u)
                z = (float) (radius * Math.Sin(stackAngle)); // r * sin(u)

                // add (sectorCount+1) vertices per stack
                // the first and last vertices have same position and normal, but different tex coords
                for (int j = 0; j <= sectorCount; ++j)
                {
                    sectorAngle = j * sectorStep; // starting from 0 to 2pi

                    // vertex position
                    x = (float) (xy * Math.Cos(sectorAngle)); // r * cos(u) * cos(v)
                    y = (float) (xy * Math.Sin(sectorAngle)); // r * cos(u) * sin(v)
                    addVertex(x, y, z);
                }
            }

            int k1, k2;
            for (int i = 0; i < stackCount; ++i)
            {
                k1 = i * (sectorCount + 1); // beginning of current stack
                k2 = k1 + sectorCount + 1; // beginning of next stack

                for (int j = 0; j < sectorCount; ++j, ++k1, ++k2)
                {
                    // 2 triangles per sector excluding 1st and last stacks
                    if (i != 0)
                    {
                        addIndices(k1, k2, k1 + 1); // k1---k2---k1+1
                    }

                    if (i != (stackCount - 1))
                    {
                        addIndices(k1 + 1, k2, k2 + 1); // k1+1---k2---k2+1
                    }

                    // vertical lines for all stacks
                    _lineIndicies.Add(new Vector2(k1, k2));
                    if (i != 0) // horizontal lines except 1st stack
                    {
                        _lineIndicies.Add(new Vector2(k1, k1 + 1));
                    }
                }
            }
        }


        void addVertex(float x, float y, float z)
        {
            _vertex.Add(new Vector3(x, y, z));
        }

        void addIndices(int i1, int i2, int i3)
        {
            _indicies.Add(new Vector3(i1, i2, i3));
        }


        public void Draw()
        {
            Draw(Color.Black);
        }

        public string Name => _name;
    }
}