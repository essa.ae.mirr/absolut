﻿using System;
using System.Collections.Generic;
using System.Drawing;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Properties;

namespace Graphics
{
    public class LinesOnAxisOnScreen : IDrawable, IPropertyUser
    {
        private readonly IPainter painter;
        protected readonly List<Vector3d> points;
        private Vector3d _axis = Vector3d.UnitX;
        protected Color lineColor = Color.Navy;
        protected double lineSize = 0.2;
        protected PropertyProvider PropertyProvider = new PropertyProvider();
        protected double shifth = 0;
        protected double start = -1.6;

        public LinesOnAxisOnScreen()
        {
            painter = new SimplePainter();
            points = new List<Vector3d>();
            RegisterProperty(new ColorProperty("Line color", Color.Navy));
        }

        public List<DrawablePropertyInfo> PropertyInfos { get; }


        public void Draw()
        {
            Vector3d start, end;
            if (Math.Abs(_axis.Z) > 0.0001)
            {
                GL.PushMatrix();
                GL.Scale(1, 1, -1);
            }
            foreach (var t in points)
            {
                GL.LineWidth(0.1f);
                var color = (Color) GetProperty("Line color");
                if (_axis.X != 0)
                {
                    start = new Vector3d(t.X, this.start + shifth, 0);
                    end = new Vector3d(t.X, this.start + shifth + lineSize, 0);
                    painter.DrawLine(start, end, color);
                }
                else if (_axis.Y != 0)
                {
                    start = new Vector3d(this.start - shifth, t.Y, 0);
                    end = new Vector3d(this.start - shifth - lineSize, t.Y, 0);
                    painter.DrawLine(start, end, color);
                }
                else if (_axis.Z != 0)
                {
                    start = new Vector3d(this.start - shifth, 0, t.Z);
                    end = new Vector3d(this.start - shifth - lineSize, 0, t.Z);
                    painter.DrawLine(start, end, color);
                }
            }
            if (_axis.Z != 0)
                GL.PopMatrix();
        }

        public string Name => "Lines on axis";

        public void SetProperty(string name, object value)
        {
            PropertyProvider.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            PropertyProvider.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return PropertyProvider.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return PropertyProvider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            PropertyProvider.RegisterProperty(p);
        }

        public void SetAxis(Vector3d axis)
        {
            _axis = axis;
        }
    }
}