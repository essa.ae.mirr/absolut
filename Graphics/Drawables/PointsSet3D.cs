﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Graphics
{
    using Properties;
    public class PointsSet3D : IDrawable, IPropertyUser
    {
        private readonly IPainter painter;
        protected readonly Dictionary<int, List<Vector3d>> Bins;
        private List<Color> allColors = new List<Color>() {Color.Red, Color.Blue, Color.Indigo, Color.DeepPink, Color.DodgerBlue, Color.LightSeaGreen, Color.Cyan, Color.Magenta};
        PropertyProvider _propertyProvider = new PropertyProvider();
        public PointsSet3D()
        {
            painter = new SimplePainter();
            Bins = new Dictionary<int, List<Vector3d>>();
            RegisterProperty(new DoubleProperty("Point size", 0.01, 0.05, 10, DoubleDeltaType.Step, 0.025));
            RegisterProperty(new ColorProperty("Thread 1 Color", Color.Blue));
        }
        

        public void Draw()
        {
            
            GL.PushMatrix();
            GL.Scale(1, 1, -1);
            double min = 2;
            foreach (var  key in Bins.Keys)
            {
                foreach (var t in Bins[key])
                {
                    if (t.Y < min)
                    {
                        min = t.Y;
                    }
                }
            }
            
            foreach (var  key in Bins.Keys)
            {
                foreach (var t in Bins[key])
                {
                    if (t.Y == min)
                    {
                        painter.DrawColorPoint(t, this.allColors.Count <= key ? Color.Black : this.allColors[key], 2*(double)GetProperty("Point size"));
                    }
                    else
                    {
                        painter.DrawColorPoint(t, this.allColors.Count <= key ? Color.Black : this.allColors[key],  (double)GetProperty("Point size"));
                    }
                   
                }
            }
           
            GL.PopMatrix();
        }

        public string Name => "Points";

        public void SetProperty(string name, object value)
        {
            if (name == "Thread 1 Color")
            {
                this.allColors[1] = (Color)value;
            }
            this._propertyProvider.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            if (propsValues.ContainsKey("Thread 1 Color"))
            {
                this.allColors[1] = (Color)propsValues["Thread 1 Color"];
            }
            this._propertyProvider.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return this._propertyProvider.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return this._propertyProvider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            this._propertyProvider.RegisterProperty(p);
        }
    }
}