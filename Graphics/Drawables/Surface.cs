﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Functions;
using Graphics.DrawablesModel;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using Properties;

namespace Graphics.Drawables
{

    public class Surface : IDrawable, IPropertyUser
    {
        private readonly IPainter _painter = PainterFactory.BuildPainter();
        
        private readonly PropertyProvider _propertyProvider = new PropertyProvider();


        private SurfaceModel _surfaceModel;

        public SurfaceModel SurfaceModel
        {
            get => _surfaceModel;
            set => _surfaceModel = value;
        }


        public Surface()
        {
            _propertyProvider.RegisterProperty(new BoolProperty("Mesh", true));
            _propertyProvider.RegisterProperty(new BoolProperty("Edge", true));
            _propertyProvider.RegisterProperty(new BoolProperty("Axis", true));
            _propertyProvider.RegisterProperty(new ColorProperty("Edge color", Color.Blue));
            _propertyProvider.RegisterProperty(new ColorProperty("Mesh color", Color.Black));
            _propertyProvider.RegisterProperty(new BoolProperty("Surface material", true));
            _propertyProvider.RegisterProperty(new IntProperty(RenderingConstants.GRID_POINTS_PROPERTY, 8, 64, 8, 42));

        }

        public void Draw()
        {
            if (SurfaceModel != null && _painter != null) DrawSurface();
        }

        public string Name => "Surface";


        public void SetProperty(string name, object value)
        {
            if (name == RenderingConstants.GRID_POINTS_PROPERTY)
            {
                SurfaceModel.SetProperty(name, value);
            }
            _propertyProvider.SetProperty(name, value);
        }


        public void SetProperties(Dictionary<string, object> propsValues)
        {
            if (propsValues.ContainsKey(RenderingConstants.GRID_POINTS_PROPERTY))
            {
                SurfaceModel.SetProperty(RenderingConstants.GRID_POINTS_PROPERTY, propsValues[RenderingConstants.GRID_POINTS_PROPERTY]);
            }
            _propertyProvider.SetProperties(propsValues);
        }

        public object GetProperty(string name) => _propertyProvider.GetProperty(name);
        public List<PropertyInfo> GetPropertiesInfo() => _propertyProvider.GetPropertiesInfo();

        public void RegisterProperty(PropertyInfo p) => _propertyProvider.RegisterProperty(p);

        private void DrawSurface()
        {
            GL.PushMatrix();
            GL.Scale(1, 1, -1);
            var source = SurfaceModel.GetGridSquares();
            if ((bool) GetProperty("Axis")) DrawAxis();
            foreach (var polygon in source)
            {
                if ((bool) GetProperty("Surface material")) DrawSurfaceMaterial(polygon);
                if ((bool) GetProperty("Mesh")) DrawBorder(polygon);
            }
            GL.PopMatrix();
        }

        private void DrawBorder(GridSquare polygon)
        {
            var a = polygon.LeftBottom;
            var b = polygon.RightBottom;
            var c = polygon.LeftUpper;
            var d = polygon.RightUpper;

            var borderColor = (Color) GetProperty("Mesh color");
            var outsideBorderColor = (bool) GetProperty("Edge")
                ? (Color) GetProperty("Edge color")
                : (Color) GetProperty("Mesh color");
            var outsideBorderWidth = (bool) GetProperty("Edge") ? 3 : 1;
            const int borderWidth = 1;
            _painter.DrawLine((Vector3d) a, (Vector3d) b, Math.Abs(a.Z + 1) < 0.001 ? outsideBorderColor : borderColor,
                Math.Abs(a.Z + 1) < 0.001 ? outsideBorderWidth : borderWidth);
            _painter.DrawLine((Vector3d) a, (Vector3d) c, Math.Abs(a.X + 1) < 0.001 ? outsideBorderColor : borderColor,
                Math.Abs(a.X + 1) < 0.001 ? outsideBorderWidth : borderWidth);
            _painter.DrawLine((Vector3d) d, (Vector3d) b, Math.Abs(d.X - 1) < 0.001 ? outsideBorderColor : borderColor,
                Math.Abs(d.X - 1) < 0.001 ? outsideBorderWidth : borderWidth);
            _painter.DrawLine((Vector3d) d, (Vector3d) c, Math.Abs(d.Z - 1) < 0.001 ? outsideBorderColor : borderColor,
                Math.Abs(d.Z - 1) < 0.001 ? outsideBorderWidth : borderWidth);
        }

        private void DrawSurfaceMaterial(GridSquare polygon)
        {
            _painter.DrawTriangle(polygon.LeftBottom, polygon.LeftBottomColor, polygon.RightUpper, polygon.RightUpperColor,
                polygon.RightBottom, polygon.RightBottomColor);
            _painter.DrawTriangle(polygon.LeftBottom, polygon.LeftBottomColor, polygon.LeftUpper, polygon.LeftUpperColor,
                polygon.RightUpper, polygon.RightUpperColor);
        }

        private void DrawAxis()
        {
            var axisPos = new Vector3d(-1.3, -1, -1);
            const double axisLen = 2.3;
            GL.PushMatrix();
            GL.Translate(axisPos);
            _painter.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(1, 0, 0), 0.01, axisLen, Color.Red);
            _painter.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(0.001, 1, 0), 0.01, axisLen, Color.Blue);
            _painter.DrawCylinder(new Vector3d(0, 0, 0), new Vector3d(0, 0, 1), 0.01, axisLen, Color.Green);
            GL.PopMatrix();
        }
    }
}