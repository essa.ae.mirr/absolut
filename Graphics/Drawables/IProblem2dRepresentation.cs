﻿using Algorithms;

namespace Graphics
{
    public interface IProblem2dRepresentation
    {
        string Name { get; }

        void Draw();
        void DrawAxis();
        void SetNewProblem(IProblem p);
        void SetSurface(int gridPointsCount, double[] x, double[] y, double[,] z);
    }
}