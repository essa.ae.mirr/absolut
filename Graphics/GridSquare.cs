﻿using System.Drawing;
using OpenTK;

namespace Graphics
{
    public class GridSquare
    {
        public  Vector3 LeftBottom;
        public  Vector3 LeftUpper;
        public  Vector3 RightBottom;
        public  Vector3 RightUpper;
        public readonly int X;
        public readonly int Y;

        public bool notGridOnly;

        public GridSquare(double[] x, double[] y, double[,] z, int xIndex, int yIndex)
        {
            X = xIndex;
            Y = yIndex;
            LeftBottom = new Vector3((float) x[xIndex], (float) z[xIndex, yIndex], (float) y[yIndex]);
            RightBottom = new Vector3((float) x[xIndex + 1], (float) z[xIndex + 1, yIndex], (float) y[yIndex]);
            LeftUpper = new Vector3((float) x[xIndex], (float) z[xIndex, yIndex + 1], (float) y[yIndex + 1]);
            RightUpper = new Vector3((float) x[xIndex + 1], (float) z[xIndex + 1, yIndex + 1], (float) y[yIndex + 1]);
            this.notGridOnly = true;
        }

        public GridSquare(Vector3 lu, Vector3 lb, Vector3 ru, Vector3 rb, int xIndex, int yIndex)
        {
            X = xIndex;
            Y = yIndex;
            LeftBottom = lb;
            RightBottom = rb;
            LeftUpper = lu;
            RightUpper = ru;
            this.notGridOnly = true;
        }
        public Color LeftUpperColor => !this.notGridOnly ? Utils.GetGreyColor(LeftUpper.Y, -1, 1) : Utils.GetColour(LeftUpper.Y, -1, 1);
        public Color LeftBottomColor => !this.notGridOnly ? Utils.GetGreyColor(LeftBottom.Y, -1, 1) : Utils.GetColour(LeftBottom.Y, -1, 1);
        public Color RightUpperColor => !this.notGridOnly ? Utils.GetGreyColor(RightUpper.Y, -1, 1) : Utils.GetColour(RightUpper.Y, -1, 1);
        public Color RightBottomColor => !this.notGridOnly ? Utils.GetGreyColor(RightBottom.Y, -1, 1) :  Utils.GetColour(RightBottom.Y, -1, 1);
    }
}