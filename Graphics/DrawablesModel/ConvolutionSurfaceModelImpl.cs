﻿using System.Collections.Generic;
using Algorithms;
using Properties;

namespace Graphics.DrawablesModel
{
    public class ConvolutionSurfaceModelImpl : ProblemaSurfaceModel, IPropertyUser
    {
        private IProblem _problem;
        private readonly List<GridSquare> _polygons = new List<GridSquare>();
        private readonly PropertyProvider _propertyProvider = new PropertyProvider();
        private double[] _x;
        private double[,] _y;
        private double[] _z;

        public IProblem Problem
        {
            get => _problem;
            set
            {
                if (_problem == value) return;
                _problem = value;
                FillPolygons();
            }
        }

        private void FillPolygons()
        {
            _polygons.Clear();

            int gridPoint = (int)GetProperty(RenderingConstants.GRID_POINTS_PROPERTY);
            double[] x = Utils.FillCoordArrays(_problem.LowerBound, _problem.UpperBound, 0, gridPoint),
                y = Utils.FillCoordArrays(_problem.LowerBound, _problem.UpperBound, 1, gridPoint);

            int funcCount = _problem.NumberOfFunctions;
            double[][,] zz = new double[funcCount][,];

            for (int i = 0; i < funcCount; i++)
            {
                var f = _problem.Functionals[i];
                zz[i] = new double[gridPoint, gridPoint];
                Utils.FillZCMatrix(gridPoint, gridPoint, zz, x, y, f, i);
            }
            
            Utils.AdjustTo01(x, y, zz[funcCount - 1], gridPoint);

            _x = x;
            _z = y;
            _y = zz[funcCount - 1];

            for (var xCell = 0; xCell < gridPoint - 1; xCell++)
            for (var yCell = 0; yCell < gridPoint - 1; yCell++)
            {
                bool f = true;
                for (int i = 0; i < (funcCount - 1); i++)
                {
                    if (((float)zz[i][xCell, yCell] > 0) ||
                        ((float)zz[i][xCell + 1, yCell] > 0) ||
                        ((float)zz[i][xCell, yCell + 1] > 0) ||
                        ((float)zz[i][xCell + 1, yCell + 1] > 0))
                        f = false;
                }

                if (f)
                    _polygons.Add(new GridSquare(_x, _z, _y, xCell, yCell));
            }
        }

        public List<GridSquare> GetGridSquares()
        {
            return _polygons;
        }

        public string Name => _propertyProvider.Name;

        public void SetProperty(string name, object value)
        {
            _propertyProvider.SetProperty(name, value);
            this.FillPolygons();
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            _propertyProvider.SetProperties(propsValues);
            this.FillPolygons();
        }

        public object GetProperty(string name)
        {
            return _propertyProvider.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _propertyProvider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            _propertyProvider.RegisterProperty(p);
        }
    }
}