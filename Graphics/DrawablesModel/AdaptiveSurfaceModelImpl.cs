﻿using System.Collections.Generic;
using Functions;
using OpenTK;
using Properties;

namespace Graphics.DrawablesModel
{
    public class AdaptiveSurfaceModelImpl : SurfaceModel, IPropertyUser

    {
        private readonly PropertyProvider _propertyProvider = new PropertyProvider();
        private readonly List<GridSquare> _adaptivePolygons = new List<GridSquare>();
        private double[] _max;
        private double[] _mins;
        private IFunction _function;
        private int _initialGridCount = 12;

        public IFunction Function
        {
            get => _function;
            set
            {
                if (_function != value)
                {
                    _function = value;
                    FillAdaptivePolygons();
                }
            }
        }

        public List<GridSquare> GetGridSquares()
        {
            return _adaptivePolygons;
        }

        public AdaptiveSurfaceModelImpl()
        {
            _propertyProvider.RegisterProperty(new IntProperty(RenderingConstants.ADAPTIVE_GRIP_POINTS_PROPERTY, 4, 14,
                2, 8));
        }


        private void FillAdaptivePolygons()
        {
            _adaptivePolygons.Clear();
            _initialGridCount = (int) GetProperty("Adaptive grid points");
            double[] x = Utils.FillCoordArrays(_function, 0, _initialGridCount),
                y = Utils.FillCoordArrays(_function, 1, _initialGridCount);
            var z = Utils.FillValues(_function, _initialGridCount);
            Utils.FillZMatrix(_initialGridCount, _initialGridCount, z, x, y, _function);
            Utils.GetInfo(x, y, z, out _mins, out _max, out _);
            for (var xCell = 0; xCell < _initialGridCount - 1; xCell++)
            for (var yCell = 0; yCell < _initialGridCount - 1; yCell++)
                _adaptivePolygons.Add(new GridSquare(x, y, z, xCell, yCell));
            var level = Utils.GetAverageGrad(_adaptivePolygons, _function);
            var splitContainer = _adaptivePolygons;
            var tempContainer = new Dictionary<GridSquare, List<GridSquare>>();
            for (var i = 0; i < 2; i++)
            {
                foreach (var p in splitContainer)
                    if (Utils.NeedToSpleetPolygon(p, _function, level))
                        Spleet(p, tempContainer);

                foreach (var t in tempContainer)
                {
                    splitContainer.Remove(t.Key);
                    splitContainer.AddRange(t.Value);
                }

                tempContainer.Clear();
                level = Utils.GetAverageGrad(_adaptivePolygons, _function);
            }

            Utils.AdjustTo01(_adaptivePolygons, _mins, _max);
        }

        private void Spleet(GridSquare gridSquare, Dictionary<GridSquare, List<GridSquare>> tmp)
        {
            var lu = gridSquare.LeftUpper;
            var lb = gridSquare.LeftBottom;
            var ru = gridSquare.RightUpper;
            var rb = gridSquare.RightBottom;
            var x = gridSquare.X;
            var y = gridSquare.Y;

            var left = (gridSquare.LeftUpper + gridSquare.LeftBottom) / 2;
            var right = (gridSquare.RightUpper + gridSquare.RightBottom) / 2;
            var bottom = (gridSquare.LeftBottom + gridSquare.RightBottom) / 2;
            var upper = (gridSquare.LeftUpper + gridSquare.RightUpper) / 2;
            var center = (left + right) / 2;

            var leftPoint = new List<double> {left.X, left.Z};
            var rightPoint = new List<double> {right.X, right.Z};
            var bottomPoint = new List<double> {bottom.X, bottom.Z};
            var upperPoint = new List<double> {upper.X, upper.Z};
            var centerPoint = new List<double> {center.X, center.Z};
            float[] values =
            {
                (float) _function.Calc(leftPoint), (float) _function.Calc(rightPoint),
                (float) _function.Calc(bottomPoint),
                (float) _function.Calc(upperPoint), (float) _function.Calc(centerPoint)
            };
            foreach (var v in values)
            {
                if (v < _mins[2])
                    _mins[2] = v;
                if (v > _max[2])
                    _max[2] = v;
            }

            left = new Vector3(left.X, values[0], left.Z);
            right = new Vector3(right.X, values[1], right.Z);
            bottom = new Vector3(bottom.X, values[2], bottom.Z);
            upper = new Vector3(upper.X, values[3], upper.Z);
            center = new Vector3(center.X, values[4], center.Z);


            var val = new List<GridSquare>
            {
                new GridSquare(left, lb, center, bottom, x, y),
                new GridSquare(center, bottom, right, rb, x, y),
                new GridSquare(lu, left, upper, center, x, y),
                new GridSquare(upper, center, ru, right, x, y)
            };
            tmp.Add(gridSquare, val);
        }

        public string Name => _propertyProvider.Name;

        public void SetProperty(string name, object value)
        {
            _propertyProvider.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            _propertyProvider.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return _propertyProvider.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _propertyProvider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            _propertyProvider.RegisterProperty(p);
        }
    }
}