﻿using System.Collections.Generic;
using Functions;
using Properties;

namespace Graphics.DrawablesModel
{
    public class SurfaceModelImpl : SurfaceModel, IPropertyUser
    {
        private readonly PropertyProvider _propertyProvider = new PropertyProvider();
        private IFunction _function;
        private readonly List<GridSquare> _polygons = new List<GridSquare>();
        private double[] _x;
        private double[,] _y;
        private double[] _z;

        public SurfaceModelImpl()
        {
            _propertyProvider.RegisterProperty(new IntProperty(RenderingConstants.GRID_POINTS_PROPERTY, 8, 64, 8, 42));
        }


        private void FillPolygons()
        {
            _polygons.Clear();

            int gridPoint = (int) GetProperty(RenderingConstants.GRID_POINTS_PROPERTY);
            double[] x = Utils.FillCoordArrays(_function, 0, gridPoint),
                y = Utils.FillCoordArrays(_function, 1, gridPoint);

            int funcCount = _function.GetNumberOfFunctions();
            double[][,] zz = new double[funcCount][,];

            for (int i = 0; i < funcCount; i++)
            {
                _function.SetFunctionNumber(i);
                zz[i] = new double[gridPoint, gridPoint];
                Utils.FillZCMatrix(gridPoint, gridPoint, zz, x, y, _function, i);
            }


            Utils.GetInfo(x, y, zz[funcCount - 1], out _, out _, out _);
            Utils.AdjustTo01(x, y, zz[funcCount - 1], gridPoint);

            _x = x;
            _z = y;
            _y = zz[funcCount - 1];

            for (var xCell = 0; xCell < gridPoint - 1; xCell++)
            for (var yCell = 0; yCell < gridPoint - 1; yCell++)
            {
                bool f = true;
                for (int i = 0; i < (funcCount - 1); i++)
                {
                    if (((float) zz[i][xCell, yCell] > 0) ||
                        ((float) zz[i][xCell + 1, yCell] > 0) ||
                        ((float) zz[i][xCell, yCell + 1] > 0) ||
                        ((float) zz[i][xCell + 1, yCell + 1] > 0))
                        f = false;
                }

                if (f)
                    _polygons.Add(new GridSquare(_x, _z, _y, xCell, yCell));
            }
        }

        public IFunction Function
        {
            get => _function;
            set
            {
                if (_function == value) return;
                _function = value;
                FillPolygons();
            }
        }

        public List<GridSquare> GetGridSquares()
        {
            return _polygons;
        }


        public string Name => "SurfaceModelImpl";

        public void SetProperty(string name, object value)
        {
            _propertyProvider.SetProperty(name, value);
            this.FillPolygons();
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            _propertyProvider.SetProperties(propsValues);
            this.FillPolygons();
        }

        public object GetProperty(string name)
        {
            return _propertyProvider.GetProperty(name);
           
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _propertyProvider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            _propertyProvider.RegisterProperty(p);
        }
    }
}