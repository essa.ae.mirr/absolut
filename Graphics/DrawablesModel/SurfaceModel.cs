﻿using System.Collections.Generic;
using Functions;

namespace Graphics.DrawablesModel
{
    public interface SurfaceModel
    {
       IFunction Function { get; set; }
        List<GridSquare> GetGridSquares();
        void SetProperty(string name, object value);
    }
}
