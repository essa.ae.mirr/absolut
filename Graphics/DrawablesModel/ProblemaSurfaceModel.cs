﻿using System.Collections.Generic;
using Algorithms;

namespace Graphics.DrawablesModel
{
    public interface ProblemaSurfaceModel
    {
       IProblem Problem { get; set; }
       List<GridSquare> GetGridSquares();

        void SetProperty(string name, object value);

    }
}