﻿using System.Collections.Generic;
using Algorithms;
using Functions;
using Properties;

namespace Graphics.DrawablesModel
{
    public class ProblemaSurfaceModelImpl : ProblemaSurfaceModel, IPropertyUser
    {
        private readonly List<GridSquare> _polygons = new List<GridSquare>();
        private readonly PropertyProvider _propertyProvider = new PropertyProvider();
        private double[] _x;
        private double[,] _y;
        private double[] _z;
        private IProblem _problem;
        private int _criteriaIndex;

        

        public ProblemaSurfaceModelImpl(int criteriaIndex = 0)
        {
            _propertyProvider.RegisterProperty(new IntProperty(RenderingConstants.GRID_POINTS_PROPERTY, 8, 64, 8, 42));
            this._criteriaIndex = criteriaIndex;
        }

        public IProblem Problem
        {
            get => _problem;
            set
            {
                if (_problem == value) return;
                _problem = value;
                FillPolygons();
            }
        }

        private void FillPolygons()
        {
            _polygons.Clear();

            // количество точек в сетке определяется пользователем через настройки
            int gridPoint = (int) GetProperty(RenderingConstants.GRID_POINTS_PROPERTY);

            // построение сетки на области определения
            double[] x = Utils.FillCoordArrays(_problem.LowerBound, _problem.UpperBound, 0, gridPoint),
                y = Utils.FillCoordArrays(_problem.LowerBound, _problem.UpperBound, 1, gridPoint);

            int funcCount = _problem.NumberOfConstraints+1;
            double[][,] zz = new double[funcCount][,];
            int j = 0;
            // заполнение матрицы значений функционалов в углах сетки
            for (j = 0; j < funcCount - 1; j++)
            {
                var f = _problem.Functionals[j];
                zz[j] = new double[gridPoint, gridPoint];
                Utils.FillZCMatrix(gridPoint, gridPoint, zz, x, y, f, j);
            }
            var criteria = _problem.Criterions[this._criteriaIndex];
            zz[j] = new double[gridPoint, gridPoint];
            var grayZz = new double[gridPoint, gridPoint];
            // матрица значения критерия для квадратов
            // в которых не выполняются ограничения
            for (int i = 0; i < gridPoint; i++)
            {
                for (int k = 0; k < gridPoint; k++)
                {
                    var point = new List<double> { 0, 0 };
                    point[0] = x[i];
                    point[1] = y[k];
                    var f = this._problem.Functionals[funcCount - 1];
                    grayZz[i,k] = f.Calc(point);
                }
            }
            Utils.FillZCMatrix(gridPoint, gridPoint, zz, x, y, criteria, j);
            // масштабирование сетки
            Utils.GetInfo(x, y, zz[funcCount - 1], out _, out _, out _);
            Utils.AdjustTo01(x, y, zz[funcCount - 1], gridPoint);
            Utils.AdjustTo01(x, y, grayZz, gridPoint);

            _x = x;
            _z = y;
            _y = zz[funcCount - 1];

            for (var xCell = 0; xCell < gridPoint - 1; xCell++)
            for (var yCell = 0; yCell < gridPoint - 1; yCell++)
            {
                bool f = true;
                // проверка значений функционалов в углах полигона
                for (int i = 0; i < (funcCount - 1); i++)
                {
                    if (((float) zz[i][xCell, yCell] > 0) ||
                        ((float) zz[i][xCell + 1, yCell] > 0) ||
                        ((float) zz[i][xCell, yCell + 1] > 0) ||
                        ((float) zz[i][xCell + 1, yCell + 1] > 0))
                        f = false;
                }

                if (f)
                {
                    this._polygons.Add(new GridSquare(_x, _z,grayZz, xCell, yCell) { notGridOnly = f });
                    }
                else
                {
                    this._polygons.Add(new GridSquare(_x, _z, grayZz, xCell, yCell) { notGridOnly = f });
                    }

            }
        }

        public List<GridSquare> GetGridSquares()
        {
            return _polygons;
        }
        

        public string Name => _propertyProvider.Name;

        public void SetProperty(string name, object value)
        {
            _propertyProvider.SetProperty(name, value);
            this.FillPolygons();
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            _propertyProvider.SetProperties(propsValues);
            this.FillPolygons();
        }

        public object GetProperty(string name)
        {
            return _propertyProvider.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _propertyProvider.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            _propertyProvider.RegisterProperty(p);
        }
    }
}