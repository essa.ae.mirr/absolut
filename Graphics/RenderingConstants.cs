﻿namespace Graphics
{
    public class RenderingConstants
    {
        public static string GRID_POINTS_PROPERTY = "Grid points";
        public static string ADAPTIVE_GRIP_POINTS_PROPERTY = "Adaptive grid points";

    }
}