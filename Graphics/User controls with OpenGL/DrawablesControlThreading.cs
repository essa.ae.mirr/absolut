﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Properties;
using ViewDemo.Views;
using Timer = System.Threading.Timer;

namespace Graphics
{
    public class DrawablesStateChecker
    {
        private DrawablesControlThreading _control;
        private bool _needRedraw = false;

        public DrawablesStateChecker(DrawablesControlThreading c)
        {
            _control = c;
        }

        public void UpdateTimerOnTick(object state)
        {
            if (!_control.NeedRedraw) return;
            if(_control.IsDisposed) return;
            if (_control.InvokeRequired)
                _control.Invoke(new Action(_control.Invalidate));
            else
                _control.Invalidate();
            _control.NeedRedraw = false;
        }
    }

    public class DrawablesControlThreading : GLControl, IPropertyUser
    {
        private const double RotationSpeed = Math.PI/2;

        private  Camera _camera;

        public Camera Camera
        {
            get { return _camera; }
            set { _camera = value; }
        }

        private IContainer components;

        private bool _isLoaded;
        private readonly Dictionary<String, List<Tuple<PropertyInfo, object>>> _parameters = new Dictionary<String, List<Tuple<PropertyInfo, object>>>();

        private double _lastUpdateTime;
        private bool _needRedraw;

        public bool NeedRedraw
        {
            get { return _needRedraw; }
            set { _needRedraw = value; }
        }

        protected readonly PropertyProvider Property = new PropertyProvider();
        protected readonly Dictionary<String, IDrawable> Drawables = new Dictionary<string, IDrawable>();
        protected readonly Dictionary<string, IPropertyUser> PropertyUsers = new Dictionary<string, IPropertyUser>();

        protected Button button1;
        protected ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem screenshotToolStripMenuItem;
        private readonly Timer _updateTimer;

        public DrawablesControlThreading() : base(new GraphicsMode(32, 24, 8, 4), 3, 0, GraphicsContextFlags.Default)
        {
            InitializeComponent();

            Load += OnLoad;
            Paint += OnPaint;
            Resize += OnResize;
            _camera = new Camera(new Vector3d(0, 3, 6));
            var checker = new DrawablesStateChecker(this);
            _updateTimer = new Timer(checker.UpdateTimerOnTick, null, 0, 32);
        }


        private void OnResize(object sender, EventArgs eventArgs)
        {
            if (DesignMode) return;
            if (!_isLoaded) return;
            MakeCurrent();
            var min = Math.Min(Height, Width);
            Camera.Viewport = new Viewport(min,min);
            
            OpenGlHelper.ResizeGLControl(this);
        }

        private void OnPaint(object sender, PaintEventArgs paintEventArgs)
        {
            if (DesignMode) return;
            if (!_isLoaded) return;
            MakeCurrent();
            _camera.Setup();
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            foreach (var d in Drawables)
            {
                d.Value.Draw();
            }

            SwapBuffers();
        }

        private void OnLoad(object sender, EventArgs eventArgs)
        {
            if (DesignMode) return;
            _camera.Viewport = new Viewport(Width, Height);
            OpenGlHelper.Init3dWithSimpleLight();
            _isLoaded = true;
        }

        public void AddDrawableObject(IDrawable o)
        {
            if (this.Drawables.ContainsKey(o.Name))
            {
                this.Drawables.Remove(o.Name);
            }
            Drawables.Add(o.Name, o);
            var propsUser = o as IPropertyUser;
            if (propsUser == null) return;
            if (this.PropertyUsers.ContainsKey(o.Name))
            {
                this.PropertyUsers.Remove(o.Name);
                PropertyUsers.Add(o.Name, propsUser);
                return;
            }
            PropertyUsers.Add(o.Name, propsUser);
            var added = new ToolStripMenuItem(o.Name);
            contextMenuStrip1.Items.Add(added);
            bool f = false;
            var listToAddProps = new List<Tuple<PropertyInfo, object>>();
            this._parameters.Add(o.Name, listToAddProps);
            
            foreach (var p in propsUser.GetPropertiesInfo())
            {
                if ((p is BoolProperty))
                {
                    var boolProperty = new ToolStripMenuItem(p.name)
                    {
                        Checked = (bool) propsUser.GetProperty(p.name),
                        CheckOnClick = true
                    };
                    boolProperty.CheckedChanged += BoolPropertyOnCheckedChanged;

                    added.DropDownItems.Add(boolProperty);
                }
                else if (p is ColorProperty)
                {
                    var colorProperty = new ToolStripMenuItem(p.name);

                    colorProperty.Click += ColorPropertyOnClick;

                    added.DropDownItems.Add(colorProperty);
                }
                else if (p is DoubleProperty)
                {
                    f = true;
                    listToAddProps.Add(new Tuple<PropertyInfo, object>(p, (double)propsUser.GetProperty(p.name)));
                   
                }
                else if (p is IntProperty)
                {
                    f = true;
                    listToAddProps.Add(new Tuple<PropertyInfo, object>(p, (int)propsUser.GetProperty(p.name)));
                }
            }
            if (f)
            {
                var paramsItem = new ToolStripMenuItem("Other...");

                paramsItem.Click += OnClick;

                added.DropDownItems.Add(paramsItem);
            }
        }

        private void OnClick(object sender, EventArgs eventArgs)
        {
            var me = sender as ToolStripMenuItem;
            if (me == null) return;
            var nameOfDrawableToChange = me.OwnerItem.Text;
            var propsUser = PropertyUsers[nameOfDrawableToChange];
            Dictionary<PropertyInfo, object> _props = new Dictionary<PropertyInfo, object>();
            foreach (var tuple
                in this._parameters[propsUser.Name])
            {
                _props.Add(tuple.Item1, tuple.Item2);
            }
            using (var k = new ParametersDialog(_props))
            {
                DialogResult dr = k.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    

                    propsUser.SetProperties(k.changedValues);
                    var parameter = this._parameters[propsUser.Name];
                    parameter.Clear();
                        foreach (var g in propsUser.GetPropertiesInfo())
                        {
                            if (g is DoubleProperty)
                            {
                                parameter.Add(new Tuple<PropertyInfo, object>(g, (double)propsUser.GetProperty(g.name)));
                            }
                            else if (g is IntProperty)
                            {
                            parameter.Add(new Tuple<PropertyInfo, object>(g, (int)propsUser.GetProperty(g.name)));
                        }
                        }
                   
                }
            }
            this.Refresh();
        }

        private void ColorPropertyOnClick(object sender, EventArgs eventArgs)
        {
            using (var paramsChoose = new ColorDialog())
            {
                DialogResult dr = paramsChoose.ShowDialog();
                if (dr == DialogResult.OK)
                {
                    var me = sender as ToolStripMenuItem;
                    if (me == null) return;
                    var nameOfDrawableToChange = me.OwnerItem.Text;
                    var propsUser = PropertyUsers[nameOfDrawableToChange];
                    propsUser.SetProperty(me.Text, paramsChoose.Color);
                   
                }
            }
            this.Refresh();
        }

        private void BoolPropertyOnCheckedChanged(object sender, EventArgs eventArgs)
        {
            var me = sender as ToolStripMenuItem;
            if (me == null) return;
            var nameOfDrawableToChange = me.OwnerItem.Text;
            var propsUser = PropertyUsers[nameOfDrawableToChange];

            propsUser.SetProperty(me.Text, me.Checked);
            
            this.Refresh();
        }

        private void InitializeComponent()
        {
            components = new Container();
            button1 = new Button();
            contextMenuStrip1 = new ContextMenuStrip(components);
            screenshotToolStripMenuItem = new ToolStripMenuItem();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Location = new Point(0, 0);
            button1.Margin = new Padding(4, 4, 4, 4);
            button1.Name = "button1";
            button1.Size = new Size(31, 28);
            button1.TabIndex = 0;
            button1.Text = "+";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // _propetiesMenuStrip
            // 
            contextMenuStrip1.ImageScalingSize = new Size(20, 20);
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new Size(61, 4);
            contextMenuStrip1.Items.AddRange(new ToolStripItem[] {
                screenshotToolStripMenuItem});

            // 
            // screenshotToolStripMenuItem
            // 
            screenshotToolStripMenuItem.Name = "screenshotToolStripMenuItem";
            screenshotToolStripMenuItem.Size = new Size(180, 22);
            screenshotToolStripMenuItem.Text = "Screenshot";
            screenshotToolStripMenuItem.Click += screenshotToolStripMenuItem_Click;
            // 
            // DrawablesControl
            // 
            AutoScaleDimensions = new SizeF(8F, 16F);
            Controls.Add(button1);
            Margin = new Padding(5, 5, 5, 5);
            Name = "DrawablesControl";
            ResumeLayout(false);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Button btnSender = (Button) sender;
            Point ptLowerLeft = new Point(0, btnSender.Height);
            ptLowerLeft = btnSender.PointToScreen(ptLowerLeft);
            contextMenuStrip1.Show(ptLowerLeft);
        }

        public void SetProperty(string name, object value)
        {
            Property.SetProperty(name, value);
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            Property.SetProperties(propsValues);
        }

        public object GetProperty(string name)
        {
            return Property.GetProperty(name);
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return Property.GetPropertiesInfo();
        }

        public void RegisterProperty(PropertyInfo p)
        {
            Property.RegisterProperty(p);
        }
        private void screenshotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Bitmap screenshot = GrabScreenshot();
            var filename = ParentForm.Text + DateTime.Now.ToString("MM/dd/yyyy HH-mm-ss")+ ".jpg";
            screenshot.Save(filename);
        }
    }
}