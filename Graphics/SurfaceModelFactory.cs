﻿using Algorithms;
using Functions;
using Graphics.DrawablesModel;

namespace Graphics
{
    public class SurfaceModelFactory
    {
        public static SurfaceModel BuildFunctionModel()
        {
            return new SurfaceModelImpl();
        }

        public static SurfaceModel BuildFunctionModel(IFunction f)
        {
            return new SurfaceModelImpl() {Function = f};
        }

        public static ProblemaSurfaceModel BuildProblemaModel(IProblem p)
        {
            return new ProblemaSurfaceModelImpl() {Problem = p};
        }
    }

}