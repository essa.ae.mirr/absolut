﻿namespace Algorithms
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;

    using Algorithms.Methods;

    using Functions;

    public struct MethodsNames
    {
        public const string Agp = "Agp";

        public const string Piyavsky = "Piyavskiy";

        public const string Bleic = "Bleic";
    }

    public static class AlgFactory
    {
        private static List<string> options = new List<string>
                                                  {
                                                      "Agp",
                                                      "Scanning alg",
                                                      "AGP_paral",
                                                      "Scanning_alg_paral",
                                                      "GeneticSimpleAlg",
                                                      "Piyavskiy",
                                                      "Reduction_AGP",
                                                      "DE",
                                                      MethodsNames.Bleic
                                                  };

        private static List<string> constrainedOptions = new List<string> { "Alglib AUL" };

        public static List<string> MoeaOptions { get; } = new List<string>()
                                                              {
                                                                  "DE",
                                                                  "NSGA2",
                                                                  "MOEAD",
                                                                  "GDE3",
                                                                  "NSGA3",
                                                                  "eNSGA2",
                                                                  "eMOEA",
                                                                  "CMAES",
                                                                  "SPEA2",
                                                                  "PAES",
                                                                  "PESA2",
                                                                  "OMOPSO",
                                                                  "SMPSO",
                                                                  "IBEA",
                                                                  "SMSEMOA",
                                                                  "VEGA",
                                                                  "DBEA",
                                                                  "RVEA",
                                                                  "MSOPS",
                                                                  "Random",
                                                                  "GA",
                                                                  "ES",
                                                                  "RSO"
                                                              };

        public static List<string> ConstrainedOptions
        {
            get => constrainedOptions;
            set => constrainedOptions = value;
        }

        public static List<string> Options
        {
            get => options;
            set => options = value;
        }

        public static ISearchAlg Build(string algName)
        {
            switch (algName)
            {
                case "Agp":
                    return new Agp() { Name = algName };
                case "Scanning alg":
                    return new ScanningAlg() { Name = algName };
                case "AGP_paral":
                    return new AGP_2() { Name = algName };
                case "Scanning_alg_paral":
                    return new ScanningAlgParal() { Name = algName };
                case "GeneticSimpleAlg":
                    return new GeneticSimpleAlg() { Name = algName };
                case "Piyavskiy":
                    return new Piyavskiy() { Name = algName };
                case MethodsNames.Bleic:
                    return new AlglibBleic() { Name = algName };
                case "DE":
                    return new MoeaAlg("DE");
                case "NSGA2":
                    return new MoeaAlg("NSGA2");
                case "MOEAD":
                    return new MoeaAlg("MOEAD");
                case "GDE3":
                    return new MoeaAlg("GDE3");
                case "NSGA3":
                    return new MoeaAlg("NSGA3");
                case "eNSGA2":
                    return new MoeaAlg("eNSGA2");
                case "eMOEA":
                    return new MoeaAlg("eMOEA");
                case "CMAES":
                    return new MoeaAlg("CMAES");
                case "SPEA2":
                    return new MoeaAlg("SPEA2");
                case "PAES":
                    return new MoeaAlg("PAES");
                case "PESA2":
                    return new MoeaAlg("PESA2");
                case "OMOPSO":
                    return new MoeaAlg("OMOPSO");
                case "SMPSO":
                    return new MoeaAlg("SMPSO");
                case "IBEA":
                    return new MoeaAlg("IBEA");
                case "SMSEMOA":
                    return new MoeaAlg("SMSEMOA");
                case "VEGA":
                    return new MoeaAlg("VEGA");
                case "DBEA":
                    return new MoeaAlg("DBEA");
                case "RVEA":
                    return new MoeaAlg("RVEA");
                case "MSOPS":
                    return new MoeaAlg("MSOPS");
                case "Random":
                    return new MoeaAlg("Random");
                case "GA":
                    return new MoeaAlg("GA");
                case "ES":
                    return new MoeaAlg("ES");
                case "RSO":
                    return new MoeaAlg("RSO");
            }

            throw new ArgumentException(algName);
        }

        public static ISearchAlg BuildConstrainedAlg(string algName)
        {
            switch (algName)
            {
                case "Alglib AUL":
                    return new AlglibAul();
            }

            throw new ArgumentException(algName);
        }

        public static ISearchAlg BuildExaminAlg(string dll, string conf, string examin)
        {
            ISearchAlg a = new ExaminAlg();
            //a.SetProperty(AbsolutAlgConstants.Exa, conf);
            a.SetProperty(AbsolutAlgConstants.ExaminDll, dll);
            a.SetProperty(AbsolutAlgConstants.ExaminPathToExe, examin);
            return a;
        }

        public static ISearchAlg BuildDiRectAlg(string dll, string conf, string examin)
        {
            // string outfile)
            ISearchAlg a = new DiRect();
            a.SetProperty("DiRect_conf", conf);
            a.SetProperty("DiRect_dll", dll);
            a.SetProperty("DiRect_exe", examin);
            a.SetProperty("outDiRect", "outDirect.txt");
            return a;
        }

        public static void SaveToXml(ISearchAlg a, string path)
        {
            XmlWriter writer = null;
            XmlWriterSettings settings =
                new XmlWriterSettings { Indent = true, IndentChars = "\t", OmitXmlDeclaration = true };

            // Create the XmlWriter object and write some content.
            writer = XmlWriter.Create(path, settings);
            const string StartElement = "Method";
            writer.WriteStartElement(StartElement);
            writer.WriteAttributeString("name", a.Name);
            writer.WriteStartElement("Problem");
            writer.WriteAttributeString("name", a.Problem.Name);
            if (a.Problem.ConfigPath != null)
            {
                writer.WriteAttributeString("config", a.Problem.ConfigPath);
            }

            if (a.Problem.DllPath != null)
            {
                writer.WriteAttributeString("dll", a.Problem.DllPath);
            }
               
            writer.WriteEndElement();
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
        }

        public static ISearchAlg OpenFromXml(string path)
        {
            XmlReader reader = XmlReader.Create(new FileStream(path, FileMode.Open));

            reader.ReadToFollowing("Method");
            reader.MoveToFirstAttribute();
            ISearchAlg a = Build(reader.Value);
            reader.ReadToFollowing("Problem");
            reader.MoveToFirstAttribute();
            if (reader.Value == "DLL")
            {
                reader.MoveToAttribute("dll");
                var dllPath = reader.Value;
                var hasConfig = reader.MoveToAttribute("config");
                var config = hasConfig ? reader.Value : null;
                a.Problem = ProblemFactory.BuildProblemFromDll(dllPath, config);
            }
            else
            {
                a.Problem = ProblemFactory.BuildNotConstrainedProblem(reader.Value);
            }

            return a;
        }

        public static ISearchAlg BuildExaminMcoAlg(
            string examinSettingsDllPath,
            string examinSettingsConfigPath,
            string examinSettingsExaminPath)
        {
            ISearchAlg a = new ExaminMcoAlg();
            a.SetProperty(AbsolutAlgConstants.ExaminPathToExe, examinSettingsExaminPath);
            a.SetProperty(AbsolutAlgConstants.ExaminDll, examinSettingsDllPath);
            a.SetProperty(AbsolutAlgConstants.OutputFile, "pls_output.txt");
            //a.Problem = ProblemFactory.BuildProblemFromDll(examinSettingsDllPath, string.Empty);
            return a;
        }
    }
}