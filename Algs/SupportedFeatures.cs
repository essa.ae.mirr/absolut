﻿namespace Algorithms
{
    public enum SupportedFeatures
    {
        Convolution,
        ParetoFront,
        SearchInformationReuse,
        InternalProblemGeneration
    }
}