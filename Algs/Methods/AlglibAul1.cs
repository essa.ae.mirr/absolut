﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Properties;
using sun.tools.tree;

namespace Algorithms.Methods
{
    public class AlglibAul: GenericAlg
    {
        private alglib.minnlcreport _minnlcreport;
        private const string StartingPoint = "Starting Point";
        private const string PenaltyCoef = "Penalty coefficient";
        private const string OuterIterations = "Outer iterations";
        private const string DiffStep = "Differntiation step size";

        public AlglibAul()
        {
            Name = "Alglib Aul";
            RegisterProperty(new PointProperty(StartingPoint, new List<double>()));
            RegisterProperty(new DoubleProperty("Precision", 0.000001, 0.1, 6, DoubleDeltaType.Log, 0.001));
            RegisterProperty(new DoubleProperty(PenaltyCoef,0, 1500, 1, DoubleDeltaType.Step, 1000));
            RegisterProperty(new IntProperty(OuterIterations, 0, 100, 1, 5));
            RegisterProperty(new DoubleProperty(DiffStep, 0.000001, 0.1, 1, DoubleDeltaType.Log, 0.00001));
            
        }

    private void ConstrainedProblemAdapter(double[] x, double[] fi, object obj)
    {
        if (!(Problem != null && Problem.NumberOfConstraints > 0 && Problem.NumberOfCriterions == 1))
            throw new NotSupportedException();
        // вычисление целевой функции в точке
        fi[0] = Problem.Criterions[0].Calc(x.ToList());
        // вычисление ограничений в точке
        for (int i = 1; i <= Problem.NumberOfConstraints; i++)
        {
            fi[i] = Problem.Constraints[i - 1].Calc(x.ToList());
        }

    }

    public  void IterationCallback(double[] x, double func, object obj)
    {
        var methodPoint = new MethodPoint()
        {
            EvolventX = 0,
            IdFun = 0,
            Iteration = CurrentIteration + 1,
           Criterions = new List<double>() { func},
            X = new List<double>(x)
        };
        ExperimentPointsReal.Add(methodPoint);

        ExperimentPoints.Add(methodPoint);
        CurrentIteration++;
    }

        public override void RecalcPoints()
        {
            if(Problem == null) throw new NotSupportedException();
            if (Problem.NumberOfCriterions > 1) throw new NotSupportedException();
            Restart();
            ExperimentPoints.Clear();
            ExperimentPointsReal.Clear();
            Init();

            double epsg = (double)GetProperty(StandartProperties.Precision);
            double epsf = 0;
            double epsx = 0;
            int maxits = (int)GetProperty(StandartProperties.MaxIter);

            //
            // This variable contains differentiation step
            //
            double diffstep = (double)GetProperty(DiffStep);
            double rho = (double) GetProperty(PenaltyCoef);
            int outeriters = (int) GetProperty(OuterIterations);
            //
            // Now we are ready to actually optimize something:
            // * first we create optimizer
            // * we add boundary constraints
            // * we tune stopping conditions
            // * and, finally, optimize and obtain results...
            //
            List<double> start = GetProperty(StartingPoint) as List<double>;
            alglib.minnlcstate state;

            alglib.minnlccreatef(Problem.Dimension, start.ToArray(), diffstep, out state);
            alglib.minnlcsetalgoaul(state,rho, outeriters);
            alglib.minnlcsetcond(state, epsg, epsf, epsx, maxits);
            alglib.minnlcsetbc(state, Problem.LowerBound.ToArray(), Problem.UpperBound.ToArray());
            alglib.minnlcsetnlc(state, 0, Problem.NumberOfConstraints);
            alglib.minnlcsetxrep(state,true);
            alglib.minnlcoptimize(state, ConstrainedProblemAdapter, IterationCallback, null);
            double[] result;
            alglib.minnlcresults(state, out result, out _minnlcreport);
            System.Console.WriteLine("{0}", alglib.ap.format(result, 2)); // EXPECTED: [1.0000,0.0000]
           
            //alglib.minbleicsetbc(alglib.minbleic.minbleicstate,Problem.LowerBound.ToArray() , Problem.UpperBound.ToArray());
            //alglib.minbleicsetcond(minbleicstate, epsg, epsf, epsx, maxits);
            //alglib.minbleicsetxrep(minbleicstate, true);
            //alglib.minbleicoptimize(minbleicstate, IFunctionAdapter, IterationCallback, null);

            //alglib.minbleicresults(minbleicstate, out result, out _minbleicreport);

        }

        private void Init()
        {
            // Problem = ProblemFactory.BuildProblem((IFunction)GetProperty(StandartProperties.MainFunction));
            var startPoint = new List<double>();

            for (int i = 0; i < Problem.LowerBound.Count; i++)
            {
                startPoint.Add(Problem.LowerBound[i] + Problem.UpperBound[i]/2);
            }
            SetPropertyWithoutRecalc(StartingPoint,startPoint);
            ExperimentPoints.Add(new MethodPoint()
            {
                IdFun = 1,
                Iteration = 0,
                IsEnd = AlgEnd.no,
                X = startPoint,
                Criterions = new List<double>() {Problem.CalculateFunctionals(startPoint, 0)},
                EvolventX = 0
            });
            ExperimentPointsReal.Add(new MethodPoint()
            {
                IdFun = 1,
                Iteration = 0,
                IsEnd = AlgEnd.no,
                X = startPoint,
                Criterions = new List<double>() {Problem.CalculateFunctionals(startPoint, 0)},
                EvolventX = 0
            });
            CurrentIteration = 0;
        }
    }
}