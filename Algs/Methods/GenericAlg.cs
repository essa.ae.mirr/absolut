﻿using System;
using System.Collections.Generic;
using System.Linq;
using com.sun.xml.@internal.ws.policy.privateutil;
using Functions;
using Properties;

namespace Algorithms.Methods
{
    [Serializable]
    public class GenericAlg : ISearchAlg, IPropertyUser
    {
        [NonSerialized] protected List<MethodPoint> ExperimentPoints;
        [NonSerialized] protected List<MethodPoint> ExperimentPointsReal;

        protected int IteratorPosition;
        protected bool IterMin;
        private readonly PropertyProvider _p = new PropertyProvider();
        protected int CurrentIteration = 0;
        private IProblem _problem;
        private List<SupportedFeatures> _features = new List<SupportedFeatures>();


        public GenericAlg()
        {
            ExperimentPointsReal = new List<MethodPoint>();
            ExperimentPoints = new List<MethodPoint>();
            RegisterProperty(new IntProperty(StandartProperties.MaxIter, 0, 10000, 10, 300));
        }

        protected void RegisterStandartProperties()
        {
            RegisterProperty(new DoubleProperty(AbsolutAlgConstants.Presicion, 0.000001, 0.1, 5,
                DoubleDeltaType.Log, 0.00001));
            RegisterProperty(new IntProperty(AbsolutAlgConstants.Dimension, 2, 2, 0, 2));
            RegisterProperty(new IntProperty(AbsolutAlgConstants.Evolvent, 8, 12, 1, 10));
            RegisterProperty(new DoubleProperty(AbsolutAlgConstants.R, 1.000001, 100, 100, DoubleDeltaType.Step, 2));
        }

        private int MaxIterations => (int) _p.GetProperty(StandartProperties.MaxIter);


        public void RegisterProperty(PropertyInfo info)
        {
            _p.RegisterProperty(info);
        }

        public string Name { get; set; }

        public void Restart()
        {
            IterMin = false;
            IteratorPosition = 0;
        }

        public void NextStep()
        {
            if (IteratorPosition + 1 <= CurrentIteration)
                IteratorPosition++;
        }

        public void PrevStep()
        {
            if (IteratorPosition > 0 && IteratorPosition <= CurrentIteration)
                IteratorPosition--;
        }

        public bool HasNext()
        {
            return IteratorPosition + 1 <= CurrentIteration;
        }

        public int CurrentIter()
        {
            return IteratorPosition;
        }

        public MethodPoint Argmin()
        {
            if (ExperimentPoints.Count == 0) throw new AccessViolationException("Experiment points is empty");
            var min = ExperimentPoints.Min(point => point.Criterions.FirstOrDefault());
            return ExperimentPoints.Find(point => Math.Abs(point.Criterions.FirstOrDefault() - min) < double.Epsilon);
        }

        public MethodPoint BestPoint()
        {
            if (IteratorPosition == 0) return null;
            if (ExperimentPointsReal.Count == 0) return null;
            var iter = 1;
            var y = double.MaxValue;
            for (var i = 0; i < MaxIterations && i < IteratorPosition; i++)
                if (ExperimentPointsReal[i].Criterions.FirstOrDefault() < y)
                {
                    y = (double) ExperimentPointsReal[i].Criterions.FirstOrDefault();
                    iter = i;
                }

            return ExperimentPointsReal[iter];
        }

        public List<MethodPoint> BestPoints()
        {
            throw new NotImplementedException();
        }

        public virtual void RecalcPoints()
        {
        }

        public IEnumerable<MethodPoint> CalculatedPoints(int count = 0)
        {
            return ExperimentPointsReal.AsReadOnly();
        }

        public MethodPoint LastPoint()
        {
            return ExperimentPointsReal[IteratorPosition];
        }

        public void ResetIterator()
        {
            IteratorPosition = 0;
        }

        public virtual void SetProperty(string name, object value)
        {
            SetPropertyWithoutRecalc(name, value);
            RecalcPoints();
        }

        public void SetProperties(Dictionary<string, object> propsValues)
        {
            foreach (var k in propsValues)
                SetPropertyWithoutRecalc(k.Key, k.Value);
            RecalcPoints();
        }

        public void SetPropertiesWithoutRecalc(Dictionary<string, object> propsValues)
        {
            foreach (var k in propsValues)
                SetPropertyWithoutRecalc(k.Key, k.Value);
        }

        public Object GetProperty(string name)
        {
            return _p.GetProperty(name);
        }

        public Dictionary<String, object> GetProperties()
        {
            return this._p.GetProperties();
        }

        public List<PropertyInfo> GetPropertiesInfo()
        {
            return _p.GetPropertiesInfo();
        }

        public virtual List<SupportedFeatures> GetSupportedFeatures()
        {
            return _features;
        }

        public virtual List<MethodPoint> GetParetoFront()
        {
            throw new NotSupportedException();
        }

        public virtual IFunction GetConvolution()
        {
            throw new NotSupportedException();
        }

        public bool WasSolved { get; set; }


        public virtual IProblem Problem
        {
            get => _problem;
            set => _problem = value;
        }

        public int IterationsPerformed => CurrentIteration;

        public void SetPropertyWithoutRecalc(string name, object value)
        {
            _p.SetProperty(name, value);
        }
    }
}