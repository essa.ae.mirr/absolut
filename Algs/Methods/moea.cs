﻿namespace Algorithms.Methods
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Functions;

    using global::Properties;

    using org.moeaframework;
    using org.moeaframework.core;
    using org.moeaframework.core.variable;
    using org.moeaframework.problem;

    public class MoeaAlg : GenericAlg
    {
        private List<SupportedFeatures> _features =
            new List<SupportedFeatures>() { SupportedFeatures.Convolution, SupportedFeatures.ParetoFront };

        private string _algName;

        private List<MethodPoint> _paretoFront = new List<MethodPoint>();

        public MoeaAlg(string algName)
        {
            this._algName = algName;
            ikvm.runtime.Startup.addBootClassPathAssemby(Assembly.Load("MOEAFramework-2.12"));
            RegisterProperty(new IntProperty(AbsolutAlgConstants.PopulationSize, 1, 10000, 10, 1000));
        }

        public override List<SupportedFeatures> GetSupportedFeatures()
        {
            return this._features;
        }

        public override IFunction GetConvolution()
        {
            return new MoeaConvolutionFunction(Problem);
        }

        public override List<MethodPoint> GetParetoFront()
        {
            return _paretoFront;
        }

        public override void RecalcPoints()
        {
            if (Problem == null) throw new InvalidOperationException(ExceptionMessages.ProblemIsNull);

            // Установка задачи оптимизации в класс-адаптер
            PseudoMoeaProblem._problem = Problem;

            // Запуск вычислений при помощи класса Executor. Точки испытания будут получены из статического поля
            // класса MoeaProblem (см. метод evalute)

            NondominatedPopulation result = new Executor()
                .withProblemClass(typeof(PseudoMoeaProblem))
                .withAlgorithm(this._algName)
                .withMaxEvaluations(100000)
                .withProperty("populationSize", (int)GetProperty(AbsolutAlgConstants.PopulationSize))
                .run();

            // Передача результатов из MoeaProblem в поля класса GenericAlg
            ExperimentPoints = PseudoMoeaProblem._points;
            ExperimentPointsReal = PseudoMoeaProblem._points;
            CurrentIteration = PseudoMoeaProblem.Iterations;
            foreach (Solution solution in result)
            {
                MethodPoint p = new MethodPoint();
                var criterions = solution.getObjectives().ToList();
                var constraints = solution.getConstraints().ToList();
                p.Criterions = criterions;
                p.Constraints = constraints;
                _paretoFront.Add(p);
            }
        }

        public class PseudoMoeaProblem : AbstractProblem
        {
            public static IProblem _problem;

            public static List<MethodPoint> _points = new List<MethodPoint>();

            public static int Iterations = 0;

            public PseudoMoeaProblem()
                : base(_problem.LowerBound.Count, _problem.NumberOfCriterions, _problem.NumberOfConstraints)
            {
                _points.Clear();
            }

            public override Solution newSolution()
            {
                // инициализация текущего решения
                Solution solution = new Solution(
                    getNumberOfVariables(),
                    getNumberOfObjectives(),
                    getNumberOfConstraints());

                for (int i = 0; i < getNumberOfVariables(); i++)
                {
                    solution.setVariable(i, new RealVariable(_problem.LowerBound[i], _problem.UpperBound[i]));
                }

                return solution;
            }

            public override void evaluate(Solution solution)
            {
                // Получение точки испытания из текущего решения
                double[] x = EncodingUtils.getReal(solution);

                // Вычисление значений критериев и ограничений в точке испытания
                double[] f = _problem.CalculateCriterions(x.ToList()).ToArray();
                double[] constr = _problem.CalculateConstraints(x.ToList()).ToArray();
                for (int i = 0; i < _problem.NumberOfConstraints; i++)
                {
                    constr[i] = constr[i] <= 0 ? 0 : constr[i];
                }

                // Добавление точки испытания в список вычисленных точек, который в дальнейшем
                // будет использован для сбора результатов работы алгоритма оптимизации
                _points.Add(
                    new MethodPoint()
                        {
                            EvolventX = 0,
                            IdProcess = 0,
                            IdFun = 0,
                            IsEnd = AlgEnd.no,
                            Iteration = Iterations,
                            X = x.ToList(),
                            Constraints = _problem.CalculateConstraints(x.ToList()),
                            Criterions = _problem.CalculateCriterions(x.ToList())
                        });

                // Передача вычисленной информации в MOEA Framework
                solution.setObjectives(f);
                solution.setConstraints(constr);
               
                Iterations++;
                
            }
        }

        class MoeaConvolutionFunction : MultidimFunction
        {
            private IProblem _problem;

            public MoeaConvolutionFunction(IProblem problem)
            {
                _problem = problem;
                Left = problem.LowerBound;
                Right = problem.UpperBound;
                Lambdas = new List<double>();
                for (int i = 0; i < problem.NumberOfCriterions; i++)
                    Lambdas.Add(1.0 / problem.NumberOfCriterions);
            }

            public override double Calc(List<double> arg)
            {
                double sum = 0;
                int lambdaIndex = 0;
                foreach (var problemCriterion in _problem.Criterions)
                {
                    sum += problemCriterion.Calc(arg) * Lambdas[lambdaIndex];
                    lambdaIndex++;
                }

                return sum;
            }
        }
    }
}