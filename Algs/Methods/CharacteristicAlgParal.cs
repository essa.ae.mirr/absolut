﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Functions;
using Properties;

namespace Algorithms.Methods
{
    class CharacteristicAlgParal : GenericAlg
    {
        protected int maxCharIndex;
        protected readonly List<Characteristic> CharacteristicList;

        protected CharacteristicAlgParal()
        {
            CharacteristicList = new List<Characteristic>();
            RegisterProperty(new DoubleProperty(StandartProperties.Precision, 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.00001));
            RegisterProperty(new IntProperty(StandartProperties.ProccessCount, 1, 4, 1, 1));
        }
        protected virtual double CalcCharacteristic(int curIntervalIndex)
        {
            return 0;
        }

        protected virtual double CalcNewPoint()
        {
            return 0;
        }
        public override void RecalcPoints()
        {
            if (Problem == null) throw new InvalidOperationException(ExceptionMessages.ProblemIsNull);
            var f = Problem.Criterions.FirstOrDefault();
            if (f == null) throw new InvalidOperationException(ExceptionMessages.ProblemWithoutCriterions);
            Restart();
            ExperimentPoints.Clear();
            Init();

            while (CurrentIteration <= (int)GetProperty(StandartProperties.MaxIter))
            {
                ExperimentPoints.Sort((p1, p2) => p1.EvolventX.CompareTo(p2.EvolventX));
                maxCharIndex = 0;
                
                CharacteristicList.Clear();
                for (int i = 0; i < CurrentIteration; i++)
                {
                    var R = CalcCharacteristic(i);

                }
                CharacteristicList.Sort((p1, p2) => p2.value_characteristic.CompareTo(p1.value_characteristic));

                int index = 0;
                if ((int)GetProperty(StandartProperties.ProccessCount) > CharacteristicList.Count) index = CharacteristicList.Count;
                else index = (int)GetProperty(StandartProperties.ProccessCount);


                for (int i = 0; i < index; i++){


                    double newPoint = CalcNewPoint();
                    if (IsEnded()) return;
                    maxCharIndex = i + 1;
                    ExperimentPointsReal.Add(new MethodPoint
                    {
                        EvolventX = newPoint,
                        IdFun = 0,
                        Iteration = CurrentIteration + 1,
                        Criterions= new List<double>() {f.Calc(newPoint)},
                        X = f.GetImage(newPoint),
                        IdProcess = i
                    });

                    ExperimentPoints.Add(new MethodPoint
                    {
                        EvolventX = newPoint,
                        IdFun = 0,
                        Iteration = CurrentIteration + 1,
                        Criterions = new List<double>() { f.Calc(newPoint) },
                        X = f.GetImage(newPoint),
                        IdProcess = i
                    });

                }
                CurrentIteration++;
            }
        }

        public virtual bool IsEnded()
        {
            return Math.Abs(CharacteristicList[maxCharIndex].right_x.EvolventX - CharacteristicList[maxCharIndex].left_x.EvolventX)
                   < (double)GetProperty(StandartProperties.Precision);
        }

        private void Init()
        {
            if (Problem == null) throw new InvalidOperationException(ExceptionMessages.ProblemIsNull);
            var f = Problem.Criterions.FirstOrDefault();
            if (f == null) throw new InvalidOperationException(ExceptionMessages.ProblemWithoutCriterions);
            ExperimentPoints.Add(new MethodPoint
            {
                IdFun = 1,
                Iteration = 0,
                IsEnd = AlgEnd.no,
                X = Problem.LowerBound,
                Criterions = new List<double>() {f.Calc(Problem.LowerBound)},
                EvolventX = 0
            });
            ExperimentPoints.Add(new MethodPoint
            {
                IdFun = 1,
                Iteration = 1,
                IsEnd = AlgEnd.no,
                X = Problem.UpperBound,
                Criterions = new List<double>() { f.Calc(Problem.UpperBound) },
                EvolventX = 1
            });
            CurrentIteration = 1;
        }
    }
}
