﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using com.sun.istack.@internal;

namespace Algorithms.Methods
{
    public static class AbsolutAlgConstants
    {
        private static string _processNum;
        public const String Presicion = "Precision";
        public const String ExaminDll = "Examin_dll";
        public const String ExaminPathToExe = "Examin_exe";

        public const String ExaminDllConf = "Examin_conf";
        public const String ExaminMethodName = "Examin";
        public const String ExaminMcoMethodName = "Examin MCO";
        public const String Dimension = "Dimension";
        public const String Evolvent = "Evolvent";
        public const String R = "r";
        public const String OutputFile = "OuputFile";
        public const String E = "E";
        public const String RLD = "rld";
        public const String M = "m";
        public const String NumberOfCriteria = "NumberOfCriteria";
        public const String FunctionNumber = "FunctionNumber";
        public const String ConstraintCount = "ConstraintCount";
        public const String ConstraintNum = "ConstraintNum";
        public const String Delta = "Delta";
        public const string LambdaList = "Lambda";
        public const string Reuse = "Reuse";
        public const string PopulationSize = "populationSize";
        public const string ProcessNum = "processNum";
        public const string PointsNum = "pointsNum";


    }
}
