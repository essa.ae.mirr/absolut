﻿using System.Diagnostics;
using System.Globalization;
using System.IO;
using NLog;
using Properties;

namespace Algorithms.Methods
{
    

    public class ExaminAlg : GenericAlg
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        

        public ExaminAlg()
        {
            // регистрация свойств
            Name = AbsolutAlgConstants.ExaminMethodName;
            RegisterProperty(new StringProperty(AbsolutAlgConstants.ExaminDll));
            RegisterProperty(new StringProperty(AbsolutAlgConstants.ExaminDllConf));
            RegisterProperty(new StringProperty(AbsolutAlgConstants.ExaminPathToExe));
            RegisterProperty(new StringProperty(AbsolutAlgConstants.OutputFile, @"out_point.txt"));
            RegisterProperty(new IntProperty(AbsolutAlgConstants.ProcessNum, 1, 8, 1, 1));
            RegisterProperty(new IntProperty(AbsolutAlgConstants.PointsNum, 1, 8, 1, 1));
            RegisterStandartProperties();
        }

        protected bool AreExaminPropertiesNotSet()
        {
            // для работы этого метода требуются пути к внешнему exe файлу системы
            // и dll-библиотеки, наличие которых проверяется в данном методе
            return string.Empty.Equals((string) GetProperty(AbsolutAlgConstants.ExaminDll)) ||
                   string.Empty.Equals((string) GetProperty(AbsolutAlgConstants.ExaminPathToExe));
        }

        public override void SetProperty(string name, object value)
        {
            // обновление свойств метода
            if (name == AbsolutAlgConstants.ExaminDll || name == AbsolutAlgConstants.ExaminPathToExe)
                SetPropertyWithoutRecalc(name, value);
            else
            {
                base.SetProperty(name, value);
            }
        }

        public override void RecalcPoints()
        {
            // запуск процесса пересчета точек испытаний
            _logger.Info("RecalcPoints started");
            _logger.Info("Examin properties not set?" + AreExaminPropertiesNotSet());
            if (AreExaminPropertiesNotSet()) return;
            _logger.Error("Process started");
            Restart();
            ExperimentPointsReal.Clear();
            StartProcessAndSolve();
            ParseExaminOutput();
            _logger.Error("Process ended");
        }

        protected virtual void ParseExaminOutput()
        {
            // расшифровка файла с выводом системы Globalizer
            var path = (string) GetProperty(AbsolutAlgConstants.OutputFile);
            int processNum = (int) GetProperty(AbsolutAlgConstants.ProcessNum);
            if (!File.Exists(path)) throw new FileNotFoundException();
            var parser = new ExaminOutputParser(Problem);
            var iteration = 0;
            using (var sr = File.OpenText(path))
            {
                var firstLine = sr.ReadLine();
                CurrentIteration = parser.ExtractInterations(firstLine);

                while (!sr.EndOfStream)
                {
                    var s = sr.ReadLine();
                    if(sr.EndOfStream)
                        continue;
                    var mp = parser.ParseExaminFileLine(s);
                    mp.Iteration = iteration;
                    iteration++;
                    mp.IdProcess = iteration % processNum;
                    ExperimentPointsReal.Add(mp);
                }
            }
        }

        protected void StartProcessAndSolve()
        {
            // запуск процесса внешней системы
            var t = BuildCommandLineArguments();
            RunExaminProcess(t);
        }

        protected virtual string BuildCommandLineArguments()
        {
            // сборка командной строки для запуска внешней системы с учетом значений параметров алгоритма
            return "-Dimension " + ((int) GetProperty(AbsolutAlgConstants.Dimension)) +
                   " -r " + ((double) GetProperty(AbsolutAlgConstants.R)).ToString(CultureInfo.InvariantCulture) +
                   " -Eps " + ((double) GetProperty(AbsolutAlgConstants.Presicion)).ToString(CultureInfo.InvariantCulture) +
                   " -m " + (int) GetProperty(AbsolutAlgConstants.Evolvent) +
                   " -MaxNumOfPoints " + (int) GetProperty("Maximum Iteration") +
                   " -sip " + (string) GetProperty(AbsolutAlgConstants.OutputFile) +
                   " - libConf " + @"""" + ((string) GetProperty(AbsolutAlgConstants.ExaminDllConf)).Replace("//", "/") + @"""" +
                   " -lib " + @"""" + ((string) GetProperty(AbsolutAlgConstants.ExaminDll)).Replace("//", "/") +
                   @"""";
        }

        protected virtual void RunExaminProcess(string arguments)
        {
            // cоздание процесса внешней системы и ожидание решения
           
            Process examinProcess = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = (string) GetProperty(AbsolutAlgConstants.ExaminPathToExe),
                    Arguments = arguments,
                    UseShellExecute = true,
                    RedirectStandardOutput = false,
                    CreateNoWindow = true
                }
            };

            examinProcess.Start();
            

            while (!examinProcess.HasExited) ;

            _logger.Info("RunExaminProcess has ended");
        }


    }
}