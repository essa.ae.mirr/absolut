﻿namespace Algorithms.Methods
{
    public class ScanningAlg : CharacteristicAlg
    {
        protected override double CalcNewPoint()
        {
            return 0.5*(ExperimentPoints[MaxCharIndex].EvolventX +
                        ExperimentPoints[MaxCharIndex - 1].EvolventX);
        }

        protected override double CalcCharacteristic(int curIntervalIndex)
        {
            
            var right = ExperimentPoints[curIntervalIndex + 1];
            var left = ExperimentPoints[curIntervalIndex];
            return right.EvolventX - left.EvolventX;

        }
    }
}