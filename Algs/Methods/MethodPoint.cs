﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Algorithms.Methods
{
    public enum AlgEnd
    {
        no,
        presicion,
        iter
    }

    public class MethodPoint
    {
        // информация для индексного метода решения - на каком номере функции остановился расчет в этой точке

        public int IdFun { get; set; }

        // флаг окончания процесса поиска минимума на данной точке

        public AlgEnd IsEnd { get; set; }

        // координаты точки

        public List<double> X { get; set; }
        // образ на развертке Пеано

        public double EvolventX { get; set; }
       

        // номер итерации

        public int Iteration { get; set; }

        // номер процесса

        public int IdProcess { get; set; }

        // значения критериев в точке

        public List<double> Criterions { get; set; }

        // значения ограничений в точке
        public List<double> Constraints { get; set; }

        public double? Convolution { get; set; }

        public MethodPoint()
        {
            X = new List<double>();
            IdProcess = 0;
        }

        public override string ToString()
        {
            var stringX = X.Select(f => f.ToString("N2"));
            var stringY = Criterions.Select(f => f.ToString("N2"));
            return "( " + String.Join(", ", stringX) + ", " + String.Join(", ", stringY) + ")";
        }
        
    }
}