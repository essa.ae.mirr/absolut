﻿using System;
using System.Linq;
using Properties;

namespace Algorithms.Methods
{
    using System.Collections.Generic;

    using Functions;

    [Serializable]
    public class Agp:CharacteristicAlg
    {
        private readonly List<SupportedFeatures> _features =
            new List<SupportedFeatures>()
                {
                    
                    SupportedFeatures.Convolution
                   
                };
        private double m;

        private IProblem problem;


        public Agp()
        {
            Name = MethodsNames.Agp;
            RegisterProperty(new DoubleProperty("r", 1.000001, 100,100,DoubleDeltaType.Step, 2));
            var lambdas = new List<double>();
           

            RegisterProperty(new ListProperty<double>(AbsolutAlgConstants.LambdaList, lambdas));
        }

        protected override double CalcNewPoint()
        {
            return (double) (0.5 * (ExperimentPoints[MaxCharIndex].EvolventX +
                                    ExperimentPoints[MaxCharIndex - 1].EvolventX) -
                             (ExperimentPoints[MaxCharIndex].Criterions.FirstOrDefault() - ExperimentPoints[MaxCharIndex - 1].Criterions.FirstOrDefault()) / (2 * m));
        }

        protected override double CalcCharacteristic(int curIntervalIndex)
        {
            if (curIntervalIndex == 0)
            {
                var d = new double[CurrentIteration];
                for (int i = 0; i < CurrentIteration; i++)
                {
                    var right = ExperimentPoints[i + 1];
                    var left = ExperimentPoints[i];
                    d[i] = Math.Abs((double) ((right.Criterions.FirstOrDefault() - left.Criterions.FirstOrDefault()) / (right.EvolventX - left.EvolventX)));
                }
                var M = d.Max();
                 m = M > 0 ? M * (double)GetProperty("r") : 1;
            }

            var point = ExperimentPoints[curIntervalIndex + 1];
            var methodPoint = ExperimentPoints[curIntervalIndex];
            var methodPointY = point.Criterions.FirstOrDefault();
            var methodPointX = methodPoint.Criterions.FirstOrDefault();
            return (double) (m * (point.EvolventX - methodPoint.EvolventX) +
                             ((methodPointY - methodPointX) * (methodPointY - methodPointX)) / (m * (point.EvolventX - methodPoint.EvolventX)) -
                             2 * (methodPointY + methodPointX));
        }

        public override List<SupportedFeatures> GetSupportedFeatures()
        {
            return this._features;
        }

      
        public override IFunction GetConvolution()
        {
            if (Problem == null) return null;
            return new ConvolutionFunction(Problem)
                       {
                           Lambdas = new List<double>() { 0.5, 0.5}
                       };
        }

        private class ConvolutionFunction : MultidimFunction
        {
            private IProblem _problem;

            public ConvolutionFunction(IProblem problem)
            {
                _problem = problem;
                Left = problem.LowerBound;
                Right = problem.UpperBound;
            }

            public override double Calc(List<double> arg)
            {
                double sum = 0;
                int lambdaIndex = 0;
                foreach (var problemCriterion in _problem.Criterions)
                {
                    sum += (problemCriterion.Calc(arg) * Lambdas[lambdaIndex]);
                    lambdaIndex++;
                }

                return sum;
            }


        }
    }
}