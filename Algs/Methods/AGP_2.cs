﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Properties;

namespace Algorithms.Methods
{
    class AGP_2 : CharacteristicAlgParal
    {
        private double m;
        public AGP_2()
        {
            Name = "AGP_2";
            RegisterProperty(new DoubleProperty("r", 1.000001, 100, 100, DoubleDeltaType.Step, 2));
        }

        protected override double CalcNewPoint()
        {
            //return 0.5 * (ExperimentPoints[maxCharIndex].EvolventX +
            //                       ExperimentPoints[maxCharIndex - 1].EvolventX) -
            //                      (ExperimentPoints[maxCharIndex].y - ExperimentPoints[maxCharIndex - 1].y) / (2 * m);
            return (double) (0.5 * (CharacteristicList[maxCharIndex].right_x.EvolventX + CharacteristicList[maxCharIndex].left_x.EvolventX) -
                             (CharacteristicList[maxCharIndex].right_x.Criterions.FirstOrDefault() - CharacteristicList[maxCharIndex].left_x.Criterions.FirstOrDefault()) / (2 * m));
        }

        protected override double CalcCharacteristic(int curIntervalIndex)
        {
            if (curIntervalIndex == 0)
            {
                var d = new double[CurrentIteration];

                for (int i = 0; i < CurrentIteration; i++)
                {
                    var right = ExperimentPoints[i + 1];
                    var left = ExperimentPoints[i];
                    d[i] = Math.Abs((double) ((right.Criterions.FirstOrDefault() - left.Criterions.FirstOrDefault()) / (right.EvolventX - left.EvolventX)));

                }

                var M = d.Max();
                m = M > 0 ? M * (double)GetProperty("r") : 1;
            }

            var point = ExperimentPoints[curIntervalIndex + 1];
            var methodPoint = ExperimentPoints[curIntervalIndex];
            CharacteristicList.Add(new Methods.Characteristic()
            {
                EvolventX = point.EvolventX,
                right_x = point,
                sort_x_interval = curIntervalIndex,
                left_x = methodPoint,
                value_characteristic = (double) (m * (point.EvolventX - methodPoint.EvolventX) +
                                                 ((point.Criterions.FirstOrDefault() - methodPoint.Criterions.FirstOrDefault()) * (point.Criterions.FirstOrDefault() - methodPoint.Criterions.FirstOrDefault())) / (m * (point.EvolventX - methodPoint.EvolventX)) -
                                                 2 * (point.Criterions.FirstOrDefault() + methodPoint.Criterions.FirstOrDefault()))
            });
            return (double) (m * (point.EvolventX - methodPoint.EvolventX) +
                             ((point.Criterions.FirstOrDefault() - methodPoint.Criterions.FirstOrDefault()) * (point.Criterions.FirstOrDefault() - methodPoint.Criterions.FirstOrDefault())) / (m * (point.EvolventX - methodPoint.EvolventX)) -
                             2 * (point.Criterions.FirstOrDefault() + methodPoint.Criterions.FirstOrDefault()));
        }
    }
}

