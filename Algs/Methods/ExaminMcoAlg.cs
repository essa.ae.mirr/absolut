﻿using System;

using NLog;

namespace Algorithms.Methods
{
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.IO;
    using System.Linq;

    using Functions;

    using global::Properties;

    public class ExaminMcoAlg : ExaminAlg
    {
        protected StreamWriter _examinInput;

        protected Process _examinProcess;

        private const string ParetoFrontFileName = "optim_pareto.txt";

        private const string Delimeter = "_";

        private List<MethodPoint> _paretoFrontPoints = new List<MethodPoint>();

        // поддерживаемая данным методом специальная функциональность
        private readonly List<SupportedFeatures> _features =
            new List<SupportedFeatures>()
                {
                    SupportedFeatures.ParetoFront,
                    SupportedFeatures.Convolution,
                    SupportedFeatures.InternalProblemGeneration
                };

        private IProblem _problem1;

        private bool _waitingForProcess = false;

        private static Logger _logger = LogManager.GetCurrentClassLogger();

        private List<double> _labmda = new List<double>();

        public ExaminMcoAlg()
        {
            // регистрация свойств
            Name = AbsolutAlgConstants.ExaminMcoMethodName;
            RegisterProperty(new DoubleProperty(AbsolutAlgConstants.E, 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.025));
            RegisterProperty(new IntProperty(AbsolutAlgConstants.RLD, -100, 100, 1, -1));
            RegisterProperty(new IntProperty(AbsolutAlgConstants.M, 1, 20, 1, 12));
            var criterionsCount = 3;
            RegisterProperty(
                new IntProperty(AbsolutAlgConstants.NumberOfCriteria, 0, int.MaxValue, 1, criterionsCount));
            RegisterProperty(new ListProperty<int>(AbsolutAlgConstants.FunctionNumber, new List<int>() { 1, 2, 3 }));
            RegisterProperty(new IntProperty(AbsolutAlgConstants.ConstraintCount, 0, int.MaxValue, 1, 2));
            RegisterProperty(new ListProperty<int>(AbsolutAlgConstants.ConstraintNum, new List<int>() { 4, 5 }));
            RegisterProperty(
                new DoubleProperty(AbsolutAlgConstants.Delta, 0.000001, 0.1, 5, DoubleDeltaType.Log, 0.025));
            var lambdas = new List<double>();
            for (int i = 0; i < criterionsCount; i++)
            {
                lambdas.Add(1.0 / criterionsCount);
            }

            RegisterProperty(new ListProperty<double>(AbsolutAlgConstants.LambdaList, lambdas));
            RegisterProperty(new BoolProperty(AbsolutAlgConstants.Reuse, true));
            SetProperty(AbsolutAlgConstants.R, 5.6);

            _logger.Info("Examin MCO alg was created");
        }

        public override IProblem Problem
        {
            get => _problem1;

            set
            {
                // специальная внутренняя генерация задачи с учетом свойств-номеров функций
                _logger.Info("Set Problem started");
                try
                {
                    var constraints = (List<int>)GetProperty(AbsolutAlgConstants.ConstraintNum);
                    var criterions = (List<int>)GetProperty(AbsolutAlgConstants.FunctionNumber);
                    _problem1 = ProblemFactory.BuildProblem(value, criterions, constraints);
                    if ((bool)GetProperty(AbsolutAlgConstants.Reuse))
                    {
                        if (_examinProcess != null)
                        {
                            _examinInput.WriteLine("quit");
                        }
                    }
                }
                catch (Exception e)
                {
                    _logger.Error(e);
                    throw;
                }

                _logger.Info("Set Problem Ended");
            }
        }

        protected override string BuildCommandLineArguments()
        {
            // сборка параметров командной строки для запуска внешней системы
            var buildCommandLineArguments =
                "-E "
                + ((double)GetProperty(AbsolutAlgConstants.E)).ToString(
                    "0.00",
                    System.Globalization.CultureInfo.InvariantCulture) + " -tm MCO_Method" + " -np 1" + " -N 2"
                + " -rld " + (int)GetProperty(AbsolutAlgConstants.RLD) + " -m "
                + (int)GetProperty(AbsolutAlgConstants.M) + " -r "
                + ((double)GetProperty(AbsolutAlgConstants.R)).ToString(
                    "0.00",
                    System.Globalization.CultureInfo.InvariantCulture) + " -lib "
                + ((string)GetProperty(AbsolutAlgConstants.ExaminDll)) + " -NumberCriteria "
                + (int)GetProperty(AbsolutAlgConstants.NumberOfCriteria) + " -function_number "
                + ((List<int>)GetProperty(AbsolutAlgConstants.FunctionNumber)).Select(s => s.ToString())
                .Aggregate((i, j) => i + Delimeter + j) + " -constraint_count "
                + (int)GetProperty(AbsolutAlgConstants.ConstraintCount) + " -constraintNums  "
                + ((List<int>)GetProperty(AbsolutAlgConstants.ConstraintNum)).Select(s => s.ToString())
                .Aggregate((i, j) => i + Delimeter + j) + " -delta " + (double)GetProperty(AbsolutAlgConstants.Delta)
                + " -sip " + (string)GetProperty(AbsolutAlgConstants.OutputFile);
            _logger.Error("Command line args " + buildCommandLineArguments);
            return buildCommandLineArguments;
        }

        public override void RecalcPoints()
        {
            // пересчет точек испытания
            _logger.Info("Recalc Points started");

            if (!AreExaminPropertiesNotSet())
            {
                this._labmda.Clear();
                if (Problem == null)
                {
                    string dll = (string)GetProperty(AbsolutAlgConstants.ExaminDll);
                    var buildProblemFromDll = ProblemFactory.BuildProblemFromDll(dll, string.Empty);
                    var constraints = (List<int>)GetProperty(AbsolutAlgConstants.ConstraintNum);
                    var criterions = (List<int>)GetProperty(AbsolutAlgConstants.FunctionNumber);
                    Problem = ProblemFactory.BuildProblem(buildProblemFromDll, criterions, constraints);
                }

                base.RecalcPoints();
                ParseParetoFrontFile();
            }

            _logger.Info("Recalc Points ended");
        }

        protected override void ParseExaminOutput()
        {
            // расшифровка выходного файла внешней системы
            var path = (string)GetProperty(AbsolutAlgConstants.OutputFile);
            int processNum = (int)GetProperty(AbsolutAlgConstants.ProcessNum);
            if (!File.Exists(path)) throw new FileNotFoundException();
            var parser = new ExaminOutputParser(Problem);
            var iteration = 0;
            using (var sr = File.OpenText(path))
            {
                var firstLine = sr.ReadLine();
                CurrentIteration = parser.ExtractInterations(firstLine);

                while (!sr.EndOfStream)
                {
                    var s = sr.ReadLine();
                    if (sr.EndOfStream)
                        continue;
                    var mp = parser.ParseExaminFileLine(s);
                    mp.Iteration = iteration;
                    iteration++;
                    mp.IdProcess = iteration % processNum;
                    var calculatedCriterions = this.Problem.CalculateCriterions(mp.X);
                    for (int i = 0; i < Problem.NumberOfCriterions; i++)
                    {
                        mp.Criterions.Add(calculatedCriterions[i]);
                    }

                    mp.Convolution = calculatedCriterions[0];
                    ExperimentPointsReal.Add(mp);
                }
            }
        }

        public override IFunction GetConvolution()
        {
            // создание функции-свертки для многокритериальной задачи
            if (Problem == null) return null;
            return new ExaminMcoConvolutionFunction(Problem)
                       {
                           Lambdas = (List<double>)GetProperty(
                               AbsolutAlgConstants.LambdaList)
                       };
        }

        protected override void RunExaminProcess(string arguments)
        {
            // запуск процесса внешней системы
            _logger.Info("Run Examin Process started");
            if (_examinProcess == null || _examinProcess.HasExited)
            {
                CreateProcess(arguments);
            }
            else
            {
                this._examinInput.WriteLine(this.BuildCommandLineArguments());
                this._examinInput.Flush();
                this._waitingForProcess = true;
            }

            while (this._waitingForProcess)
            {
                if (!_examinProcess.HasExited) continue;
                _logger.Error("Something went wrong with examin_mco_absolut_wrapper, exiting");
                return;
            }

            _logger.Info("Run Examin Process ended");
        }

        private void CreateProcess(string arguments)
        {
            // создание процесса и перенаправление потоков ввода-вывода
            _logger.Info("CreateProcess started");
            try
            {
                var processStartInfo = new ProcessStartInfo
               {
                   FileName =
                       (string)this.GetProperty(
                           AbsolutAlgConstants.ExaminPathToExe),
                   Arguments = arguments,
                   UseShellExecute = false,
                   // включение перенаправления потоков ввода-вывода
                   RedirectStandardOutput = true,
                   RedirectStandardInput = true,
                   CreateNoWindow = false
               };
                _examinProcess = new Process { StartInfo = processStartInfo };
                if ((bool)GetProperty(AbsolutAlgConstants.Reuse))
                {
                    // обработка при получение вывода
                    _examinProcess.OutputDataReceived += ExaminProcessOnOutputDataReceived;
                    this._waitingForProcess = true;
                    _examinProcess.Start();
                    _examinProcess.BeginOutputReadLine();
                    // сохранения потока ввода
                    this._examinInput = _examinProcess.StandardInput;
                }
                else
                {
                    _examinProcess.Start();
                    _examinProcess.WaitForExit();
                }
            }
            catch (Exception e)
            {
                _logger.Error(e, "Something went wrong during process creation");
            }

            _logger.Info("CreateProcess ended");
        }

        private void ExaminProcessOnOutputDataReceived(object sender, DataReceivedEventArgs dataReceivedEventArgs)
        {
            // обработка данных из потока вывода внешней системы
            _logger.Info("ExaminProcessOnOutputDataReceived started");
            _logger.Debug(dataReceivedEventArgs.Data);
            if (dataReceivedEventArgs.Data == null) return;

            // считывания значений параметров свертки
            if (dataReceivedEventArgs.Data.StartsWith("Lambda ")
                && this._labmda.Count < (int)this.GetProperty(AbsolutAlgConstants.NumberOfCriteria))
            {
                CultureInfo culture = new CultureInfo("en");
                var s = dataReceivedEventArgs.Data.Split(' ')[1];
                this._labmda.Add(double.Parse(s, culture));
            }
            // внешняя система завершила работу и ожидает ввода новых параметров
            if (dataReceivedEventArgs.Data == "[Input]")
            {
                this._waitingForProcess = false;
                _logger.Info("Recieved [Input] from examin mco, exiting waiting mod");
            }

            _logger.Info("ExaminProcessOnOutputDataReceived ended");
        }

        public override void SetProperty(string name, object value)
        {
            // обновление свойств метода
            base.SetPropertyWithoutRecalc(name, value);
            if (_examinProcess != null)
            {
                _examinInput.WriteLine(
                    GetProperty(AbsolutAlgConstants.ExaminPathToExe) + " " + BuildCommandLineArguments());
            }

            RecalcPoints();
        }

        private void ParseParetoFrontFile()
        {
            // обработка выходного файла с точками границы Парето
            _logger.Info("ParseParetoFrontFile started");
            var path = ParetoFrontFileName;
            if (!File.Exists(path))
            {
                _logger.Warn("Pareto file not found");
                return;
            }

            ;
            var parser = new ExaminOutputParser(Problem);
            using (var sr = File.OpenText(path))
            {
                while (!sr.EndOfStream)
                {
                    var s = sr.ReadLine();
                    if (sr.EndOfStream)
                        continue;
                    var mp = parser.ParseParetoFrontFileLine(s);
                    mp.IdProcess = mp.Iteration % (int)this.GetProperty(AbsolutAlgConstants.PointsNum);
                    mp.Criterions = new List<double>();
                    var calculatedCriterions = this.Problem.CalculateCriterions(mp.X);
                    if (this._labmda.Count > 0)
                    {
                        for (int i = 0; i < Problem.NumberOfCriterions; i++)
                        {
                            mp.Criterions.Add(this._labmda[i] * calculatedCriterions[i]);
                        }

                        mp.Convolution = calculatedCriterions[0];
                    }

                    _paretoFrontPoints.Add(mp);
                }
            }

            _logger.Info("ParseParetoFrontFile ended");
        }

        public override List<MethodPoint> GetParetoFront()
        {
            return _paretoFrontPoints;
        }

        public override List<SupportedFeatures> GetSupportedFeatures()
        {
            return _features;
        }

        private class ExaminMcoConvolutionFunction : MultidimFunction
        {
            private IProblem _problem;

            public ExaminMcoConvolutionFunction(IProblem problem)
            {
                _problem = problem;
                Left = problem.LowerBound;
                Right = problem.UpperBound;
            }

            public override double Calc(List<double> arg)
            {
                double sum = 0;
                int lambdaIndex = 0;
                foreach (var problemCriterion in _problem.Criterions)
                {
                    sum += problemCriterion.Calc(arg) * Lambdas[lambdaIndex];
                }

                return sum;
            }
        }
    }
}