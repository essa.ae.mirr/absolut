﻿using System.Collections.Generic;
using Algorithms.Methods;
using Functions;
using Properties;

namespace Algorithms
{
    public interface ISearchAlg
    {
        string Name { get; }
        int IterationsPerformed { get; }
        IProblem Problem { get; set; }
        void NextStep();
        void PrevStep();
        bool HasNext();
        int CurrentIter();
        MethodPoint LastPoint();
        MethodPoint BestPoint();
        void RecalcPoints();
        IEnumerable<MethodPoint> CalculatedPoints(int start = 0);

        void SetProperty(string name, object value);
        void SetProperties(Dictionary<string, object> propsValues);
        void SetPropertyWithoutRecalc(string name, object value);
        void SetPropertiesWithoutRecalc(Dictionary<string, object> propsValues);
        object GetProperty(string name);
        List<PropertyInfo> GetPropertiesInfo();
        // получение списка поддерживаемых операций
        List<SupportedFeatures> GetSupportedFeatures();
        // формирование области Парето
        List<MethodPoint> GetParetoFront();
        // доступ к фунцкии свертки
        IFunction GetConvolution();

        void ResetIterator();
    }
}