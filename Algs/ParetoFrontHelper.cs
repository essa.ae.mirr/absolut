﻿namespace Algorithms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Algorithms.Methods;

    public static class ParetoFrontHelper
    {
        public static List<MethodPoint> LazyPointsSetForParetoFront(IProblem problem)
        {
            List<MethodPoint> points = new List<MethodPoint>();
            int gridSize = 100;
            var steps = new double[problem.Dimension];
            for (int i = 0; i < problem.Dimension; i++)
            {
                steps[i] = (problem.UpperBound[i] - problem.LowerBound[i]) / (gridSize - 1);
            }

            for (int i = 0; i < gridSize; i++)
            {
                for (int j = 0; j < gridSize; j++)
                {
                    MethodPoint p = new MethodPoint
                                        {
                                            X = new List<double>(2)
                                                    {
                                                        problem.LowerBound[0] + i * steps[0],
                                                        problem.LowerBound[1] + i * steps[1]
                                                    }
                                        };
                    p.Criterions = problem.CalculateCriterions(p.X);
                    points.Add(p);
                }
            }

            return points;
        }

        public static List<MethodPoint> LazyApproximateParetoFront(IProblem problem)
        {
            if (problem.Dimension != 2)
            {
                throw new NotSupportedException();
            }

            if (problem.NumberOfConstraints > 0)
            {
                return new List<MethodPoint>();
            }

            List<MethodPoint> points = LazyPointsSetForParetoFront(problem);

            return ParetoFront(points);
        }

        public static List<MethodPoint> ParetoFront(List<MethodPoint> set)
        {
           
            var current = new List<MethodPoint>();
            var accepted = new List<MethodPoint>();
            int size = set.Count;
            int cntFunc = set.First().Criterions.Count;
            int dim = set.First().X.Count;
            int cntPareto = 0;
            int cntTmp = 0;
            foreach (var methodPoint in set)
            {
          
            //                        SIM_Value* sim = it->simVall;
            //                        for (int iim = 0; iim < dim; iim++)
            //                        {
            //                            pareto[cnt_pareto].img[iim] = sim->y[iim];
            //                        }
            //                        for (int ifn = 0; ifn < cntFnc; ifn++)
            //                        {
            //                            pareto[cnt_pareto].fValue[ifn] = sim->calcedValue[ifn];
            //                        }

                    //                        cnt_tmp = 0;
                    //                        bool f = true;
                    //                        for (int k = 0; k < cnt_pareto; k++)
                    //                        {
                    //                            if (!(pareto[cnt_pareto] < pareto[k]))
                    //                            {
                    //                                pareto_tmp[cnt_tmp] = pareto[k];
                    //                                cnt_tmp++;
                    //                                if (cnt_tmp >= size)
                    //                                {
                    //                                    printf("out of mem\n");
                    //                                    exit(-1);
                    //                                }
                    //                            }
                    //                            if ((pareto[cnt_pareto] > pareto[k]))
                    //                            {
                    //                                f = false;
                    //                            }
                    //                        }
                    //                        if (f)
                    //                        {
                    //                            pareto_tmp[cnt_tmp] = pareto[cnt_pareto];
                    //                            cnt_tmp++;
                    //                            if (cnt_tmp >= size - 1)
                    //                            {
                    //                                printf("out of mem\n");
                    //                                exit(-1);
                    //                            }
                    //                        }

                    //                        cnt_pareto = cnt_tmp;
                    //                        sw = pareto; pareto = pareto_tmp; pareto_tmp = sw;

                    //                    }
                    //                }
            }
            //                

            //                FILE* pf;
            //                printf("pareto count: %d\n", cnt_pareto);
            //                pf = fopen("estimate_pareto.txt", "w");

            //                for (int k = 0; k < cnt_pareto; k++)
            //                {
            //                    for (int ifn = 0; ifn < cntFnc; ifn++)
            //                    {
            //                        fprintf(pf, "%1.6lf ", pareto[k].fValue[ifn]);
            //                    }
            //                    for (int iim = 0; iim < dim; iim++)
            //                    {
            //                        fprintf(pf, "%1.6lf ", pareto[k].img[iim]);
            //                    }
            //                    fprintf(pf, "\n");
            //                }

            //                fclose(pf);

            //                delete[] pareto;
            //                delete[] pareto_tmp;
            //            }


            HashSet<int> indexesForDeletion = new HashSet<int>();
            var result = new List<MethodPoint>(set);
            current.Add(set[0]);
            for (int i = 0; i < set.Count; i++)
            {
                if (indexesForDeletion.Contains(i))
                {
                    continue;
                }

                for (int j = 0; j < set.Count; j++)
                {
                    if (indexesForDeletion.Contains(j) || i == j)
                    {
                        continue;
                    }

                    if (IsCurrentPointBetter(set[i], set[j]))
                    {
                        indexesForDeletion.Add(j);
                    }
                }
            }

            foreach (var index in indexesForDeletion.OrderByDescending(v => v))
            {
                result.RemoveAt(index);
            }

            return result;
        }

        public static double CalculateHV(List<MethodPoint> approximatePareto, MethodPoint referencePoint)
        {
            approximatePareto.Sort((i, j) => j.Criterions[0].CompareTo(i.Criterions[0]));
            MethodPoint previous = referencePoint;
            double hv = 0;
            for (int i = 0; i < approximatePareto.Count; i++)
            {
                hv += CalculateRectangleArea(approximatePareto[i], previous);
                previous = new MethodPoint()
                               {
                                   Criterions =
                                       new List<double>()
                                           {
                                               approximatePareto[i].Criterions[0],
                                               referencePoint.Criterions[1]
                                           }
                               };
            }

            return hv;
        }

        private static double CalculateRectangleArea(MethodPoint current, MethodPoint previous)
        {
            double a = previous.Criterions[0] - current.Criterions[0];
            double b = previous.Criterions[1] - current.Criterions[1];
            return a * b;
        }

        public static double CalculateDistanceIndex(List<MethodPoint> approximatePareto)
        {
            double du = 0;
            List<double> di = new List<double>(approximatePareto.Count);

            foreach (var methodPoint in approximatePareto)
            {
                var minDistance = approximatePareto.Where(s => s != methodPoint)
                    .Select(s => CalculateDistance(s.Criterions, methodPoint.Criterions)).Min();
                di.Add(minDistance);
            }

            double d = di.Sum() / approximatePareto.Count;

            du = di.Select(s => (d - s) * (d - s)).Sum();

            return du;
        }

        public static double CalculateDistance(List<double> a, List<double> b)
        {
            double sum = 0;

            for (int i = 0; i < a.Count; i++)
            {
                sum += (a[i] - b[i]) * (a[i] - b[i]);
            }

            return Math.Sqrt(sum);
        }

        private static bool IsCurrentPointBetter(MethodPoint p1, MethodPoint p2)
        {
            bool f = true;
            for (int i = 0; i < p1.Criterions.Count; i++)
            {
                var v1 = p1.Criterions[i];
                var v2 = p2.Criterions[i];
                f = f && (v1 < v2);
            }

            return f;
        }
    }
}