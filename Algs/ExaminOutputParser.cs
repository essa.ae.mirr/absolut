﻿namespace Algorithms
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;

    using Algorithms.Methods;

    using NLog;

    public class ExaminOutputParser
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        private int _numberOfConstraints;
        private int _numberOfCriterions;
        private int _dimension;
       

        public ExaminOutputParser(IProblem problem)
        {
            _numberOfConstraints = problem.NumberOfConstraints;
            _numberOfCriterions = problem.NumberOfCriterions;
            _dimension = problem.Dimension;
        }

        public ExaminOutputParser(int constraints, int criterions, int dimension)
        {
            _numberOfConstraints = constraints;
            _numberOfCriterions = criterions;
            _dimension = dimension;
        }

        public int ExtractInterations(string firstLine)
        {
            _logger.Debug("ExtractInterations started with line {0}", firstLine);
            int totalIterations = -1;
            string[] info = firstLine.Split(' ');
            totalIterations =  Convert.ToInt32(info[0]);
            _logger.Debug("ExtractInterations ended, iterations = {0}", totalIterations );
            return totalIterations;
        }

        public MethodPoint ParseExaminFileLine(string line)
        {
            _logger.Debug("ParseExaminFileLine started with line {0}", line);
            string[] parts = line.Split('|');
            string[] point = parts[0].TrimEnd(' ').Split(' ');
            string[] functionalsValues = parts[1].TrimStart(' ').TrimEnd(' ').Split(' ');
           
            var result = new MethodPoint
            {
                Constraints = new List<double>(),
                Criterions = new List<double>()
            };
            for (int i = 0; i < _numberOfConstraints && i < functionalsValues.Length; i++)
            {
                result.Constraints.Add(Convert.ToDouble(functionalsValues[i], CultureInfo.InvariantCulture));
            }

            result.Criterions = new List<double>();
            if (functionalsValues.Length == 1 + _numberOfConstraints)
            {
                result.Criterions.Add(Convert.ToDouble(functionalsValues.Last(), CultureInfo.InvariantCulture));
            }

            foreach (var dim in point)
            {
                result.X.Add(Convert.ToDouble(dim, CultureInfo.InvariantCulture));
            }
            _logger.Debug("ParseExaminFileLine ended, parsed point {0}", result.X);
            _logger.Debug("ParseExaminFileLine ended, parsed criterions {0}", result.Criterions);
            
            return result;
            
        }

        public MethodPoint ParseParetoFrontFileLine(string line)
        {
            _logger.Debug("ParseParetoFrontFileLine started with line {0}", line);
           
            string[] parts = line.Split(' ');
            var result = new MethodPoint
            {
                X = new List<double>(),
                Constraints = new List<double>(),
                Criterions = new List<double>()
            };

            for (int i = 0; i < this._dimension; i++)
            {
                result.X.Add(Convert.ToDouble(parts[i], CultureInfo.InvariantCulture));
            }

            for (int i = this._dimension; i < _dimension + _numberOfCriterions; i++)
            {
                result.Criterions.Add(Convert.ToDouble(parts[i], CultureInfo.InvariantCulture));
            }

            _logger.Debug("ParseParetoFrontFileLine ended, parsed point {0}", result);
            return result;
        }
    }
}