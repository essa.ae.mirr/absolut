﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Functions;

namespace Properties
{
    public abstract class PropertyInfo
    {
        public string name { get; protected set; }
        public object defaultValue { get; protected set; }
        public abstract bool CheckValue(object value);
        public PropertyType Type { get; protected set; }
    }

    public enum PropertyType
    {
        Double,
        Integer,
        Func,
        Bool,
        Color
    }

    public enum DoubleDeltaType
    {
        Step,
        Log
    }

    public class DoubleProperty : PropertyInfo
    {
        public double min { get; protected set; }
        public double max { get; protected set; }
        public int delta { get; set; }
        public DoubleDeltaType deltaType { get; protected set; }


        public DoubleProperty(string name, double min, double max, int delta, DoubleDeltaType type, double defaultValue)
        {
            Type = PropertyType.Double;
            if (defaultValue > max || defaultValue < min) throw new ArgumentException();
            if (delta <= 0) throw new ArgumentOutOfRangeException();
            this.name = name;
            this.min = min;
            this.max = max;
            this.delta = delta;
            this.defaultValue = defaultValue;
            deltaType = type;
        }


        public override bool CheckValue(object value)
        {
            double val = 0;
            try
            {
                val = (double) value;
            }
            catch (Exception)
            {
                return false;
            }
            return !(val < min) && !(val > max);
        }
    }

    public class IntProperty : PropertyInfo
    {
        public int min { get; protected set; }
        public int max { get; protected set; }
        public int delta { get; set; }

        public IntProperty(string name, int min, int max, int delta, int defaultValue)
        {
            Type = PropertyType.Integer;
            if (defaultValue > max || defaultValue < min) throw new ArgumentException();
            this.name = name;
            this.min = min;
            this.max = max;
            this.delta = delta;
            this.defaultValue = defaultValue;
        }

        public override bool CheckValue(object value)
        {
            int val;
            try
            {
                val = (int) value;
            }
            catch (Exception)
            {
                return false;
            }
            return !(val < min) && !(val > max);
        }
    }

    public class FuncProperty : PropertyInfo
    {
        public FuncProperty(string name, IFunction defaultValue)
        {
            Type = PropertyType.Func;
            this.name = name;
            this.defaultValue = defaultValue;
        }

        public override bool CheckValue(object value)
        {
            IFunction f = value as IFunction;
            if (f == null)
            {
                return false;
            }
            return true;
        }
    }

    public class FuncListProperty : PropertyInfo
    {
        public FuncListProperty(string name, List<IFunction> defaultValue)
        {
            this.name = name;
            this.defaultValue = defaultValue;
        }

        public override bool CheckValue(object value)
        {
            List<IFunction> f = value as List<IFunction>;
            if (f == null)
            {
                return false;
            }
            return true;
        }
    }

    public class BoolProperty : PropertyInfo
    {
        public BoolProperty(string name, bool defaultValue)
        {
            this.name = name;
            this.defaultValue = defaultValue;
        }
        
        public override bool CheckValue(object value)
        {
            bool val;
            try
            {
                val = (bool)value;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }

    public class ColorProperty : PropertyInfo
    {
        public ColorProperty(string name, Color defaultValue)
        {
            this.name = name;
            this.defaultValue = defaultValue;
        }

        public override bool CheckValue(object value)
        {
            Color val;
            try
            {
                val = (Color) value;
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }

}