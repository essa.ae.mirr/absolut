﻿using System;
using System.Windows.Forms;

namespace Plotter
{
    using Functions;

    using ViewDemo;

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void openDllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var dllSettings = new DLLSettings() {Text = "Choose dll"};
            DialogResult dr = dllSettings.ShowDialog();
            if (dr == DialogResult.OK)
            {
                try
                {
                    var buildProblemFromDll = ProblemFactory.BuildProblemFromDll(
                        dllSettings.dllPath,
                        dllSettings.configPath);
                    int k = 0;
                    foreach (var f in buildProblemFromDll.Constraints)
                    {
                        SliceEditorForm sliceEditorForm =
                            new SliceEditorForm(f)
                                {
                                    MdiParent = this,
                                    Text = dllSettings.dllPath + " Constraint " + k
                                };
                        sliceEditorForm.Show();
                        k++;
                    }

                    k = 0;

                    foreach (var f in buildProblemFromDll.Criterions)
                    {
                        SliceEditorForm sliceEditorForm =
                            new SliceEditorForm(f) { MdiParent = this, Text = dllSettings.dllPath + " Criteria " + k };
                        sliceEditorForm.Show();
                        k++;
                    }

                    if (buildProblemFromDll.Constraints.Count > 0)
                    {
                        SliceEditorForm sliceEditorForm =
                            new SliceEditorForm(buildProblemFromDll) { MdiParent = this, Text = dllSettings.dllPath };
                        sliceEditorForm.Show();
                    }
                }
                catch (Exception e1)
                {
                    MessageBox.Show("Error has happened, please try again");
                }



            }
        }



        private void closeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form c in this.MdiChildren)
            {
                c.Close();
            }
        }

        private void cascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void horizonalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void arrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }
    }
}
