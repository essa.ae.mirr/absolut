﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Functions
{
    [Serializable]
    public class Plane : MultidimFunction
    {
        private double _coef1;
        private double _coef2;
        private double _const;
        public Plane(double coef1, double coef2, double constValue, List<double> left, List<double> right)
        {
            _coef1 = coef1;
            _coef2 = coef2;
            _const = constValue;
            Left = left;
            Right = right;
        }

        public override double Calc(List<double> arg)
        {
            return _coef1 * arg[0] + _coef2 * arg[1] + _const;
        }
    }
}
