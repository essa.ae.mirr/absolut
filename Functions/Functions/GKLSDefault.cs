﻿using System;
using System.Collections.Generic;
using System.Linq;
using GKLScli;

namespace Functions.Functions
{
    internal class GKLSDefault : MultidimFunction
    {
        private readonly GKLSwrap f = new GKLSwrap();

        public GKLSDefault(int number = 5)
        {
            var left = new double[2];
            var right = new double[2];
            f.GetDomainBounds(left, right);
            Right = right.ToList();
            Left = left.ToList();


            f.SetFunctionNumber(number);
        }

        public override double Calc(List<double> arg)
        {
            if (!this.CheckPoint(arg)) throw new ArgumentException();
            return f.Calculate(arg.ToArray());
        }

        public double GetOptimumValue()
        {
            return f.GetOptimalValue();
        }

        public List<double> GetOptimumCoordinates()
        {
            double[] tmp = new double[f.GetDimension()];
            var optimumCoordinates = f.GetOptimumCoordinates(tmp);
            return tmp.ToList();
        }
    }
}