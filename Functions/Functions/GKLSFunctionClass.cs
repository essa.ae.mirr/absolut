﻿using System;
using System.Collections.Generic;
using System.Linq;
using GKLScli;

namespace Functions.Functions
{
    internal enum GKLSFunctionType
    {
        TND,
        TD,
        TD2
    }

    internal class GKLSFunctionClass
    {
        public const int MaxDimension = 1009;
        public const double MaxGlobalMinValue = 0.0;

        private readonly GKLSwrap functionClass;


        public GKLSFunctionClass()
        {
            functionClass = new GKLSwrap();
            functionClass.SetDefaultParameters();
            functionClass.SetFunctionNumber(1);
        }

        public int CurrentFunction
        {
            get { return functionClass.GetFunctionNumber(); }
            set
            {
                if (value < 0 || value > 100) throw new ArgumentOutOfRangeException();
                functionClass.SetFunctionNumber(value);
            }
        }

        public int Dimension
        {
            get { return (int) functionClass.GetDimension(); }
            set
            {
                if (value < 0 || value >= MaxDimension) throw new ArgumentOutOfRangeException();
                functionClass.SetDimension((uint) value);
            }
        }

        public int NumberOfLocalMinima
        {
            get { return (int) functionClass.GetNumberOfLocalMinima(); }
            set
            {
                if (value <= 1) throw new ArgumentOutOfRangeException();
                functionClass.SetNumberOfLocalMinima((uint) value);
            }
        }

        public List<double> LeftBoundary
        {
            get
            {
                var left = new double[Dimension];
                var right = new double[Dimension];
                functionClass.GetDomainBounds(left, right);
                return left.ToList();
            }
            //set
            //{
            //    if(value.Count != Dimension) throw  new ArgumentOutOfRangeException();

            //    double[] left = value.ToArray();
            //    functionClass.

            //}
        }

        public List<double> RightBoundary
        {
            get
            {
                var left = new double[Dimension];
                var right = new double[Dimension];
                functionClass.GetDomainBounds(left, right);
                return right.ToList();
            }
        }
    }
}