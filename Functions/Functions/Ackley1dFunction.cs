﻿using System;
using System.Collections.Generic;

namespace Functions.Functions
{
    internal class AckleyFunction1 : MultidimFunction
    {
        public AckleyFunction1()
        {
            Right = new List<double> {32, 32};
            Left = new List<double> {-32, -32};
        }

        public override double Calc(List<double> arg)
        {
            if (!this.CheckPoint(arg)) throw new ArgumentException();
            return -200 * Math.Exp(-0.02 * Math.Sqrt(arg[0] * arg[0] + arg[1] * arg[1]));
        }
    }

    internal class AckleyFunction2 : MultidimFunction
    {
        public AckleyFunction2()
        {
            Right = new List<double> {32, 32};
            Left = new List<double> {-32, -32};
        }

        public override double Calc(List<double> arg)
        {
           // if (!this.CheckPoint(arg)) throw new ArgumentException();
            return -200 * Math.Exp(-0.02 * Math.Sqrt(arg[0] * arg[0] + arg[1] * arg[1])) +
                   5 * Math.Exp(Math.Cos(3 * arg[0]) + Math.Sin(3 * arg[1]));
        }
    }
}