﻿using System;
using System.Collections.Generic;

namespace Functions
{
    internal class FunctionHL : Function1d
    {
        private readonly double[] _a = new double[14];
        private readonly double[] _b = new double[14];

        public FunctionHL()
        {
            Left = new List<double>();
            Right = new List<double>();
            Left.Add(0.0);
            Right.Add(1.0);
            var rand = new Random();

            for (var i = 0; i < 14; i++)
            {
                _a[i] = rand.NextDouble() * (1 + 1) - 1;
                _b[i] = rand.NextDouble() * (1 + 1) - 1;
            }
        }

        public override double Calc(double x)
        {
            double sum = 0;
            for (var i = 0; i < 14; i++)
                sum += _a[i] * Math.Sin(2 * i * Math.PI * x) + _b[i] * Math.Cos(2 * i * Math.PI * x);
            return sum;
        }
    }
}