﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Algorithms;
using Functions.Functions;

namespace Functions
{
    [Serializable]
    public class RosenbrokCubeLine : Problem
    {
        private static IFunction _firstConstraint =
            new MultidimFunction(list => list[0] * list[0] + list[1]*list[1] - 2)
                {Dimension = 2,
                    Left = new List<double>() {-1.5, -1.5},
                    Right = new List<double>() {1.5, 1.5}};

       

        private static IFunction _criteria =
            new MultidimFunction(x => (1 - x[0]) * (1 - x[0]) + 100 * (x[1] - x[0] * x[0]) * (x[1] - x[0] * x[0]))
            {
                Dimension = 2,
                Left = new List<double>() {-1.5, -1.5},
                Right = new List<double>() {1.5, 1.5}
            };


        public RosenbrokCubeLine() : base(null, new List<double>() {-1.5, -1.5},
            new List<double>() {1.5, 1.5}, 0, new List<double>() {1, 1}, new List<IFunction>() {_criteria},
            new List<IFunction>() {_firstConstraint})
        {
            Dimension = 2;
        }
    }

    [Serializable]
    class RosenbrokDisk : Problem
    {
        private static IFunction _firstConstraint =
            new MultidimFunction(list => list[0] * list[0] + list[1] * list[1]);

        private static IFunction _criteria =
            new MultidimFunction(x => (1 - x[0]) * (1 - x[0]) + 100 * (x[1] - x[0] * x[0]) * (x[1] - x[0] * x[0]));

        public RosenbrokDisk() : base(null, new List<double>() {-1.5, -1.5},
            new List<double>() {1.5, 1.5}, 0, new List<double>() {1, 1}, new List<IFunction>() {_criteria},
            new List<IFunction>() {_firstConstraint})
        {
        }
    }

    public class G8 : Problem
    {
        private static IFunction _firstConstraint =
            new MultidimFunction(x => x[0] * x[0] - x[1] + 1)
            {
                Dimension = 2,
                Left = new List<double>() {0.1, 0.1},
                Right = new List<double>() { 9.9, 9.9 }
            };

        private static IFunction _secondConstraint =
            new MultidimFunction(x => 1 - x[0] + (x[1] - 4) * (x[1] - 4))
            {
                Dimension = 2,
                Left = new List<double>() { 0.1, 0.1 },
                Right = new List<double>() { 9.9, 9.9 }
            };

        private static IFunction _criteria =
            new MultidimFunction(x =>
                Math.Pow(Math.Sin(2 * Math.PI * x[0]), 3) * Math.Sin(2 * Math.PI * x[1]) /
                (Math.Pow(x[1], 3) * (x[0] + x[1])))
            {
                Dimension = 2,
                Left = new List<double>() { 0.1, 0.1 },
                Right = new List<double>() { 9.9, 9.9 }
            };

        public G8() : base(null, new List<double>() {0.1, 0.1},
            new List<double>() {9.9, 9.9}, 0, new List<double>() {10, 10}, new List<IFunction>() {_criteria},
            new List<IFunction>() {_firstConstraint, _secondConstraint})
        {
            Dimension = 2;
        }


        public class BinhAndCornProblem : Problem
        {
            private static IFunction _firstConstraint =
                new MultidimFunction(x => (x[0] - 5) * (x[0] - 5) + x[1] * x[1])
                {
                    Dimension = 2,
                    Left = new List<double>() {0, 0},
                    Right = new List<double>() {5, 3}
                };

            private static IFunction _secondConstraint =
                new MultidimFunction(x => (x[0] - 8) * (x[0] - 8) + (x[1] + 3) * (x[1] + 3))
                {
                    Dimension = 2,
                    Left = new List<double>() {0, 0},
                    Right = new List<double>() {10, 10}
                };

            private static IFunction _criteria1 =
                new MultidimFunction(x =>
                    4 * x[0] * x[0] + 4 * x[1] * x[1])
                {
                    Dimension = 2,
                    Left = new List<double>() {0, 0},
                    Right = new List<double>() {5, 3}
                };

            private static IFunction _criteria2 =
                new MultidimFunction(x =>
                    (x[0] - 5) * (x[0] - 5) + (x[1] - 5) * (x[1] - 5))
                {
                    Dimension = 2,
                    Left = new List<double>() {0, 0},
                    Right = new List<double>() {5, 3}
                };

            public BinhAndCornProblem() : base(null, new List<double>() {0, 0},
                new List<double>() {5, 3}, 0, new List<double>() {10, 10}, new List<IFunction>() {_criteria1, _criteria2},
                new List<IFunction>() {_firstConstraint, _secondConstraint})
            {
                Dimension = 2;
            }
        }
    }

    public class Benchmark1 : Problem
    {
        private static IFunction _criteria1 = new MultidimFunction(x => (x[0] - 1) * x[1] * x[1] + 1)
                                                  {
                                                      Dimension = 2,
                    Left = new List<double>() { 0, 0 },
                    Right = new List<double>() {1, 1 }
                };

        private static IFunction _criteria2 =
            new MultidimFunction(x =>
                x[1])
                {
                    Dimension = 2,
                    Left = new List<double>() { 0, 0 },
                    Right = new List<double>() { 1, 1 }
                };

        public Benchmark1() : base(null, new List<double>() { 0, 0 },
            new List<double>() { 1, 1 }, 0, new List<double>() { 1, 1 }, new List<IFunction>() { _criteria1, _criteria2 },
            new List<IFunction>())
        {
            Dimension = 2;
        }
    }

    [Serializable]
    public class BinnCornMco : Problem
    {
        private static IFunction _criteria1 =
            new MultidimFunction(x => 4*x[0] * x[0] + 4*x[1] * x[1])
                {
                    Dimension = 2,
                    Left = new List<double>() { -5, -3 },
                    Right = new List<double>() {5, 3 }
                };



        private static IFunction _criteria2 =
            new MultidimFunction(x => (x[0] - 5) * (x[0] - 5) + (x[1] - 5)* (x[1] - 5))
            {
                 Dimension = 2,
                    Left = new List<double>() { -5, -3 },
                    Right = new List<double>() { 5, 3 }
            };


        public BinnCornMco() : base(null, new List<double>() { -5, -3 },
            new List<double>() { 5, 3 }, 0, new List<double>() { 1, 1 }, new List<IFunction>() { _criteria1, _criteria2 },
            new List<IFunction>())
        {
            Dimension = 2;
        }
    }
}