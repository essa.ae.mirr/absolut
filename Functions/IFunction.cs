﻿using System;
using System.Collections.Generic;

namespace Functions
{
    public interface IFunction
    {
        // размерность задачи
        int Dimension { get; set; }
        // имя задачи
        String Name { get; set; }
        // границы допустимой области
        List<double> Right { get; set; }
        List<double> Left { get; set; }
        // информация об известном глобальном минимуме
        List<double> Min_x { get; set; }
        double? Min { get; set; }
        // вычисление значения функции в точке
        double Calc(List<double> arg);
        // методы для представления многомерной функции как одномерной
        double Calc(double x);
        List<double> GetImage(double x);
        double GetPrototype(List<double> x);
        // методы для совместимости с форматом представления задач в системе Globalizer
        int GetNumberOfFunctions();
        void SetFunctionNumber(int index);
        List<double> Lambdas { get; set; }
    }
}