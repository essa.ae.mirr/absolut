﻿using System;
using System.Collections.Generic;
using System.Linq;
using Algorithms;

namespace Functions
{
    [Serializable]
    public class ProblemDll : IProblem
    {
        private int _dimension;
        private string _configPath;
        private List<double> _lowerBound;
        private List<double> _upperBound;
        private List<IFunction> _criterions;
        private List<IFunction> _constraints;
        private List<IFunction> _functionals;
        private double? _optimalValue;
        private List<double> _optimalPoint;
        private int _numberOfFunctions;
        private int _numberOfConstratins;
        private int _numberOfCriterions;
        private ProblemInterface _importedProblem;
        private string _name;
        private string _dllPath;

        public ProblemDll(ProblemInterface problem)
        {
            _importedProblem = problem;
            Initialize();
        }

        // установка значений основных атрибутов задачи

        public int Dimension
        {
            get { return _importedProblem.GetDimension(); }
            set { _importedProblem.SetDimension(value); }
        }

        public string ConfigPath
        {
            get { return _configPath; }
            set
            {
                _configPath = value;
                _importedProblem.SetConfigPath(value);
            }
        }

        public string DllPath
        {
            get { return _dllPath; }
            set { _dllPath = value; }
        }

        public List<double> LowerBound
        {
            get { return _lowerBound; }
            set => _lowerBound = value;
        }

        public List<double> UpperBound
        {
            get { return _upperBound; }
        }

        public List<IFunction> Criterions
        {
            get { return _criterions; }
        }

        public List<IFunction> Constraints
        {
            get { return _constraints; }
        }

        public List<IFunction> Functionals
        {
            get { return _functionals; }
        }

        public double? OptimalValue
        {
            get { return _optimalValue; }
        }

        public List<double> OptimalPoint
        {
            get { return _optimalPoint; }
        }

        public double GetOptimalValue(int index)
        {
            double value;
            unsafe
            {
                _importedProblem.GetOptimalValue(&value);
            }

            return value;
        }

        public void Initialize()
        {
            // установка свойств задачи
            // в соответствии с свойствами из динамической библиотеки
            _dimension = _importedProblem.GetDimension();
            _numberOfCriterions = _importedProblem.GetNumberOfCriterions();
            _numberOfConstratins = _importedProblem.GetNumberOfConstraints();
            _numberOfFunctions = _importedProblem.GetNumberOfFunctions();

            double[] upper = new double[_dimension];
            double[] lower = new double[_dimension];
            _importedProblem.GetBounds(lower, upper);
            _lowerBound = lower.ToList();
            _upperBound = upper.ToList();

            double[] optimalPoint = new double[_dimension];
            _importedProblem.GetOptimumPoint(optimalPoint);
            _optimalPoint = optimalPoint.ToList();
            double optimalValue;
            unsafe
            {
                _importedProblem.GetOptimalValue(&optimalValue);
            }

            _optimalValue = optimalValue;

            _constraints = new List<IFunction>();
            int i;
            for (i = 0; i < _numberOfConstratins; i++)
                _constraints.Add(new FunctionFromDll(this, i));

            _criterions = new List<IFunction>();
            for (; i < _numberOfFunctions; i++)
                _criterions.Add(new FunctionFromDll(this, i));

            _functionals = new List<IFunction>(NumberOfFunctions);
            _functionals.AddRange(_constraints);
            _functionals.AddRange(_criterions);
            foreach (var func in Functionals)
            {
                func.Left = LowerBound.GetRange(0, func.Left.Count);
                func.Right = UpperBound.GetRange(0, func.Left.Count);
                func.Dimension = _dimension;
            }
        }

        public int NumberOfFunctions
        {
            get { return _numberOfFunctions; }
        }

        public int NumberOfConstraints
        {
            get { return _numberOfConstratins; }
        }

        public int NumberOfCriterions
        {
            get { return _numberOfCriterions; }
        }

        // все вычисления значений функционалов в точках
        // транслируются в класс-обертку и далее в C++ код

        public double CalculateFunctionals(List<double> y, int fNumber)
        {
            return _importedProblem.CalculateFunctionals(y.ToArray(), fNumber);
        }

        public List<double> CalculateCriterions(List<double> x)
        {
            List<double> criterions = new List<double>();
            for (int i = 0; i < _numberOfCriterions; i++)
                criterions.Add(_importedProblem.CalculateFunctionals(x.ToArray(), _numberOfConstratins + i));
            return criterions;
        }

        public List<double> CalculateConstraints(List<double> toList)
        {
            List<double> constraints = new List<double>();
            for (int i = 0; i < _numberOfConstratins; i++)
                constraints.Add(_importedProblem.CalculateFunctionals(toList.ToArray(), i));
            return constraints;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        // адаптер для доступа к функционалам из динамической библиотеки
        // по интерфейсу IFunction, используемому в системе
        class FunctionFromDll : IFunction
        {
            private List<double> _right;
            private List<double> _left;
            private List<double> _minX;
            private ProblemDll _problem;
            private int _fNumber;
            private double? _min;
            private string _name1;
            private int _dimension1;
            private List<double> _lambdas;

            public FunctionFromDll(ProblemDll problem, int fNumber)
            {
                _problem = problem;
                _fNumber = fNumber;
                _right = problem.UpperBound;
                _left = problem.LowerBound;
                _minX = problem.OptimalPoint;
            }

            public List<double> Right
            {
                get { return _right; }
                set { _right = value; }
            }

            public List<double> Left
            {
                get { return _left; }
                set { _left = value; }
            }

            public List<double> Min_x
            {
                get { return _minX; }
                set { _minX = value; }
            }

            public int Dimension
            {
                get => _dimension1;
                set => _dimension1 = value;
            }

            public double? Min
            {
                get { return _min; }
                set { _min = value; }
            }

            public double Calc(List<double> arg)
            {
                return _problem.CalculateFunctionals(arg, _fNumber);
            }

            public double Calc(double x)
            {
                return 1;
            }

            public List<double> GetImage(double x)
            {
                return null;
            }

            public double GetPrototype(List<double> x)
            {
                return 0;
            }

            public int GetNumberOfFunctions()
            {
                return 1;
            }

            public void SetFunctionNumber(int index)
            {
                return;
            }

            public List<double> Lambdas
            {
                get { return _lambdas; }
                set { _lambdas = value; }
            }

            public string Name
            {
                get { return _name1; }
                set { _name1 = value; }
            }
        }
    }
}