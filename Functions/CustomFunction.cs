﻿using System;
using System.Collections.Generic;
using org.mariuszgromada.math.mxparser;


namespace Functions
{
    [Serializable]
    public class CustomFunction : MultidimFunction

    {
        private List<double> _right;
        private List<double> _left;
        private Argument _x;
        private Argument _y;
        private Argument _z;

        private Expression f;
        

        public CustomFunction(List<double> left, List<double> right, string formula)
        {
            _right = right;
            _left = left;
            Right = _right;
            Left = _left;

            try
            {
               
               
                    formula = "z =" + formula;
                    _x = new Argument("x");
                    _y = new Argument("y");
                    _z = new Argument(formula, _x, _y);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public override double Calc(List<double> arg)
        {
            if (f != null) return f.calculate();
             _x.setArgumentValue(arg[0]);
            _y.setArgumentValue(arg[1]);
           
            return _z.getArgumentValue();
        }
    }
}