﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Functions
{
    [Serializable]
    public  class PeanoHelper
    {
        private PeanoEvolvent _peanoEvolvent;
        private List<double> _lowerBound;
        private List<double> _upperBound;

        public PeanoHelper(List<double> lowerBound, List<double> upperBound)
        {
            _peanoEvolvent = new PeanoEvolvent(lowerBound.Count, 19);
            _peanoEvolvent.SetBounds(lowerBound.ToArray(), upperBound.ToArray());
            _lowerBound = lowerBound;
            _upperBound = upperBound;
        }

        public List<double> GetImage(double x)
        {
           double[] image;
            _peanoEvolvent.GetImage(x, out image);
            return image.ToList();
        }

        public double GetPrototype(List<double> x)
        {
            double[] image = x.ToArray();
            double pr_x = 0;
            _peanoEvolvent.GetPreimages(image, out pr_x);
            return pr_x;
        }
    }
}
