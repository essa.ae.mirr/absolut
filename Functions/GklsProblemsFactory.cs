﻿namespace Functions
{
    using System;
    using System.Collections.Generic;

    using Algorithms;

    using global::Functions.Functions;

    public static class GklsProblemsFactory
    {
        public static IProblem BuildGKLS(int functionNum)
        {
            GKLSDefault f = new GKLSDefault(functionNum);
            return new Problem(null, f.Left, f.Right, f.GetOptimumValue(), f.GetOptimumCoordinates(), new List<IFunction>() {f}, new List<IFunction>()  ) {Name = "GKLS DLL"};
        }

        public static IProblem BuildMultiObjectiveGkls(int objectivesCount)
        {
            var criterions = new List<IFunction>();
            var rand = new Random();
            var f = new GKLSDefault(rand.Next(1, 99)); 
            for (int i = 1; i < objectivesCount; i++)
            {
                f = new GKLSDefault(rand.Next(1, 99));
                criterions.Add(f);
            }
            return new Problem(
                       null, 
                       f.Left,
                       f.Right,
                       f.GetOptimumValue(), 
                       f.GetOptimumCoordinates(), 
                       criterions, 
                       new List<IFunction>())
                       {
                           Name = "GKLS DLL"
                       };
        }

        public static IProblem BuildMultiObjectiveGklsWithConstraints(int objectivesCount, int constraintsCount)
        {
            var criterions = new List<IFunction>();
            var constraints = new List<IFunction>();
            var rand = new Random();
            var f = new GKLSDefault(rand.Next(1, 99));
            for (int i = 1; i < objectivesCount+1; i++)
            {
                f = new GKLSDefault(rand.Next(1, 99));
                criterions.Add(f);
            }

            for (int i = 0; i < constraintsCount+1; i++)
            {
                f = new GKLSDefault(rand.Next(1, 99));
                constraints.Add(f);
            }
            return new Problem(
                       null,
                       f.Left,
                       f.Right,
                       f.GetOptimumValue(),
                       f.GetOptimumCoordinates(),
                       criterions,
                      constraints)
                       {
                           Name = "GKLS DLL"
                       };
        }
    }
}