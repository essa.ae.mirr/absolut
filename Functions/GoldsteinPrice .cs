﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Functions
{
    [Serializable]
    public class GoldsteinPrice : MultidimFunction
    {
        

        public GoldsteinPrice()
        {
            
            Right = new List<double>() {2,2};
            Left = new List<double>() { -2, -2 };
            Min_x = new List<double>() {0,-1};
            Min = 3;
            Name = "Goldstein - Price";

        }

        public override double Calc(List<double> arg)
        {
            double first = (1 +
                            (arg[0] + arg[1] + 1) * (arg[0] + arg[1] + 1) *
                            (19 - 14 * arg[0] + 3 * arg[0] * arg[0] - 14 * arg[1] + 6 * arg[0] * arg[1] +
                             3 * arg[1] * arg[1]));
            double second = 30 +
                            (2 * arg[0] - 3 * arg[1]) * (2 * arg[0] - 3 * arg[1]) *
                            (18 - 32 * arg[0] + 12 * arg[0] * arg[0] + 48 * arg[1] - 36 * arg[0] * arg[1] +
                             27 * arg[1] * arg[1]);
            return first * second;
        }

        
    }
}