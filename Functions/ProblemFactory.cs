﻿using NLog;

namespace Functions
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;

    using Algorithms;

    using global::Functions.Functions;

    public static class ProblemFactory
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();

        public struct NotConstrainedOptions
        {
            public const string Ackley1 = "Ackley1";

            public const string Ackley2 = "Ackley2";

            public const string Adjiman = "Adjiman";

            public const string Beale = "Beale";

            public const string Cube = "Cube";

            public const string Bumps = "Bumps";

            public const string Tube = "Tube";

            public const string Parabalod = "Parabaloid";

            public const string BartelsConn = "Bartels Conn";

            public const string GKLS = "GKLS";

            public const string Grishagin = "Grishagin";

            public const string Squares = "Squares";

            public const string Ripple = "Ripple";

            public const string Pyramid = "Pyramid";

            public const string Rastrigin = "Rastrigin";

            public const string GoldsteinPrice = "Goldstein - Price";
        };

        public static List<string> NotConstrainedOptionsList = new List<string>
                                                                   {
                                                                       "Ackley1",
                                                                       "Ackley2",
                                                                       "Adjiman",
                                                                       "Beale",
                                                                       "Cube",
                                                                       "Bumps",
                                                                       "Parabaloid",
                                                                       "Bartels Conn",
                                                                       "GKLS",
                                                                       "Squares",
                                                                       "Ripple",
                                                                       "Pyramid",
                                                                       "Rastrigin",
                                                                       "Goldstein - Price",
                                                                       "RosenbrokCubeLine",
                                                                       "G8",
                                                                       "BihnConn",
                                                                       "BihnConnMco"
                                                                   };

        public static List<string> ConstrainedOptions = new List<string> { "RosenbrokCubeLine", "G8" };

        public static IProblem BuildProblem(IFunction f)
        {
            return new Problem(null, f.Left, f.Right, null, null, new List<IFunction>() { f }, new List<IFunction>());
        }

        public static IProblem BuildProblemFromDll(string pathToDll, string pathToConfig)
        {
            _logger.Info("BuildProblemFromDll started, pathToDll - {}, pathToConfig - {}", pathToDll, pathToConfig);
            var p = new ProblemManager.ProblemManager();

            if (p.LoadProblemLibrary(pathToDll) != ProblemManager.ProblemManager.OK_)
                throw new ArgumentException("Can not load lib");

            var problem = p.GetProblem();
            problem.SetConfigPath("C:\\Users\\maria\\Desktop\\globalizer\\_bin\\gkls_conf.xml");
            problem.SetDimension(2);
            problem.Initialize();
           
            _logger.Info("Passing settings to ProblemDll");
            var buildProblemFromDll =
                new ProblemDll(problem) { Name = "DLL", ConfigPath = pathToConfig, DllPath = pathToDll };
            _logger.Info("BuildProblemFromDll ended");
            return buildProblemFromDll;
        }

        public static IProblem BuildProblem(
            List<IFunction> constraints,
            List<IFunction> criterions,
            List<double> lower,
            List<double> upper)
        {
            return new Problem(null, lower, upper, null, null, criterions, constraints);
        }

        public static IProblem BuildStandartConstrainedProblem(string name)
        {
            switch (name)
            {
                case "RosenbrokCubeLine":
                    return new RosenbrokCubeLine() { Name = "RosenbrokCubeLine" };
                case "G8":
                    return new G8() { Name = "G8" };
                case "BihnCorn":
                    return new G8.BinhAndCornProblem() { Name = "Bihn and Corn" };
                case "BihnConnMco":
                    return new BinnCornMco() {Name= "BihnConnMco" };
            }

            return new RosenbrokCubeLine();
        }

        public static IProblem BuildNotConstrainedProblem(string name)
        {
            switch (name)
            {
                case "RosenbrokCubeLine":
                    return new RosenbrokCubeLine() { Name = "RosenbrokCubeLine" };
                case "G8":
                    return new G8() { Name = "G8" };
                case "BihnConn":
                    return new G8.BinhAndCornProblem() { Name = "Bihn and Corn" };
                case "BihnConnMco":
                    return new BinnCornMco() { Name = "BihnConnMco" };
            }

            var buildNotConstrainedProblem = BuildProblem(FunctionFactory.Build(name));
            buildNotConstrainedProblem.Name = name;
            return buildNotConstrainedProblem;

            /*switch (name)
                        {
                            case "RosenbrokCubeLine":
                                return new RosenbrokCubeLine() { Name = "RosenbrokCubeLine" };
                            case "G8":
                                return new G8() { Name = "G8" };
                            case "BihnCorn":
                                return new G8.BinhAndCornProblem() { Name = "Bihn and Corn" };
                        }
            
                        return new RosenbrokCubeLine();*/
        }

        public static IProblem BuildFromTextInput(List<double> left, List<double> right, string formula)
        {
            throw new NotImplementedException();
        }

        public static void ProblemToXML(IProblem p, string path)
        {
            XmlWriter writer = null;
            XmlWriterSettings settings =
                new XmlWriterSettings { Indent = true, IndentChars = "\t", OmitXmlDeclaration = true };

            // Create the XmlWriter object and write some content.
            writer = XmlWriter.Create(path, settings);
            const string startElement = "Problem";
            writer.WriteStartElement(startElement);
            writer.WriteAttributeString("name", p.Name);
            writer.WriteEndElement();
            writer.Flush();
            writer.Close();
        }

        public static IProblem ProblemFromXML(string path)
        {
            XmlReader reader = XmlReader.Create(new FileStream(path, FileMode.Open));

            reader.ReadToFollowing("Problem");
            reader.MoveToFirstAttribute();
            return BuildNotConstrainedProblem(reader.Value);
        }

        public static IProblem BuildProblemWithFixedVars(Dictionary<int, double> fixedVars, IProblem problem)
        {
            List<IFunction> newConstraints = new List<IFunction>(problem.NumberOfConstraints);
            foreach (var problemConstraint in problem.Constraints)
            {
                newConstraints.Add(new FixedByValuesFunction(problemConstraint, fixedVars));
            }

            List<IFunction> newCriterions = new List<IFunction>(problem.NumberOfCriterions);
            foreach (var problemConstraint in problem.Criterions)
            {
                newCriterions.Add(new FixedByValuesFunction(problemConstraint, fixedVars));
            }

            return BuildProblem(newConstraints, newCriterions, newCriterions[0].Left, newCriterions[0].Right);
        }

        public static IProblem BuildProblem(IProblem problem, List<int> criterionsIndexes, List<int> constraintsIndexes)
        {
            var constraints = new List<IFunction>();
            foreach (var constraintsIndex in constraintsIndexes)
            {
                constraints.Add(
                    constraintsIndex - 1 >= problem.Functionals.Count
                        ? problem.Functionals[0]
                        : problem.Functionals[constraintsIndex - 1]);
            }

            var criterions = new List<IFunction>();
            foreach (var criterionsIndex in criterionsIndexes)
            {
                criterions.Add(
                    criterionsIndex - 1 >= problem.Functionals.Count
                        ? problem.Functionals[0]
                        : problem.Functionals[criterionsIndex - 1]);
            }

            return BuildProblem(constraints, criterions, problem.LowerBound, problem.UpperBound);
        }

        public static IProblem BuildProblemWithConvolution(IProblem problem)
        {
            throw new NotImplementedException();
        }
    }
}