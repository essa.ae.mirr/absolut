﻿using System;
using System.Collections.Generic;
using Functions;

namespace Algorithms
{
    public interface IProblem
    {
        // размерность задачи
        int Dimension { get; set; }
        // имя задачи
        string Name { get; set; }
        // границы допустимой области
        List<double> LowerBound { get; set; }
        List<double> UpperBound { get; }
        // доступ к функционалам задачи - критерием и ограничениям
        List<IFunction> Criterions { get; }
        List<IFunction> Constraints { get; }
        List<IFunction> Functionals { get; }
        int NumberOfFunctions { get; }
        int NumberOfConstraints { get; }
        int NumberOfCriterions { get; }
        // вычисление значений функционалов в точке
        double CalculateFunctionals(List<double> y, int fNumber);
        List<double> CalculateCriterions(List<double> x);
        List<double> CalculateConstraints(List<double> toList);
        // информация об известном решении задачи
        double? OptimalValue { get; }
        List<double> OptimalPoint { get; }
        double GetOptimalValue(int index);
        // начальная инициализация
        void Initialize();
        // путь к конфигурационному файлу для настройки динамически подключаемых задач
        string ConfigPath { get; set; }
        // путь к файлу динамической библиотеки
        string DllPath { get; set; }
    }
}