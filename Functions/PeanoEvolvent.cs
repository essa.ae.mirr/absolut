﻿using System;
using System.Collections.Generic;

namespace Functions
{
    [Serializable]
    public class PeanoEvolvent
    {
        public const int MaxDim = 20;
        public const int MaxM = 20;

        private const double extNull = 0.0; //double(0.0);
        private const double extOne = 1.0; //double(1.0);
        private const double extHalf = 0.5; //double(0.5);
        private readonly double[] A = new double[MaxDim]; // left and
        private readonly double[] B = new double[MaxDim]; // right bounds of search area

        private readonly int m; // accuracy of decomposition of hypercube
        private readonly int N; // dimension
        private readonly double nexpdouble;

        private  double[] y; // y point from hypercube [-1/2, 1/2]^N

        public PeanoEvolvent(int _N, int _m)
        {
            int i;
            if ((_N < 1) || (_N > MaxDim))
            {
                throw new Exception("N is1 out of range");
            }
            N = _N;
            y = new double[N];

            if ((_m < 2) || (_m > MaxM))
            {
                throw new Exception("m is1 out of range");
            }
            m = _m;

            for (nexpdouble = extOne, i = 0; i < N; nexpdouble += nexpdouble, i++)
                ;
        }

        public List<double> Right { get; set; }
        public List<double> Left { get; set; }

        public double Calc(List<double> arg)
        {
            throw new NotImplementedException();
        }

        public void SetBounds(double[] _A, double[] _B)
        {
            for (var i = 0; i < N; i++)
            {
                A[i] = _A[i];
                B[i] = _B[i];
            }
        }

// ----------------------------------------------------------------------------
        private void CalculateNumbr(out double s, int[] u, int[] v, out int l)
            // calculate s(u)=is1,l(u)=l,v(u)=iv by u=iu
        {
            int i, k1, k2, l1;
            double is1, iff;

            iff = nexpdouble;
            is1 = extNull;
            k1 = -1;
            k2 = 0;
            l1 = 0;
            l = -1;
            for (i = 0; i < N; i++)
            {
                iff = iff/2;
                k2 = -k1*u[i];
                v[i] = u[i];
                k1 = k2;
                if (k2 < 0)
                    l1 = i;
                else
                {
                    is1 += iff;
                    l = i;
                }
            }
            if (is1 == extNull)
                l = N - 1;
            else
            {
                v[N - 1] = -v[N - 1];
                if (is1 == nexpdouble - extOne)
                    l = N - 1;
                else
                {
                    if (l1 == N - 1)
                        v[l] = -v[l];
                    else
                        l = l1;
                }
            }
            s = is1;
        }

// ----------------------------------------------------------------------------
        private void CalculateNode(double is1, int n, int[] u, int[] v, out int l)
// âû÷èñëåíèå âñïîìîãàòåëüíîãî öåíòðà u(s) è ñîîòâåòñòâóþùèõ åìó v(s) è l(s)
// calculate u=u[s], v=v[s], l=l[s] by is1=s
        {
            int n1, i, j, k1, k2, iq;
            double iff;
            double nexp;

            iq = 1;
            for (nexp = 1, i = 0; i < n; nexp += nexp, i++) ;
            n1 = n - 1;
            l = 0;
            if (is1 == 0)
            {
                l = n1;
                for (i = 0; i < n; i++)
                {
                    u[i] = -1;
                    v[i] = -1;
                }
            }
            else if (is1 == nexpdouble - extOne)
            {
                l = n1;
                u[0] = 1;
                v[0] = 1;
                for (i = 1; i < n; i++)
                {
                    u[i] = -1;
                    v[i] = -1;
                }
                v[n1] = 1;
            }
            else
            {
                iff = nexpdouble;
                k1 = -1;
                for (i = 0; i < n; i++)
                {
                    iff = iff/2;
                    if (is1 >= iff)
                    {
                        if ((is1 == iff) && (is1 != extOne))
                        {
                            l = i;
                            iq = -1;
                        }
                        is1 -= iff;
                        k2 = 1;
                    }
                    else
                    {
                        k2 = -1;
                        if ((is1 == iff - extOne) && (is1 != extNull))
                        {
                            l = i;
                            iq = 1;
                        }
                    }
                    j = -k1*k2;
                    v[i] = j;
                    u[i] = j;
                    k1 = k2;
                }
                v[l] = v[l]*iq;
                v[n1] = -v[n1];
            }
        }

// ------------------------------------------------------------------------------------------------
        private void transform_P_to_D()
        {
            //if (N == 1) return;
            // transformation from hypercube P to hyperinterval D
            for (var i = 0; i < N; i++)
                y[i] = y[i]*(B[i] - A[i]) + (A[i] + B[i])/2;
        }

// ----------------------------------------------------------------------------
        private void transform_D_to_P()
        {
            //if (N == 1) return;
            // transformation from hyperinterval D to hypercube P
            for (var i = 0; i < N; i++)
                y[i] = (y[i] - (A[i] + B[i])/2)/(B[i] - A[i]);
        }

// ----------------------------------------------------------------------------
        private double[] GetYOnX(double _x)
        {
            if (N == 1)
            {
                y[0] = _x - 0.5;
                return y;
            }

            var iu = new int[MaxDim];
            var iv = new int[MaxDim];
            int l;
            double d;
            int mn;
            double r;
            var iw = new int[MaxDim];
            int it, i, j;
            double is1;

            d = _x;
            r = 0.5;
            it = 0;
            mn = m*N;
            for (i = 0; i < N; i++)
            {
                iw[i] = 1;
                y[i] = 0.0;
            }
            for (j = 0; j < m; j++)
            {
                if (_x == extOne)
                {
                    is1 = nexpdouble - extOne;
                    d = extNull; // d = 0.0;
                }
                else
                {
                    //Êîä èç ñòàðîé âåðñèè - óòî÷íèòü ðàáîòîñïîñîáíîñòü ïðè N > 32
                    d *= nexpdouble;
                    is1 = (int) d;
                    d -= is1;
                }
                CalculateNode(is1, N, iu, iv, out l);
                i = iu[0];
                iu[0] = iu[it];
                iu[it] = i;
                i = iv[0];
                iv[0] = iv[it];
                iv[it] = i;
                if (l == 0)
                    l = it;
                else if (l == it)
                    l = 0;
                r *= 0.5;
                it = l;
                for (i = 0; i < N; i++)
                {
                    iu[i] *= iw[i];
                    iw[i] *= -iv[i];
                    y[i] += r*iu[i];
                }
            }
            return y;
        }

//-----------------------------------------------------------------------------
        private double GetXOnY()
        {
            var u = new int[MaxDim];
            var v = new int[MaxDim];
            double x, r1;
            if (N == 1)
            {
                x = y[0] + 0.5;
                return x;
            }

            double r;
            var w = new int[MaxDim];
            int i, j, it, l;
            double is1;

            for (i = 0; i < N; i++)
                w[i] = 1;
            r = 0.5;
            r1 = extOne;
            x = extNull;
            it = 0;
            for (j = 0; j < m; j++)
            {
                r *= 0.5;
                for (i = 0; i < N; i++)
                {
                    u[i] = y[i] < 0 ? -1 : 1;
                    y[i] -= r*u[i];
                    u[i] *= w[i];
                }
                i = u[0];
                u[0] = u[it];
                u[it] = i;
                CalculateNumbr(out is1, u, v, out l);
                i = v[0];
                v[0] = v[it];
                v[it] = i;
                for (i = 0; i < N; i++)
                    w[i] *= -v[i];
                if (l == 0)
                    l = it;
                else if (l == it)
                    l = 0;
                it = l;
                r1 = r1/nexpdouble;
                x += r1*is1;
            }
            return x;
        }

//----------------------------------------------------------------------------
        public void GetImage(double x, out double[] _y, int EvolventNum = 0)
        {
            // â îäèíî÷íîé ðàçâåðòêå EvolventNum íå èñïîëüçóåòñÿ
            //   ââåäåí, ÷òîáû ðàáîòàë ïîëèìîðôèçì â ìíîæåñòâåííûõ ðàçâåðòêàõ
            // x ---> y
            GetYOnX(x); // it saves return value to y, so no need to call operator= again

            transform_P_to_D();
            _y = y;
            //memcpy(_y, y, N * sizeof(double));
        }

        //    x[0] = GetXOnY();
        //{
       public void GetPreimages(double [] _y, out double x)
       {
            y = _y;
           transform_D_to_P();

            x = GetXOnY();
        }
    }
}