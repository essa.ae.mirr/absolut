﻿using System;
using System.Collections.Generic;
using System.Linq;
using Algorithms;

namespace Functions
{
    [Serializable]
    public class Problem : IProblem
    {
        private List<IFunction> _allFunctionals;

        public Problem()
        {

        }

        public Problem(string configPath, List<double> lowerBound, List<double> upperBound, double? optimalValue, List<double> optimalPoint, List<IFunction> criterions, List<IFunction> constraints)
        {
            ConfigPath = configPath;
            LowerBound = lowerBound;
            UpperBound = upperBound;
            OptimalValue = optimalValue;
            OptimalPoint = optimalPoint;
            Criterions = criterions;
            Constraints = constraints;
            Initialize();
        }

        public int Dimension { get; set; }

        public string ConfigPath { get; set; }

        public string DllPath { get; set; }

        public List<double> LowerBound { get; set; }

        public List<double> UpperBound { get; }

        public double? OptimalValue { get; }

        public List<double> OptimalPoint { get; }

        public double GetOptimalValue(int index)
        {
            return _allFunctionals[index].Calc(OptimalPoint);
        }

        public void Initialize()
        {
            NumberOfConstraints = Constraints.Count;
            NumberOfCriterions = Criterions.Count;
            NumberOfFunctions = NumberOfConstraints + NumberOfCriterions;
            _allFunctionals = new List<IFunction>(NumberOfFunctions);
            _allFunctionals.AddRange(Constraints);
            _allFunctionals.AddRange(Criterions);
            Dimension = LowerBound.Count;
            foreach (var func in Functionals)
            {
                func.Left = LowerBound.GetRange(0, func.Left.Count);
                func.Right = UpperBound.GetRange(0, func.Left.Count);
                func.Dimension = Dimension;
            }
        }

        public int NumberOfFunctions { get; private set; }

        public int NumberOfConstraints { get; private set; }

        public int NumberOfCriterions { get; private set; }

        public double CalculateFunctionals(List<double> y, int fNumber)
        {
            return _allFunctionals[fNumber].Calc(y);
        }

        public List<IFunction> Criterions { get; }

        public List<IFunction> Constraints { get; }

        public List<double> CalculateCriterions(List<double> x)
        {
            return Criterions.Select(f => f.Calc(x)).ToList();
        }

        public List<double> CalculateConstraints(List<double> toList)
        {
            return Constraints.Select(f => f.Calc(toList)).ToList();
        }

        public string Name { get; set; }

        public List<IFunction> Functionals => _allFunctionals;
    }
}
