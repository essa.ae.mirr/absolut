﻿using System;
using System.Collections.Generic;

namespace Functions
{
    [Serializable]
    public class Rastrigin : MultidimFunction
    {
        private List<double> _right;
        private List<double> _left;
        private List<double> _minX;
        private int _dimension;
        private double? _min;

        public Rastrigin(int dimension)
        {
            _dimension = dimension;
            Right = new List<double>();
            Left = new List<double>();
            Min_x = new List<double>();
            for (int i = 0; i < dimension; i++)
            {
                Right.Add(5.12);
                Left.Add(-5.12);
                Min_x.Add(0);
            }
            Min = 0;
        }

        public override double Calc(List<double> arg)
        {
            double value = 10 * _dimension;
            for (int i = 1; i <= _dimension; i++)
            {
                 value += (arg[i - 1] * arg[i - 1] - 10 * Math.Cos(2 * Math.PI * arg[i - 1]));
            }
            return value;
        }
       
    }
}