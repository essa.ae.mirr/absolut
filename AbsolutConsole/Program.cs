﻿using System.Diagnostics;
using Algorithms.Methods;

namespace AbsolutConsole
{
    using System;
    using System.Linq;

    using Algorithms;

    using Functions;

    public class Program
    {
        public static void Main(string[] args)
        {
            AbsolutSaveRestoreExperiments.runWithStub();

        }
    }
}
