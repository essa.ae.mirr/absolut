﻿using System.Security.Cryptography;

namespace AbsolutConsole
{
    using Algorithms.Methods;

    using Functions;

    public static class AbsolutSaveRestoreExperiments
    {
        public static void runWithStub()
        {
            ExaminMcoAlg mco = new ExaminMcoAlg();
            mco.SetProperty(AbsolutAlgConstants.ExaminPathToExe, "ExaminMcoAbsolutWrapperStub.exe");
            mco.SetProperty(AbsolutAlgConstants.ExaminDll, "gklsC_mco.dll");
            var buildProblemFromDll = ProblemFactory.BuildProblemFromDll("gklsC_mco.dll", "");
            mco.Problem = buildProblemFromDll;
            mco.RecalcPoints();
            mco.SetProperty(AbsolutAlgConstants.Delta, 0.1);


        }
    }
}