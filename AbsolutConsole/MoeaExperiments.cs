﻿namespace AbsolutConsole
{
    using System;
    using System.Collections.Generic;

    using Algorithms;
    using Algorithms.Methods;

    using Functions;

    public static class MoeaExperiments
    {
        public static void GklsSeriesExperiment()
        {
            var experimentsCount = 5;
            List<string> lines = new List<string>();
            foreach (var moeaAlgName in AlgFactory.MoeaOptions)
            {
                Console.Out.WriteLine(moeaAlgName);
                lines.Add(moeaAlgName);
                var average = MoeaAlgExperiment(moeaAlgName, experimentsCount, 2, 1);
                lines.Add("Average: " + average);
            }

            System.IO.File.WriteAllLines(@".\" + "MoeaExperiment Constraints" + System.DateTime.Now + ".txt", lines);
        }

        public static void MoeaPopulationSizeSeriesExperiment()
        {
            List<string> lines = new List<string>();
            foreach (var moeaAlgName in AlgFactory.MoeaOptions)
            {
                Console.Out.WriteLine(moeaAlgName);
                MoeaPopulationSizeExperiment(moeaAlgName);
            }

            System.IO.File.WriteAllLines(@".\" + "MoeaPopulationSizeExperiment" + ".txt", lines);
        }

        public static void MoeaPointsSeriesExperiment()
        {
            List<string> lines = new List<string>();
            foreach (var moeaAlgName in AlgFactory.MoeaOptions)
            {
                Console.Out.WriteLine(moeaAlgName);
                MoeaAlgExperimentWithPoints(moeaAlgName,2,1);
            }

            System.IO.File.WriteAllLines(@".\" + "MoeaPopulationSizeExperiment" + ".txt", lines);
        }

        public static void MoeaPopulationSizeExperiment(string moeaAlgName)
        {
            List<string> lines = new List<string>();
            Console.Out.WriteLine(moeaAlgName);
            lines.Add(moeaAlgName);
            int populationSize = 100;
            int delta = 1000;
            for (; populationSize < 10000; populationSize += delta)
            {
                ISearchAlg a = AlgFactory.Build(moeaAlgName);
                a.SetPropertyWithoutRecalc(AbsolutAlgConstants.PopulationSize, populationSize);
                a.Problem = ProblemFactory.BuildNotConstrainedProblem("BihnCorn");
                a.RecalcPoints();
                Console.Out.WriteLine(a.GetParetoFront().Count);
                lines.Add(a.GetParetoFront().Count.ToString());
            }

            System.IO.File.WriteAllLines(@".\" + moeaAlgName + " " + "PopulationSizeExperiment" + ".txt", lines);
        }

        private static void MoeaAlgExperimentWithPoints(string moeaAlgName, int objectivesCount, int constraintsCount)
        {
            double paretoPointsCumulativeCount = 0;
            List<string> lines = new List<string>();
            Console.Out.WriteLine(moeaAlgName);
            lines.Add(moeaAlgName);

            ISearchAlg a = AlgFactory.Build(moeaAlgName);
            a.Problem = GklsProblemsFactory.BuildMultiObjectiveGklsWithConstraints(objectivesCount, constraintsCount);
            a.RecalcPoints();
            foreach (var point in a.CalculatedPoints())
            {
                lines.Add("Iteration: " + point.Iteration);
                lines.Add("X: " + string.Join("; ", point.X));
                lines.Add("Objectives: " + string.Join("; ", point.Criterions));
                lines.Add("Constraints: " + string.Join("; ", point.Constraints));
            }

            Console.Out.WriteLine(a.GetParetoFront().Count);
            lines.Add(a.GetParetoFront().Count.ToString());
            System.IO.File.WriteAllLines(@".\" + moeaAlgName + "Points" + ".txt", lines);
            
        }

        private static double MoeaAlgExperiment(
            string moeaAlgName,
            int experimentsCount,
            int objectivesCount,
            int constraintsCount)
        {
            double paretoPointsCumulativeCount = 0;
            List<string> lines = new List<string>();
            Console.Out.WriteLine(moeaAlgName);
            lines.Add(moeaAlgName);
            for (int i = 0; i < experimentsCount; i++)
            {
                ISearchAlg a = AlgFactory.Build(moeaAlgName);
                a.Problem = GklsProblemsFactory.BuildMultiObjectiveGklsWithConstraints(
                    objectivesCount,
                    constraintsCount);
                a.RecalcPoints();
                paretoPointsCumulativeCount += a.GetParetoFront().Count;
                Console.Out.WriteLine(a.GetParetoFront().Count);
                lines.Add(a.GetParetoFront().Count.ToString());
            }

            var average = paretoPointsCumulativeCount / experimentsCount;
            lines.Add("Average: " + average);

            System.IO.File.WriteAllLines(@".\" + moeaAlgName + "Constraints" + ".txt", lines);
            return average;
        }
    }
}