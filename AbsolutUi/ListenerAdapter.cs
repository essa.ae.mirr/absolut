﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Absolut_Model;

namespace AbsolutUi
{
    public class ListenerAdapter : IEventListener
    {
        public void OnPointsReset()
        {
            
        }

        public void OnMainFunctionChanged()
        {
           
        }

        public void OnSlicesChanged()
        {
            
        }

        public void OnMethodChanged()
        {
          
        }

        public void OnNextStep()
        {
        }

        public void OnPrevStep()
        {
           
        }

        public void OnExperimentAdded()
        {
           
        }

        public void OnExperimentDeleted()
        {
           
        }

        public void OnProblemChanged()
        {
           
        }
    }
}
