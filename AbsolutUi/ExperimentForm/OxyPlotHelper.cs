﻿namespace AbsolutUi.ExperimentForm
{
    using System.Windows.Controls;
    using System.Windows.Data;

    using OxyPlot;
    using OxyPlot.Wpf;

    public class OxyPlotHelper
    {
        public static Plot BuildApproximateParetoPlot()
        {
            var plot = new Plot();
            var scatterSeries =
                new OxyPlot.Wpf.ScatterSeries() { MarkerStrokeThickness = 25, MarkerType = MarkerType.Triangle };
            scatterSeries.SetBinding(ItemsControl.ItemsSourceProperty, new Binding("ApproximateScatterParetoFront"));
            var lineSeries = new OxyPlot.Wpf.LineSeries();
            lineSeries.SetBinding(ItemsControl.ItemsSourceProperty, new Binding("ApproximateParetoFront"));
            plot.Series.Add(scatterSeries);
            plot.Series.Add(lineSeries);
            return plot;
        }

        public static Plot BuildHistoryOfRunsPlot()
        {
            var plot = new Plot();
            var lineSeries = new LineSeries();
            
            //lineSeries.SetBinding(ItemsControl.ItemsSourceProperty, new Binding("OperationalChars"));
            plot.Series.Add(lineSeries);
            plot.Title = "Operational characteristic";
            return plot;
        }
    }
}