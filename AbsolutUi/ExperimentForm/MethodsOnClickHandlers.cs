﻿using System.Configuration;

namespace AbsolutUi.ExperimentForm
{
    using System;
    using System.Linq;
    using System.Windows.Forms;

    using Algorithms;

    using Functions;

    using ViewDemo;
    using ViewDemo.Dialogs;
    using ViewDemo.Presenters;

    public class MethodsOnClickHandlers
    {
        public Action OnChangeMethod { get; set; }

        public Action OnExternalMethod { get; set; }

        public Action OnMethodParameters { get; set; }

        public Action OnLibrary { get; set; }

        public Action OnDll { get; set; }

        private MethodPresenter _methodPresenter;

        public MethodsOnClickHandlers(MethodPresenter methodPresenter)
        {
            _methodPresenter = methodPresenter;
            OnChangeMethod = this.OnChangeMethodClick;
            OnExternalMethod = this.OnExternalMethodClick;
            OnLibrary = this.OnProblemLibraryClick;
            OnDll = this.OnDllClick;
            OnMethodParameters = this.OnMethodParametersClick;
        }

        private void OnChangeMethodClick()
        {
            using (var methodChoose = new MethodChooseDialog(AlgFactory.Options))
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;

                this._methodPresenter.SetMethodWithOldFunction(methodChoose.ChoosedOption);
            }
        }

        private void OnExternalMethodClick()
        {
            using (var examinSettings = new ExaminSettings())
            {
                examinSettings.ExaminPath = ConfigurationManager.AppSettings.Get("examinMcoPath");
                examinSettings.DllPath = ConfigurationManager.AppSettings.Get("examinMcoDllPath");
                var dr = examinSettings.ShowDialog();
                if (dr != DialogResult.OK) return;
                string config = examinSettings.ConfigPath == null ? "" : examinSettings.ConfigPath;
                try
                {
                    if (examinSettings.AlgOption == "Examin")
                    {
                        this._methodPresenter.SetExaminMethod(
                            examinSettings.DllPath,
                            config,
                            examinSettings.ExaminPath);
                    }
                    else if (examinSettings.AlgOption == "Examin Mco")
                    {
                        this._methodPresenter.SetExaminMcoMethod(
                            examinSettings.ExaminPath,
                            examinSettings.DllPath,
                            config);
                    }
                }
                catch (Exception e)
                {
                    System.Windows.MessageBox.Show("Something went wrong during external alg creation " + e);
                }
            }
        }

        private void OnMethodParametersClick()
        {
            try
            {
                var values = this._methodPresenter.GetParamsInfo().ToDictionary(
                    i => i,
                    i => this._methodPresenter.GetProperty(i.name));
                using (var paramsChoose = new ParametersDialog(values))
                {
                    var dr = paramsChoose.ShowDialog();
                    if (dr == DialogResult.OK)
                    {
                        this._methodPresenter.SetProperties(paramsChoose.changedValues);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                System.Windows.MessageBox.Show("Something went wrong during external alg creation " + e);
            }
            
        }

        private void OnProblemLibraryClick()
        {
            using (var methodChoose = new MethodChooseDialog(ProblemFactory.NotConstrainedOptionsList))
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
                this._methodPresenter.SetMainFunction(methodChoose.ChoosedOption);
            }
        }

        private void OnDllClick()
        {
            using (var methodChoose = new DLLSettings())
            {
                var dr = methodChoose.ShowDialog();
                if (dr != DialogResult.OK) return;
                
                   
                    try
                    {
                        this._methodPresenter.SetMainFunction(methodChoose.dllPath, methodChoose.configPath);
                    }
                    catch (Exception exception)
                    {
                        Console.Write(exception);
                        MessageBox.Show("Cant open dll");
                    }
               
            }
        }
    }
}