﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExperimentForm.xaml.cs" company="">
//   
// </copyright>
// <summary>
//   Interaction logic for ExperimentForm.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AbsolutUi.ExperimentForm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows;

    using Xceed.Wpf.AvalonDock.Layout;

    /// <summary>
    /// Interaction logic for ExperimentForm.xaml
    /// </summary>
    public partial class ExperimentForm : IFunctionalTabsView
    {
        private ExperimentFormViewModel _viewModel;

        public ExperimentForm(Guid id)
        {
            InitializeComponent();
            this._viewModel = new ExperimentFormViewModel(this, id);
            this.DataContext = this._viewModel;
        }

        public void AddFunctionalTab(LayoutDocument tab)
        {
            this.documentPane.Children.Add(tab);
        }

        public void ShowAllTabs(List<LayoutDocument> tabs)
        {
            this.documentPane.Children.Clear();
            foreach (var layoutDocument in tabs)
            {
                this.documentPane.Children.Add(layoutDocument);
            }
        }

        public void HideAllTabs()
        {
            this.documentPane.Children.Clear();
        }

        public void AddAnchorableTab(LayoutAnchorable tab)
        {
            this._layoutGroup.Children.Add(tab);
        }

        public bool DoesAnchorableTabExist(string tabName)
        {
            return this._layoutGroup.Children.Count(s => s.Title == tabName) == 0;
        }

        public bool DoesFunctionalsTabExists(string convolutionMenuItem)
        {
            return this.documentPane.Children.Count(s => s.Title == convolutionMenuItem) != 0;
        }

        public void DeleteTabs()
        {
            this.documentPane.Children.Clear();
        }

        private void OnPreviousStepClick(object sender, EventArgs e)
        {
            this._viewModel.Prev();
        }

        private void OnNextStepClick(object sender, EventArgs e)
        {
            this._viewModel.Next();
        }

        private void OnPauseClick(object sender, RoutedEventArgs e)
        {
            this._viewModel.Pause();
        }

        private void OnStartClick(object sender, RoutedEventArgs e)
        {
            this._viewModel.Start();
        }

        private void OnRestartClick(object sender, RoutedEventArgs e)
        {
            this._viewModel.Restart();
        }
    }
}