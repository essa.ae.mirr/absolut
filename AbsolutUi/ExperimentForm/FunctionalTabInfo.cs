﻿namespace AbsolutUi.ExperimentForm
{
    using System;
    using System.Windows.Forms;

    using Functions;

    public class FunctionalTabInfo
    {
        public string FunctionalName { get; set; }

        public IFunction Functional { get; set; }

        public object Content { get; set; }

        public EventHandler OnClosed { get; set; }

        public int CriteriaIndex { get; set; }

        public bool Is2d { get; set; }

        public bool HidePoints { get; set; }
    }
}