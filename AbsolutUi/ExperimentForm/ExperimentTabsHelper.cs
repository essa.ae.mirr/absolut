﻿namespace AbsolutUi
{
    using System.Windows.Forms;
    using System.Windows.Forms.Integration;

    using AbsolutUi.ExperimentForm;

    using ViewDemo;

    using Xceed.Wpf.AvalonDock.Layout;

    public class ExperimentTabsHelper
    {
        public static SliceEditorForm BuildForm(FunctionalTabInfo info)
        {
            var sliceEditorForm = new SliceEditorForm(info.Functional)
                                      {
                                          Text = info.FunctionalName,
                                          Mode2D = false,
                                          Isobars = false,
                                          TopLevel = false,
                                          FormBorderStyle = FormBorderStyle.None,
                                          NeedToShowPoint = !info.HidePoints
                                      };
            return sliceEditorForm;
        }

        public static SliceEditorForm BuildProblemForm(ProblemTabInfo info)
        {
            return new SliceEditorForm(info.Problem)
                       {
                           Text = info.TabName,
                           Mode2D = info.Is2d,
                           TopLevel = false,
                           FormBorderStyle = FormBorderStyle.None
                       };
        }

        public static  SliceEditorForm BuildCriteriaAwareForm(FunctionalTabInfo info)
        {
            return new SliceEditorForm(info.Functional, info.CriteriaIndex)
                       {
                           Text = info.FunctionalName,
                           Mode2D = info.Is2d,
                           TopLevel = false,
                           FormBorderStyle = FormBorderStyle.None,
                           NeedToShowPoint = !info.HidePoints

            };
        }

        public static LayoutDocument BuildLayoutDocumentFromForm(FunctionalTabInfo info)
        {
            var layoutDocument = new LayoutDocument()
                                     {
                                         Content = new WindowsFormsHost { Child = info.Content as Control },
                                         CanClose = true,
                                         Title = info.FunctionalName
                                     };
            layoutDocument.Closed += info.OnClosed;
            return layoutDocument;
        }

        public static LayoutDocument BuildLayoutDocumentFromControl(FunctionalTabInfo info)
        {
            var layoutDocument = new LayoutDocument()
                                     {
                                         Content = info.Content,
                                         CanClose = true,
                                         Title = info.FunctionalName
                                     };
            layoutDocument.Closed += info.OnClosed;
            return layoutDocument;
        }

        public static LayoutDocument BuildLayoutDocument(ProblemTabInfo info)
        {
            var layoutDocument = new LayoutDocument()
                                     {
                                         Content = new WindowsFormsHost { Child = info.Content },
                                         CanClose = true,
                                         Title = info.TabName
                                     };

            layoutDocument.Closed += info.OnClosed;
            return layoutDocument;
        }

        public static LayoutAnchorable BuildAnchorableTab(AnchorableTabInfo info)
        {
            return new LayoutAnchorable()
                       {
                           Content = info.Content,
                           AutoHideMinWidth = 400,
                           CanClose = false,
                           CanHide = false,
                           Title = info.Title
                       };
        }

        public static SliceEditorForm BuildCriteriaAwareProblemForm(ProblemTabInfo info)
        {
            return new SliceEditorForm(info.Problem, info.CriteriaIndex)
                       {
                           Text = info.TabName,
                           Mode2D = info.Is2d,
                           TopLevel = false,
                           FormBorderStyle = FormBorderStyle.None
                       };
        }
    }
}