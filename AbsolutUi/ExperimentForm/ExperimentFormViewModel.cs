﻿using Absolut_Model;
using NLog;

namespace AbsolutUi
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Threading;
    using AbsolutUi.Custom_controls;
    using AbsolutUi.ExperimentForm;
    using Algorithms;
    using Algorithms.Methods;
    using Functions;
    using global::Properties;
    using OxyPlot;
    using OxyPlot.Series;
    using ViewDemo;
    using ViewDemo.Presenters;
    using Xceed.Wpf.AvalonDock.Layout;

    public class ExperimentFormViewModel : INotifyPropertyChanged, ISearchInformationView, IProblemaView,
        IParetoFrontView, IMethodView
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        public Guid ExpId { get; }

        public IProblem Problem { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private const string CriteriaAfterCostraint = "Criteria After Constraint";

        private int _criteriaCount;

        public int CriteriaCount
        {
            get { return this._criteriaCount; }
            set
            {
                this._criteriaCount = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("CriteriaCount"));
            }
        }

        public int Dimension
        {
            get => _dimension;
            set
            {
                _dimension = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("Dimension"));
            }
        }

        public int ConstraintsCount
        {
            get => _constraintsCount;
            set
            {
                _constraintsCount = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("ConstraintsCount"));
            }
        }

        public String AlgInfo
        {
            get => _algInfo;
            set
            {
                _algInfo = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("AlgInfo"));
            } 

        
        }

        public ObservableCollection<MethodPointRow> MethodPoints { get; set; }

        public ObservableCollection<MethodPointRow> ParetoFront { get; set; }

        public ObservableCollection<DataPoint> ApproximateParetoFront { get; set; }

        public ObservableCollection<ScatterPoint> ApproximateScatterParetoFront { get; set; }

        public ObservableCollection<FunctionalsMenuItemViewModel> MenuItems { get; set; }

        public bool Is2d { get; set; } = false;

        private SearchInformationPresenter _expPresenter;

        private ProblemaPresenter _problemaPresenter;

        private DispatcherTimer timer;

        private FunctionalsMenuItemViewModel _functionalsMenuItemViewModel;

        private Dictionary<string, LayoutDocument> _functionalsNamesToTabs = new Dictionary<string, LayoutDocument>();

        private Dictionary<string, SliceEditorForm> _childForms = new Dictionary<string, SliceEditorForm>();

        private IFunctionalTabsView _view;

        private MethodsOnClickHandlers _methodHandlers;

        private MethodPresenter _methodPresenter;
        private int _dimension;
        private int _constraintsCount;
        private string _algInfo;
        private ParetoFrontPresenter _paretoFrontPresenter;

        public ExperimentFormViewModel(IFunctionalTabsView view, Guid expId)
        {
            ExpId = expId;
            _view = view;
            _expPresenter = new SearchInformationPresenter(this);
            _problemaPresenter = new ProblemaPresenter(this);
            this._methodPresenter = new MethodPresenter(this);
            this._methodPresenter.ExpId = ExpId;
            _paretoFrontPresenter = new ParetoFrontPresenter(this) {ExpId = expId};
            _expPresenter.ExpId = expId;
            _problemaPresenter.ExpId = expId;
            MethodPoints = new ObservableCollection<MethodPointRow>();
            ParetoFront = new ObservableCollection<MethodPointRow>();
            ApproximateParetoFront = new ObservableCollection<DataPoint>();
            ApproximateScatterParetoFront = new ObservableCollection<ScatterPoint>();
            timer = new DispatcherTimer();
            timer.Tick += (sender, args) => Next();
            timer.Interval = new TimeSpan(0, 0, 0, 0, 10);

            this._methodHandlers = new MethodsOnClickHandlers(this._methodPresenter);
            RestoreMenuItems();
        }

        public void ClearPoints()
        {
            MethodPoints?.Clear();
            ParetoFront?.Clear();
            ApproximateParetoFront?.Clear();
            timer.Stop();
            foreach (var form in _childForms)
            {
                form.Value.ClearPoints();
            }
        }

        public void AddPoint(MethodPoint p)
        {
            MethodPoints.Add(new MethodPointRow(p));

            foreach (var childForm in this._childForms)
            {
                childForm.Value.AddPoint(p);
            }
        }

        public void DeleteLastPoint(MethodPoint p)
        {
            if (MethodPoints.Count > 1) MethodPoints.RemoveAt(MethodPoints.Count - 1);

            foreach (var childForm in this._childForms)
            {
                childForm.Value.DeleteLastPoint(p);
            }
        }

        public void SetProblem(IProblem problem)
        {
            _logger.Debug("Setting problem");
            MethodPoints.Clear();
            CriteriaCount = problem.NumberOfCriterions;
            Dimension = problem.Dimension;
            ConstraintsCount = problem.NumberOfConstraints;
            ClearFunctionalWindows();
            AddDefaultFunctionalsToolStripItems(this.ShowAllItemOnClick, this.HideAllItemOnClick);
            this.SetupFunctionalsElements(problem);
            var searchAlg = this._methodPresenter.GetAlg();
            AlgInfo = AlgDescriptionGenerator.generate(searchAlg);
            UpdateMethodProperties(searchAlg);
            _logger.Debug("Setting problem ended");
            
            
        }

        private void ClearFunctionalWindows()
        {
            _view.DeleteTabs();
            _childForms.Clear();
        }

        public void Next()
        {
            _expPresenter.Next();
        }

        public void Prev()
        {
            _expPresenter.Prev();
        }

        public void Pause()
        {
            timer.Stop();
        }

        public void Start()
        {
            timer.Start();
        }

        public void AddDefaultFunctionalsToolStripItems(Action showAllItemOnClick, Action hideAllItemOnClick)
        {
            var showAllItem =
                new FunctionalsMenuItemViewModel(showAllItemOnClick)
                {
                    Header = "Show All Functionals",
                    IsCheckable = false
                };

            var hideAllItem =
                new FunctionalsMenuItemViewModel(hideAllItemOnClick)
                {
                    Header = "Hide All Functionals",
                    IsCheckable = false
                };

            _functionalsMenuItemViewModel.MenuItems.Add(showAllItem);
            _functionalsMenuItemViewModel.MenuItems.Add(hideAllItem);
        }

        public void AddCriteriaAfterConstraintMenuItem(Action<string> onClick)
        {
            var showAfterConstraints = new FunctionalsMenuItemViewModel(onClick, CriteriaAfterCostraint);

            _functionalsMenuItemViewModel.MenuItems.Add(showAfterConstraints);
        }

        public void UncheckMenuItems()
        {
            foreach (var dropDownItem in _functionalsMenuItemViewModel.MenuItems)
            {
                if (dropDownItem.IsCheckable)
                {
                    dropDownItem.Checked = false;
                }
            }
        }

        public void CheckMenuItems()
        {
            foreach (var dropDownItem in _functionalsMenuItemViewModel.MenuItems)
            {
                if (dropDownItem.IsCheckable)
                {
                    dropDownItem.Checked = true;
                }
            }
        }

        public void UncheckFunctionalsSubMenu(string s)
        {
            var item = _functionalsMenuItemViewModel.MenuItems.FirstOrDefault(x => x.Header == s);
            if (item == null) return;
            if (item.IsCheckable)
                item.Checked = false;
        }

        public void AddFunctionalMenuItem(string functionalName, Action<string> action)
        {
            var constraintMenuItem = new FunctionalsMenuItemViewModel(action, functionalName) {IsCheckable = true};
            constraintMenuItem.Checked = true;
            _functionalsMenuItemViewModel.MenuItems.Add(constraintMenuItem);
        }

        public void CheckMenuItem(string s)
        {
            var item = _functionalsMenuItemViewModel.MenuItems.First(x => x.Header == s);
            if (item == null) return;
            if (item.IsCheckable)
                item.Checked = true;
        }

        public void ParetoNotSupported()
        {
            ParetoFront.Clear();
            ApproximateParetoFront.Clear();
        }

        public void UpdatePoints(List<MethodPoint> paretoFront)
        {
            foreach (var methodPoint in paretoFront)
            {
                ParetoFront.Add(new MethodPointRow(methodPoint));
                ApproximateScatterParetoFront.Add(
                    new ScatterPoint(methodPoint.Criterions[0], methodPoint.Criterions[1]) {Size = 5, Value = 4});
            }
        }

        public void UpdateApproximatePoints(List<MethodPoint> approximateParetoFront)
        {
            ApproximateParetoFront.Clear();
            foreach (var methodPoint in approximateParetoFront)
            {
                if (methodPoint.Criterions.Count < 2) return;
                ApproximateParetoFront.Add(new DataPoint(methodPoint.Criterions[0], methodPoint.Criterions[1]));
            }
        }


        public void UpdateMethodProperties(ISearchAlg alg)
        {
            if (alg.GetSupportedFeatures().Contains(SupportedFeatures.Convolution) && alg.Problem.Criterions.Count > 1 &&
               !this._view.DoesFunctionalsTabExists(AbsolutUiConstants.CONVOLUTION_MENU_ITEM))
            {
                this.SetupConvolutionMenuItem(AbsolutUiConstants.CONVOLUTION_MENU_ITEM, alg.GetConvolution());
            }

            var paretoFront = "Pareto Front";
            if (alg.GetSupportedFeatures().Contains(SupportedFeatures.ParetoFront) && alg.Problem.Criterions.Count > 1 
                && _view.DoesAnchorableTabExist(paretoFront))
            {
                var paretoFrontDataGrid = new ParetoFrontDataGrid("ParetoFront");
                var pointsInfo = new AnchorableTabInfo() {Content = paretoFrontDataGrid, Title = paretoFront};
                var layoutDocument = ExperimentTabsHelper.BuildAnchorableTab(pointsInfo);
                this._view.AddAnchorableTab(layoutDocument);

                var plot = OxyPlotHelper.BuildApproximateParetoPlot();
                var front = "Approximate Pareto Front";
                var plotInfo = new AnchorableTabInfo() {Content = plot, Title = front};
                var approximateParetoFront = ExperimentTabsHelper.BuildAnchorableTab(plotInfo);
                this._view.AddAnchorableTab(approximateParetoFront);
            }
        }

        public void UpdateIterarion(int curIter)
        {
        }

        public void CreateCustomFunctionMenu(string functionName, Dictionary<PropertyInfo, object> info)
        {
        }

        public void СreateUsualFunctionMenu()
        {
        }

        public void ShowAdditionalFunction(IFunction f)
        {
        }


        private void SetupFunctionalsElements(IProblem problem)
        {
            Problem = problem;
            int functionalNum = 0;
            var k = 0;
            if (problem.Constraints.Count != 0)
            {
                foreach (var problemCriteria in problem.Criterions)
                {
                    var functionalName = problemCriteria.Name ?? "Criteria " + k + " After Constraints";
                    this.SetupCriteriaAfterConstraint(functionalName, k);
                    k++;
                }
            }

            foreach (var p in problem.Constraints)
            {
                var functionalName = p.Name ?? "Constraint " + functionalNum;

                this.SetupFunctional(functionalName, Problem.Functionals[functionalNum], -1, true);
                functionalNum++;
            }

            k = 0;
            foreach (var p in problem.Criterions)
            {
                var functionalName = p.Name ?? "Criteria " + k;
                this.SetupFunctional(functionalName, this.Problem.Functionals[functionalNum], k, false);
                functionalNum++;
                k++;
            }
        }

        private void SetupCriteriaAfterConstraint(string criteriaAfterConstraint, int criteriaIndex)
        {
            void LayoutDocumentOnClosed(object sender, EventArgs args) =>
                this.UncheckFunctionalsSubMenu(criteriaAfterConstraint);

            ProblemTabInfo info = new ProblemTabInfo()
            {
                Problem = Problem,
                TabName = criteriaAfterConstraint,
                Is2d = Is2d,
                OnClosed = LayoutDocumentOnClosed,
                CriteriaIndex = criteriaIndex
            };

            var problemForm = ExperimentTabsHelper.BuildCriteriaAwareProblemForm(info);
            info.Content = problemForm;
            _childForms.Add(info.TabName, problemForm);

            var layoutDocument = ExperimentTabsHelper.BuildLayoutDocument(info);
            this._view.AddFunctionalTab(layoutDocument);
            this._functionalsNamesToTabs[criteriaAfterConstraint] = layoutDocument;
            AddCriteriaAfterConstraintMenuItem(s => { this._view.AddFunctionalTab(layoutDocument); });
        }

        private void SetupFunctional(string functionalName, IFunction f, int criteriaNum, bool needToHidePoints)
        {
            var tabInfo = new FunctionalTabInfo()
            {
                CriteriaIndex = criteriaNum,
                Functional = f,
                FunctionalName = functionalName,
                HidePoints = needToHidePoints,
                OnClosed = (sender, args) => { UncheckFunctionalsSubMenu(functionalName); }
            };

            var sliceEditorForm = criteriaNum > 0
                ? ExperimentTabsHelper.BuildCriteriaAwareForm(tabInfo)
                : ExperimentTabsHelper.BuildForm(tabInfo);
            tabInfo.Content = sliceEditorForm;
            _childForms.Add(tabInfo.FunctionalName + tabInfo.CriteriaIndex, sliceEditorForm);

            var layoutDocument = ExperimentTabsHelper.BuildLayoutDocumentFromForm(tabInfo);

            this._view.AddFunctionalTab(layoutDocument);
            this._functionalsNamesToTabs[functionalName] = layoutDocument;
            AddFunctionalMenuItem(
                functionalName,
                s => { CheckMenuItem(s); });
        }

        private void RestoreMenuItems()
        {
            _functionalsMenuItemViewModel = FunctionalsMenuItemsHelper.BuildMainMenuFunctionalViewModel();
            MenuItems = FunctionalsMenuItemsHelper.BuildFunctionalsMenuItemViewModels(
                _functionalsMenuItemViewModel,
                this._methodHandlers);
        }

        private void HideAllItemOnClick()
        {
            UncheckMenuItems();
            this._view.HideAllTabs();
        }

        private void ShowAllItemOnClick()
        {
            CheckMenuItems();
            this._view.ShowAllTabs(this._functionalsNamesToTabs.Values.ToList());
        }

        private void SetupConvolutionMenuItem(string nameOfMenuItem, IFunction convolutionFunction)
        {
            var info = new FunctionalTabInfo()
            {
                Content = new ConvolutionControl(convolutionFunction),
                FunctionalName = nameOfMenuItem,
                Functional = convolutionFunction,
                OnClosed = (sender, args) => { UncheckFunctionalsSubMenu(nameOfMenuItem); }
            };
            var layoutDocument = ExperimentTabsHelper.BuildLayoutDocumentFromControl(info);
            this._view.AddFunctionalTab(layoutDocument);
            this._functionalsNamesToTabs[nameOfMenuItem] = layoutDocument;
            AddFunctionalMenuItem(
                nameOfMenuItem,
                s => { this._view.AddFunctionalTab(this._functionalsNamesToTabs[s]); });
        }

        public void Restart()
        {
            _expPresenter.ResetPoints();
        }
    }
}