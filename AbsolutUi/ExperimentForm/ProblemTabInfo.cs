﻿namespace AbsolutUi.ExperimentForm
{
    using System;
    using System.Windows.Forms;

    using Algorithms;

    public class ProblemTabInfo
    {
        public bool Is2d { get; set; }

        public string TabName { get; set; }

        public IProblem Problem { get; set; }

        public Form Content { get; set; }

        public EventHandler OnClosed { get; set; }

        public int CriteriaIndex { get; set; }
    }
}