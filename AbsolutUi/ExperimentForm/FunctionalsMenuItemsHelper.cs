﻿namespace AbsolutUi
{
    using System.Collections.ObjectModel;

    using AbsolutUi.Custom_controls;
    using AbsolutUi.ExperimentForm;

    public class FunctionalsMenuItemsHelper
    {

        public static ObservableCollection<FunctionalsMenuItemViewModel> BuildFunctionalsMenuItemViewModels(
            FunctionalsMenuItemViewModel topViewModel, MethodsOnClickHandlers handlers)
        {
            return new ObservableCollection<FunctionalsMenuItemViewModel>
                       {
                           new FunctionalsMenuItemViewModel
                               {
                                   Header
                                       =
                                       "Method",
                                   MenuItems
                                       =
                                       new
                                       ObservableCollection<FunctionalsMenuItemViewModel>()
                                           {
                                               new
                                               FunctionalsMenuItemViewModel(
                                                   handlers.OnChangeMethod)
                                                   {
                                                       Header
                                                           = "Absolut's Library Method..."
                                                   },
                                               new
                                               FunctionalsMenuItemViewModel(
                                                   handlers.OnExternalMethod)
                                                   {
                                                       Header
                                                           = "External Method..."
                                                   },
                                               new
                                               FunctionalsMenuItemViewModel(
                                                   handlers.OnMethodParameters)
                                                   {
                                                       Header
                                                           = "Method Parameters..."
                                                   }
                                           }
                               },
                           new FunctionalsMenuItemViewModel
                               {
                                   Header
                                       =
                                       "Problem",
                                   MenuItems
                                       =
                                       new
                                       ObservableCollection<FunctionalsMenuItemViewModel>
                                           {
                                               new
                                               FunctionalsMenuItemViewModel(
                                                  handlers.OnLibrary)
                                                   {
                                                       Header
                                                           = "Absolut's Library Problems..."
                                                   },
                                               new
                                               FunctionalsMenuItemViewModel(
                                                   handlers.OnDll)
                                                   {
                                                       Header
                                                           = "Dll.."
                                                   }
                                           }
                               },
                         topViewModel
                       };
        }

        public static FunctionalsMenuItemViewModel BuildMainMenuFunctionalViewModel()
        {
            return new FunctionalsMenuItemViewModel
                       {
                           Header = "Functionals",
                           MenuItems =
                               new ObservableCollection<
                                   FunctionalsMenuItemViewModel>()
                       };
        }
    }
}