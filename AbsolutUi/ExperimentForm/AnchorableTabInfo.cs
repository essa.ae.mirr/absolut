﻿namespace AbsolutUi.ExperimentForm
{
    using System.Windows.Controls;

    public class AnchorableTabInfo
    {
        public string Title { get; set; }

        public Control Content { get; set; }
    }
}