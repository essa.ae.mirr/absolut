﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AbsolutUi
{
    using Algorithms;

    /// <summary>
    /// Interaction logic for ExaminMcoProblemSettings.xaml
    /// </summary>
    public partial class ExaminMcoProblemSettings : Window
    {
        public IProblem Problem { get; set; }
        public List<int> Criterions { get; set; }
        public List<int> Constraints { get; set; }
        public ExaminMcoProblemSettings()
        {
            InitializeComponent();
        }

        private void OkButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var criterions = this._criterionsTextBox.Text.Split(',').Select(int.Parse).ToList();
                var constraints = this._constraintsTextBox.Text.Split(',').Select(int.Parse).ToList();
                Criterions = criterions;
                Constraints = constraints;
            }
            catch (Exception e1)
            {
                System.Console.Error.WriteLine(e1.StackTrace);
            }

            
        }
    }
}
