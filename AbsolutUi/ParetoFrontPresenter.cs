﻿using System;

using Absolut_Model;

using Algorithms;

namespace AbsolutUi
{
    // EventListenerAdapter содержит пустые реализации методов-обработчиков событий
    public class ParetoFrontPresenter : EventListenerAdapter
    {
        // инициализация модели
        private IModel _m = ModelFactory.Build();

        private Guid _expId;

        private IParetoFrontView _view;

        // подписка на эксперимент во время получения его id
        public Guid ExpId
        {
            get
            {
                return _expId;
            }

            set
            {
                _expId = value;
                _m.SubscribeExperiment(this, value);
            }
        }

        public ParetoFrontPresenter(IParetoFrontView view)
        {
            _view = view;
        }
        
        // при изменении задачи необходимо получить
        // новую оценку области Парето и передать ее
        // в связанное отображение

        public override void OnProblemChanged()
        {
            var searchAlg = this._m.GetAlg(this._expId);
            if (searchAlg.GetSupportedFeatures().Contains(SupportedFeatures.ParetoFront) && searchAlg.Problem.Criterions.Count > 1)
            {
                _view.UpdatePoints(searchAlg.GetParetoFront());
                this._view.UpdateApproximatePoints(ParetoFrontHelper.LazyApproximateParetoFront(searchAlg.Problem));
            }
            else
            {
                _view.ParetoNotSupported();
            }
        }
    }
}