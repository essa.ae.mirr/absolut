﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AbsolutUi.Custom_controls;
using Absolut_Model;
using Algorithms;

namespace AbsolutUi
{
    class ConvolutionPresenter : EventListenerAdapter
    {
        private IConvolutionView _view;
        private IModel _m = ModelFactory.Build();
        private Guid _expId;

        public Guid ExpId
        {
            get => _expId;
            set
            {
                _expId = value;
                InitialSetup();
            }
        }

        private void InitialSetup()
        {
            _m.SubscribeExperiment(this, _expId);
            _view.ClearView();
        }

        public ConvolutionPresenter(IConvolutionView view)
        {
            _view = view;
        }

        public override void OnMainFunctionChanged()
        {
            UpdateView();
        }

        private void UpdateView()
        {
            var alg = _m.GetAlg(_expId);
            if(!alg.GetSupportedFeatures().Contains(SupportedFeatures.Convolution)) return;
            var problem = _m.GetProblem(_expId);
            _view.SetCriteriaCount(problem.NumberOfCriterions);
            _view.SetConvolutionFunction(alg.GetConvolution());
        }

        public override void OnMethodChanged()
        {
            UpdateView();
        }
    }
}