﻿using System;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using Color = System.Windows.Media.Color;

namespace AbsolutUi.Custom_controls
{
    public class MethodPointsDataGrid : DataGrid
    {
        private const string Criteria = "y";

        private const string Constraint = "c";

        private const string TrialPoint = "x";


        public int? CriteriaCount
        {
            get => GetValue(CriteriaCountProperty) as int?;
            set => SetValue(CriteriaCountProperty, value);
        }

        public int? Dimension
        {
            get => GetValue(DimensionCountProperty) as int?;
            set => SetValue(DimensionCountProperty, value);
        }

        public int? ConstraintsCount
        {
            get => GetValue(ConstraintsCountProperty) as int?;
            set => SetValue(ConstraintsCountProperty, value);
        }

        public static readonly DependencyProperty CriteriaCountProperty = DependencyProperty.Register(
            "CriteriaCount",
            typeof(int?), typeof(MethodPointsDataGrid),
            new PropertyMetadata(OnProblemParamsChanged));

        public static readonly DependencyProperty DimensionCountProperty = DependencyProperty.Register(
            "Dimension",
            typeof(int?), typeof(MethodPointsDataGrid),
            new PropertyMetadata(OnProblemParamsChanged));

        public static readonly DependencyProperty ConstraintsCountProperty = DependencyProperty.Register(
            "ConstraintsCount",
            typeof(int?), typeof(MethodPointsDataGrid),
            new PropertyMetadata(OnProblemParamsChanged));

        private static Style _cellStyle;

        static void OnProblemParamsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var dataGrid = d as MethodPointsDataGrid;
            dataGrid.Columns.Clear();
            // Set iteration number columns
            dataGrid.Columns.Add(new DataGridTextColumn() {Header = "Iteration", Binding = new Binding("Iteration")});
            dataGrid.Columns.Add(new DataGridTextColumn() {Header = "Process", Binding = new Binding("IdProcess")});
           // dataGrid.Columns.Add(new DataGridTextColumn() { Header = "Convolution", Binding = new Binding("Convolution") });
            // Generate columns for Criterions
            _cellStyle = new Style(typeof(DataGridCell))
            {
                Setters = { new Setter(BackgroundProperty, new SolidColorBrush(Color.FromRgb(218, 218, 217))) },
            };
            if (dataGrid.CriteriaCount != null)
            {
                for (int i = 0; i < dataGrid.CriteriaCount; i++)
                {
                    dataGrid.Columns.Add(new DataGridTextColumn()
                    {
                        Header = Criteria + i,
                        Binding = new Binding("Criterions")
                        {
                            ConverterParameter = Criteria + i,
                            Converter = new ListConverter(Criteria)
                        }
                    });
                }
            }

            if (dataGrid.Dimension != null)
            {
                for (int i = 0; i < dataGrid.Dimension; i++)
                {
                    dataGrid.Columns.Add(new DataGridTextColumn()
                    {
                        Header = TrialPoint + i,
                        
                        Binding = new Binding("X")
                        {
                            ConverterParameter = TrialPoint + i,
                            Converter = new ListConverter(TrialPoint)
                        }
                    });
                }
            }


            //if (dataGrid.ConstraintsCount != null)
            //{
            //    for (int i = 0; i < dataGrid.ConstraintsCount; i++)
            //    {
            //        dataGrid.Columns.Add(new DataGridTextColumn()
            //        {
                        
            //            Header = TrialPoint + i,
            //            Binding = new Binding("Constraints")
            //            {
            //                ConverterParameter = Constraint + i,
            //                Converter = new ListConverter(Constraint)
            //            }
            //        });
            //    }
            //}
        }
    }

    public class ListConverter : IValueConverter
    {
        private readonly string _columnPrefix;

        public ListConverter(string columnPrefix)
        {
            _columnPrefix = columnPrefix;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ObservableCollection<double> criteriaValues && parameter != null)
            {
                string columnName = parameter.ToString();
                var criteriaIndex = System.Convert.ToInt32(columnName.Replace(_columnPrefix, ""));
                if (criteriaValues.Count > criteriaIndex) return criteriaValues[criteriaIndex].ToString("N6");
                else return "NAN";
            }

            return "NAN";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}