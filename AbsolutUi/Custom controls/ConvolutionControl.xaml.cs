﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Functions;
using ViewDemo;
using ComboBox = System.Windows.Controls.ComboBox;
using UserControl = System.Windows.Controls.UserControl;

namespace AbsolutUi.Custom_controls
{
    /// <summary>
    /// Interaction logic for ConvolutionControl.xaml
    /// </summary>
    public partial class ConvolutionControl : UserControl
    {
        Dictionary<int, double> _lambdasMap = new Dictionary<int, double>();
        private SliceEditorForm _sliceEditorForm;
        private IFunction _convolutionFunction;

        public ConvolutionControl(IFunction convolutionFunction)
        {
            InitializeComponent();

            _convolutionFunction = convolutionFunction;
            _sliceEditorForm = new SliceEditorForm(_convolutionFunction)
            {
                Text = "Convolution",
                Mode2D = false,
                TopLevel = false,
                FormBorderStyle = FormBorderStyle.None,
                NeedToShowPoint = false

            };
            WindowsFormsHost.Child = _sliceEditorForm;

            SetupComboBox(_convolutionFunction);
            SetupSlider();
        }

        private void SetupComboBox(IFunction convolutionFunction)
        {
            var comboBoxItemsSource = new List<String>();
            int i = 0;
            foreach (var convolutionFunctionLambda in convolutionFunction.Lambdas)
            {
                _lambdasMap.Add(i, convolutionFunctionLambda);
                comboBoxItemsSource.Add("Lambda " + i);
                i++;
            }

            ComboBox.ItemsSource = comboBoxItemsSource;
            ComboBox.SelectedIndex = 0;

        }

        private void SetupSlider()
        {
            Slider.Maximum = 1;
            Slider.Minimum = 0;
            this.Slider.TickFrequency = 10;
            Slider.Value = _lambdasMap[0];
        }


        private void ComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox box = sender as ComboBox;
            Slider.Value = _lambdasMap[box.SelectedIndex];
        }


        private void Slider_OnValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var comboBoxSelectedIndex = ComboBox.SelectedIndex;
            this._lambdasMap[comboBoxSelectedIndex] = this.Slider.Value;
            _convolutionFunction.Lambdas[comboBoxSelectedIndex] = Slider.Value;

            var anotherIndex = comboBoxSelectedIndex == 0 ? 1 : 0;

            this._lambdasMap[anotherIndex] = 1 - this.Slider.Value;
            _convolutionFunction.Lambdas[anotherIndex] = 1 - Slider.Value;

            _sliceEditorForm.FunctionToSlice = _convolutionFunction;

        }
    }
}
