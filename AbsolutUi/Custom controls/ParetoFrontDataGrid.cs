﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;

namespace AbsolutUi.Custom_controls
{
    public class ParetoFrontDataGrid : DataGrid
    {
        private const string Criteria = "y";

        public int? CriteriaCount
        {
            get => GetValue(CriteriaCountProperty) as int?;
            set => SetValue(CriteriaCountProperty, value);
        }

        public static readonly DependencyProperty CriteriaCountProperty = DependencyProperty.Register(
            "LambdaCount",
            typeof(int?),
            typeof(ParetoFrontDataGrid),
            new PropertyMetadata(OnProblemParamsChanged));

        public ParetoFrontDataGrid(string binding)
        {
            SetBinding(CriteriaCountProperty, new Binding("CriteriaCount"));
            BindingOperations.SetBinding(this, DataGrid.ItemsSourceProperty, new Binding(binding));
        }

        static void OnProblemParamsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var dataGrid = d as ParetoFrontDataGrid;
            dataGrid.Columns.Clear();

            // Generate columns for Criterions
            new Style(typeof(DataGridCell))
                {
                    Setters =
                        {
                            new Setter(
                                BackgroundProperty,
                                new SolidColorBrush(Color.FromRgb(218, 218, 217)))
                        },
                };
            if (dataGrid.CriteriaCount == null) return;
            for (int i = 0; i < dataGrid.CriteriaCount; i++)
            {
                dataGrid.Columns.Add(
                    new DataGridTextColumn()
                        {
                            Header = Criteria + i,
                            Binding = new Binding("Criterions")
                                          {
                                              ConverterParameter =
                                                  Criteria + i,
                                              Converter = new ListConverter(
                                                  Criteria)
                                          }
                        });
            }
        }
    }
}