﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xceed.Wpf.Toolkit;

namespace AbsolutUi.Custom_controls
{
    public class FunctionalsMenuItemViewModel
    {
        private bool _checked;
        public ObservableCollection<FunctionalsMenuItemViewModel> MenuItems { get; set; }

        public FunctionalsMenuItemViewModel(Action onClick)
        {
            Command = new CommandViewModel(onClick);
        }

        public FunctionalsMenuItemViewModel(Action<string> onClick, string header)
        {
            Header = header;
            Command = new PassNameToCommandViewModel(onClick, header);

        }

        public FunctionalsMenuItemViewModel()
        {
            Command = new CommandViewModel(Execute);
        }

        public string Header { get; set; }
        public bool IsCheckable { get; set; }
       

        public ICommand Command { get; }

        public bool Checked
        {
            get { return _checked; }
            set { _checked = value; }
        }

        private void Execute()
        {
         
        }
    }

    public class CommandViewModel : ICommand
    {
        private readonly Action _action;

        public CommandViewModel(Action action)
        {
            _action = action;
        }

        public void Execute(object o)
        {
            _action();
        }

        public bool CanExecute(object o)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { }
            remove { }
        }
    }


    public class PassNameToCommandViewModel : ICommand
    {
        private readonly Action<string> _action;
        private string _param;

        public PassNameToCommandViewModel(Action<string> action, string param)
        {
            _action = action;
            _param = param;
        }

        public void Execute(object o)
        {
           
            _action(_param);
        }

        public bool CanExecute(object o)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { }
            remove { }
        }
    }
}