﻿using System.Collections.Generic;
using Functions;

namespace AbsolutUi.Custom_controls
{
    public interface IConvolutionView
    {
        void ClearView();
        void SetConvolutionFunction(IFunction convolution);
        void SetCriteriaCount(int count);
    }
}