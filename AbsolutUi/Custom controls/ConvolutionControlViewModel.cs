﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Algorithms;
using Functions;

namespace AbsolutUi.Custom_controls
{
    class ConvolutionControlViewModel
    {
        public IFunction ConvolutionFunction { get; set; }
        public int NumberOfCriterias { get; set; }
        public ObservableCollection<double> ListOfLambdas { get; set; }

        public ConvolutionControlViewModel()
        {

        }


    }
}