﻿using System.Collections.ObjectModel;
using Algorithms.Methods;

namespace AbsolutUi.Custom_controls
{
    public class MethodPointRow
    {
        
        public int IdFun { get; set; }

        // флаг окончания процесса поиска минимума на данной точке

        public AlgEnd IsEnd { get; set; }

        // координаты точки

        public ObservableCollection<double> X { get; set; }
        // образ на развертке Пеано

        public double EvolventX { get; set; }

        // вычисленное значение критерия

        public double? Y { get; set; }

        // номер итерации

        public int Iteration { get; set; }

        // номер процесса

        public int IdProcess { get; set; }

        // значения критериев в точке

        public ObservableCollection<double> Criterions { get; set; }

        // значения ограничений в точке
        public ObservableCollection<double> Constraints { get; set; }

        public MethodPointRow(MethodPoint p)
        {
            IdFun = p.IdFun;
            IsEnd = p.IsEnd;
            X = new ObservableCollection<double>(p.X);
            EvolventX = p.EvolventX;
            Iteration = p.Iteration;
            IdProcess = p.IdProcess;
           
            Criterions =  p.Criterions != null ? new ObservableCollection<double>(p.Criterions) : new ObservableCollection<double>();
            Constraints = p.Constraints != null ? new ObservableCollection<double>(p.Constraints) : new ObservableCollection<double>();
        }
    }
}
