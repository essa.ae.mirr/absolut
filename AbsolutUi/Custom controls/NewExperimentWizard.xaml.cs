﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AbsolutUi.Custom_controls
{
    using System.Windows.Forms;

    using Algorithms;

    using Functions;

    using Xceed.Wpf.Toolkit;
    using Xceed.Wpf.Toolkit.Core;

    using RadioButton = System.Windows.Controls.RadioButton;

    /// <summary>
    /// Interaction logic for NewExperimentWizard.xaml
    /// </summary>
    /// 
    public partial class NewExperimentWizard : Wizard
    {
        public bool IsAbsolut { get; private set; } = true;

        public bool IsMoea { get; private set; } = false;

        public bool IsExternal { get; private set; } = false;

        public bool IsDll { get; set; }

        public bool IsMco { get; set; }

        public string MethodName { get; set; }

        public DialogResult DialogResult { get; set; } = DialogResult.None;

        public string DllPath => (string)this._dllPath.Text;

        public string ConfigPath => (string)this._configPath.Text;

        public string ExaminPath => (string)this._examinPath.Text;

        public string ProblemaName { get; set; }

        public NewExperimentWizard()
        {
            InitializeComponent();
            this._absolutMethodChooser.NextPage = this._problemTypeChooser;
            this._moeaMethodChooser.NextPage = this._problemTypeChooser;
            this._externalMethodChooser.NextPage = this._examinFileChooser;
            this._examinFileChooser.PreviousPage = this._externalMethodChooser;
            this._examinFileChooser.NextPage = this._dllProblemChooser;
            this.fillAbsolutMethodsPage();
            this.fillMoeaMethodsPage();
            this.fillAbsolutProblemsPage();
            this._absolut.IsChecked = true;
        }

        private void fillAbsolutMethodsPage()
        {
            var options = AlgFactory.Options;
            foreach (var option in options)
            {
                this.createMethodRadioButton(option, this._absolutMethodDock, this.RbOnChecked);
            }
        }

        private void fillMoeaMethodsPage()
        {
            var options = AlgFactory.MoeaOptions;
            foreach (var option in options)
            {
                this.createMethodRadioButton(option, this._moeaDock, this.RbOnChecked);
            }
        }

        private void fillAbsolutProblemsPage()
        {
            var options = ProblemFactory.NotConstrainedOptionsList;
            foreach (var option in options)
            {
                this.createMethodRadioButton(option, this._absolutProblemDock, problemRbChecked);
            }

            var uiElement = this._absolutProblemDock.Children[0] as RadioButton;
            uiElement.IsChecked = true;
        }

        private void problemRbChecked(object sender, RoutedEventArgs routedEventArgs)
        {
            RadioButton rbSender = sender as RadioButton;
            ProblemaName = (string)rbSender.Content;

        }

        private void createMethodRadioButton(string option, DockPanel dock, RoutedEventHandler handler)
        {
            RadioButton rb = new RadioButton { Content = option };
            rb.Checked += handler;
            DockPanel.SetDock(rb, Dock.Top);
            dock.Children.Add(rb);
        }

        private void RbOnChecked(object sender, RoutedEventArgs routedEventArgs)
        {
            RadioButton rbSender = sender as RadioButton;
            MethodName = (string)rbSender.Content;
        }

        private void Absolut_Library_Method(object sender, RoutedEventArgs e)
        {
            this.IsAbsolut = true;
            this.IsMoea = false;
            this.IsExternal = false;
            this._methodChooser.NextPage = this._absolutMethodChooser;
            this._absolutMethodChooser.PreviousPage = this._methodChooser;
            if (this._absolutMethodDock == null) return;
            if (this._absolutMethodDock.Children.Count > 0)
            {
                var uiElement = this._absolutMethodDock.Children[0] as RadioButton;
                uiElement.IsChecked = true;
            }
            
        }

        private void Moea_Algorithms_OnChecked(object sender, RoutedEventArgs e)
        {
            this.IsAbsolut = false;
            this.IsMoea = true;
            this.IsExternal = false;
            this._methodChooser.NextPage = this._moeaMethodChooser;
            this._moeaMethodChooser.PreviousPage = this._methodChooser;
            if (this._moeaDock == null) return;
            if (this._moeaDock.Children.Count > 0)
            {
                var uiElement = this._moeaDock.Children[0] as RadioButton;
                uiElement.IsChecked = true;
            }
            
        }

        private void External_System_OnChecked(object sender, RoutedEventArgs e)
        {
            this.IsAbsolut = false;
            this.IsMoea = false;
            this.IsExternal = true;
            IsDll = true;
            this._methodChooser.NextPage = this._externalMethodChooser;
            this._externalMethodChooser.PreviousPage = this._methodChooser;
        }

        private void Absolut_Library_Problem_OnChecked(object sender, RoutedEventArgs e)
        {
            IsDll = false;
            if (this._problemTypeChooser != null && this._absolutProblemChooser != null)
            {
                this._problemTypeChooser.NextPage = this._absolutProblemChooser;
                this._absolutProblemChooser.PreviousPage = this._problemTypeChooser;
            }
         
        }

        private void DLL_Problem_OnChecked(object sender, RoutedEventArgs e)
        {
            IsDll = true;
            this._problemTypeChooser.NextPage = this._dllProblemChooser;
            this._dllProblemChooser.PreviousPage = this._problemTypeChooser;
        }

        private void Examin_OnChecked(object sender, RoutedEventArgs e)
        {
            IsMco = false;
        }

        private void ExaminMco_OnChecked(object sender, RoutedEventArgs e)
        {
            IsMco = true;
        }

        private void SelectDllFile_OnClick(object sender, RoutedEventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Filter = "Dynamic Library Files (DLL)|*.DLL;";
                var dialogResult = dialog.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {
                    this._dllPath.Text = dialog.FileName;
                }
            }
        }

        private void SelectConfigFile_OnClick(object sender, RoutedEventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Filter = "Configuration Files (XML)|*.XML;";
                var dialogResult = dialog.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {
                    this._configPath.Text = dialog.FileName;
                }
            }
        }

        private void NewExperimentWizard_OnCancel(object sender, RoutedEventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void NewExperimentWizard_OnFinish(object sender, CancelRoutedEventArgs e)
        {
            DialogResult = DialogResult.OK;
        }

        private void SelectExaminFile_OnClick(object sender, RoutedEventArgs e)
        {
            using (var dialog = new OpenFileDialog())
            {
                dialog.Filter = "Executable Files (EXE)|*.EXE;";
                var dialogResult = dialog.ShowDialog();
                if (dialogResult == DialogResult.OK)
                {
                    this._examinPath.Text = dialog.FileName;
                }
            }
        }

        private void NewExperimentWizard_OnNext(object sender, CancelRoutedEventArgs e)
        {
            if (this.CurrentPage == this._absolutMethodChooser)
            {
                this._problemTypeChooser.PreviousPage = this._absolutMethodChooser;
            }

            if (this.CurrentPage == this._moeaMethodChooser)
            {
                this._problemTypeChooser.PreviousPage = this._moeaMethodChooser;
            }
        }
    }
}