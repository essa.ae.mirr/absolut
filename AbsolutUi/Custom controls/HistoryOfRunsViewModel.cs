﻿namespace AbsolutUi.Custom_controls
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Drawing;
    using System.Linq;

    using AbsolutUi.ExperimentForm;

    using Absolut_Model;

    using OxyPlot;
    using OxyPlot.Series;

    public class HistoryOfRunsViewModel : EventListenerAdapter
    {
        private IModel _m = ModelFactory.Build();

        public ObservableCollection<RunInfo> History { get; }
        public ObservableCollection<DataPoint> OperationalChars { get; }

        public HistoryOfRunsViewModel()
        {
            this._m.Subscribe(this);
            History = new ObservableCollection<RunInfo>(this._m.GetHistory());
            OperationalChars = new ObservableCollection<DataPoint>();
            this.operationalCharsPlotRefresh();
            
        }

        public override void OnMethodChanged()
        {
            History.Add(this._m.GetHistory().Last());
            this.operationalCharsPlotRefresh();
        }

        private void operationalCharsPlotRefresh()
        {
           
            OperationalChars.Add(new DataPoint(0,0));
            int i = 0;
            int expCount = this.History.Count;
            var filteredHistory = this.History.Where(s => s.IterToMin != null);
            foreach (var result in filteredHistory)
            {
                if (result.WasMinimumReached)
                {
                    i++;
                }

                var b = this.OperationalChars.FirstOrDefault(x => Math.Abs(x.X - result.IterToMin.Value) < 0.00001);
                if (!b.IsDefined())
                {
                    this.OperationalChars.Remove(b);
                }

                var dataPoint = new DataPoint(result.IterToMin.Value, (float)i / expCount * 100.0f);
                this.OperationalChars.Add(dataPoint);
            }
        }
        
    }
}