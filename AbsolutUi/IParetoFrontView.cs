﻿using System.Collections.Generic;
using Algorithms.Methods;

namespace AbsolutUi
{
    public interface IParetoFrontView
    {
        void ParetoNotSupported();
        void UpdatePoints(List<MethodPoint> paretoFront);
        void UpdateApproximatePoints(List<MethodPoint> approximateParetoFront);
    }
}