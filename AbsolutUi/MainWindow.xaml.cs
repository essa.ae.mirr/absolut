﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;

using Algorithms;

using Properties;

using ViewDemo;
using ViewDemo.Dialogs;
using ViewDemo.Series;

using Xceed.Wpf.AvalonDock.Layout;
using Xceed.Wpf.Toolkit;

namespace AbsolutUi
{
    using System.ComponentModel;

    using AbsolutUi.Custom_controls;

    using Functions;

    using MessageBox = System.Windows.MessageBox;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, ISeriesExperimentsView, IExperimentView
    {
        private SeriesExperimentOperatorPresenter _seriesOperatorPresenter;

        private readonly ExperimentPresenter _experimentPresenter;

        private readonly BackgroundWorker worker = new BackgroundWorker();

        public MainWindow()
        {
            AppDomain.CurrentDomain.FirstChanceException += (sender, eventArgs) =>
                {
                    Console.Out.WriteLine(eventArgs.Exception.ToString());
                };
            InitializeComponent();

            _experimentPresenter = new ViewDemo.ExperimentPresenter();
            _experimentPresenter.AddView(this);
            _seriesOperatorPresenter = new SeriesExperimentOperatorPresenter(this);
        }

        private void OnNewSeriesClick(object sender, RoutedEventArgs e)
        {
            using (var name = new SeriesExperimentCreationForm())
            {
                var dr = name.ShowDialog();
                if (dr != System.Windows.Forms.DialogResult.OK) return;
                if (name.isAbsolut)
                {
                    using (var methodChoose = new MethodChooseDialog(AlgFactory.Options))
                    {
                        var methodName = methodChoose.ShowDialog();
                        if (methodName != System.Windows.Forms.DialogResult.OK) return;
                        name.Creator.UseMethod(methodChoose.ChoosedOption);
                    }
                }
                else if (name.isExamin)
                {
                    using (var examin = new ExaminSeriesSettings())
                    {
                        var examinRes = examin.ShowDialog();
                        if (examinRes != System.Windows.Forms.DialogResult.OK) return;
                        name.Creator.UseExamin(examin.examinPath, examin.dllPath, examin.configPath);
                    }
                }

                _seriesOperatorPresenter.AddNewExperiment(name.ExperimentName, name.Creator);
            }
        }

        public void ShowNewSeriesExperiment(Guid id, string expName, int expCount)
        {
            // создание формы серийного эксперимента
            var child = new ExperimentsDemo(id, expCount)
                            {
                                Text = expName,
                                TopLevel = false,
                                FormBorderStyle = FormBorderStyle.None
                            };

            // добавление новой вкладки с формой эксперимента
            _layoutPane.Children.Add(
                new LayoutDocument() { Title = expName, Content = new WindowsFormsHost() { Child = child } });
        }

        public void HideDeletedSeriesExperiment(Guid id)
        {
            // throw new NotImplementedException();
        }

        public void AddNewExperiment(ISearchAlg alg)
        {
            _experimentPresenter.AddNewExperimentWithAlg("Experiment " + string.Format("{0:G}", DateTime.Now), alg);
        }

        private void OnNewExperimentClick(object sender, RoutedEventArgs e)
        {
            NewExperimentWizard wizard = new NewExperimentWizard();
            Window window;
            window = new System.Windows.Window();
            window.Title = "New expirement...";
            window.Content = wizard;
            window.Width = 600;
            window.Height = 400;
            window.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

            // Window will be closed by Wizard because FinishButtonClosesWindow = true and CancelButtonClosesWindow = true
            window.ShowDialog();
            if (wizard.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    ISearchAlg alg = null;
                    if (wizard.IsAbsolut)
                    {
                        alg = AlgFactory.Build(wizard.MethodName);
                    }
                    else if (wizard.IsMoea)
                    {
                        alg = AlgFactory.Build(wizard.MethodName);
                    }
                    else if (wizard.IsExternal)
                    {
                        if (wizard.IsMco)
                        {
                            alg = AlgFactory.BuildExaminMcoAlg(wizard.DllPath, wizard.ConfigPath, wizard.ExaminPath);
                        }
                        else
                        {
                            alg = AlgFactory.BuildExaminAlg(wizard.DllPath, wizard.ConfigPath, wizard.ExaminPath);
                        }
                    }

                    IProblem problem = null;
                    if (wizard.IsDll)
                    {
                        problem = ProblemFactory.BuildProblemFromDll(wizard.DllPath, wizard.ConfigPath);
                    }
                    else
                    {
                        problem = ProblemFactory.BuildNotConstrainedProblem(wizard.ProblemaName);
                    }

                    alg.Problem = problem;
                    this._experimentPresenter.AddNewExperimentWithAlg("Experiment " + string.Format("{0:G}", DateTime.Now), alg);
                }
                catch (Exception e1)
                {
                    MessageBox.Show("Error happened, please check that all fields are filled");
                }


            }
        }

        public void ShowNewExperiment(Guid expId, string expName)
        {
            
            _layoutPane.Children.Add(
                new LayoutDocument() { Title = expName, Content = new ExperimentForm.ExperimentForm(expId) });
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this._experimentPresenter.OnApplicationClosed();
        }

        private void OnHistoryTableClick(object sender, RoutedEventArgs e)
        {
            _layoutPane.Children.Add(
                new LayoutDocument() { Title = "History of runs", Content = new HistoryControl() });
        }
    }
}