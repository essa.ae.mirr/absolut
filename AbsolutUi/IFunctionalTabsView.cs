﻿namespace AbsolutUi
{
    using System.Collections.Generic;

    using Xceed.Wpf.AvalonDock.Layout;

    public interface IFunctionalTabsView
    {
        void AddFunctionalTab(LayoutDocument tab);

        void ShowAllTabs(List<LayoutDocument> tabs);

        void HideAllTabs();

        void AddAnchorableTab(LayoutAnchorable tab);

        bool DoesAnchorableTabExist(string tabName);

        bool DoesFunctionalsTabExists(string convolutionMenuItem);
        void DeleteTabs();
    }
}