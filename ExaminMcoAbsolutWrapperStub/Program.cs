﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExaminMcoAbsolutWrapperStub
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            while (true)
            {
                Console.WriteLine("[Input]");
                var newArgs = Console.ReadLine();
                if ("quit".Equals(newArgs))
                {
                    return;
                }
                Console.WriteLine("New params are - " + newArgs);


            }

        }
    }
}
