#pragma once
#include "IProblem.h"
#include"problem_interface.h"
ref class ExaminProblem : ProblemInterface
{
private:
    IProblem * p;
public:
    ExaminProblem(IProblem * p);
    static const int OK = 0;
    static const int UNDEFINED = -1;
    static const int ProblemERROR = -2;

    virtual int SetConfigPath(String ^ configPath);
    virtual int SetDimension(int dimension);
    virtual int GetDimension();
    virtual int Initialize();
    virtual void GetBounds(array<double> ^ lower, array<double> ^ upper);
    virtual int GetOptimalValue(double& value);
    virtual int GetOptimalValue(double & value, int index);
    virtual int GetOptimumPoint(array<double>^ y);
    virtual int GetNumberOfFunctions();
    virtual int GetNumberOfConstraints();
    virtual int GetNumberOfCriterions();
    virtual double CalculateFunctionals(array<double> ^ y, int fNumber);

};

