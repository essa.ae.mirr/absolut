// ProblemManager.h

#pragma once
#include"IProblem.h"
#include"problem_manager.h"
using namespace System;

namespace ProblemManager {

	public ref class ProblemManager
	{
	private:
        TProblemManager * manager;
	public:
        static const int OK_ = 0;
        static const int ERROR_ = -2;
        ProblemManager();
        int LoadProblemLibrary(String ^ libPath);
        ProblemInterface^ GetProblem();
        ~ProblemManager();
	};
}
