﻿using System.Collections.Generic;
using Algorithms;
using Functions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestMethods
{
    [TestClass]
    public class ParallelAlgsSmokes
    {
        private IProblem p = ProblemFactory.BuildNotConstrainedProblem("Parabaloid");
        [TestMethod]
        public void AgpParalSmoke()
        {
            ISearchAlg a = AlgFactory.Build("AGP_paral");
            a.Problem = p;
            a.RecalcPoints();
            List<int> pointsFromDifferentProcesses = new List<int>((int)a.GetProperty(StandartProperties.ProccessCount));
            for (int i = 0; i < (int)a.GetProperty(StandartProperties.ProccessCount); i++)
            {
                pointsFromDifferentProcesses.Add(0);
            }
            foreach (var point in a.CalculatedPoints())
            {
                pointsFromDifferentProcesses[point.IdProcess]++;

            }
            foreach (var var in pointsFromDifferentProcesses)
            {
                Assert.AreNotEqual(0, var);
            }
        }
        [TestMethod]
        public void ScanningParalSmoke()
        {

            ISearchAlg a = AlgFactory.Build("Scanning_alg_paral");
            a.Problem = p;
            a.RecalcPoints();
            List<int> pointsFromDifferentProcesses = new List<int>((int)a.GetProperty(StandartProperties.ProccessCount));
            for (int i = 0; i < (int)a.GetProperty(StandartProperties.ProccessCount); i++)
            {
                pointsFromDifferentProcesses.Add(0);
            }
            foreach (var point in a.CalculatedPoints())
            {
                pointsFromDifferentProcesses[point.IdProcess]++;

            }
            foreach (var var in pointsFromDifferentProcesses)
            {
                Assert.AreNotEqual(0, var);
            }
        }
     }
}