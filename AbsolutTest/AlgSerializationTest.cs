﻿using Algorithms;
using Functions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AbsolutTest
{
    [TestClass]
    public class AlgSerializationTest
    {
        
       [TestMethod]
        public void ProblemToXML()
        {
            IProblem p = ProblemFactory.BuildNotConstrainedProblem(ProblemFactory.NotConstrainedOptions.Ackley1);
            ProblemFactory.ProblemToXML(p, "problem.xml");
        }

       [TestMethod]
        public void AlgToXML()
        {
            IProblem p = ProblemFactory.BuildNotConstrainedProblem(ProblemFactory.NotConstrainedOptions.Ackley1);
            ISearchAlg a = AlgFactory.Build("Agp");
            a.Problem = p;
            AlgFactory.SaveToXml(a, "method.xml");
            
        }
       

    }
}