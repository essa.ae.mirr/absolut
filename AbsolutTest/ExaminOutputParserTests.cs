﻿using System.Collections.Generic;
using Algorithms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AbsolutTest
{
    [TestClass()]
    public class ExaminOutputParserTests
    {
        [TestMethod()]
        public void ParseCoordinatesTest()
        {
            ExaminOutputParser parser = new ExaminOutputParser(2,1,2);
            var line = "-0.968506 -0.968506 | -4.48512 -4.26115 3.494";
            var methodPoint = parser.ParseExaminFileLine(line);
            var expected = new List<double> { -0.968506, -0.968506 };
            CollectionAssert.AreEqual(methodPoint.X, expected);
        }

        [TestMethod()]
        public void ParseConstraintsTest()
        {
            ExaminOutputParser parser = new ExaminOutputParser(2, 1,2);
            var line = "-0.968506 -0.968506 | -4.48512 -4.26115 3.494";
            var methodPoint = parser.ParseExaminFileLine(line);
            var expected = new List<double> { -4.48512, -4.26115 };
            CollectionAssert.AreEqual(methodPoint.Constraints, expected);
        }

        [TestMethod()]
        public void ParseCriterionsTest()
        {
            ExaminOutputParser parser = new ExaminOutputParser(2, 1,2);
            var line = "-0.968506 -0.968506 | -4.48512 -4.26115 3.494";
            var methodPoint = parser.ParseExaminFileLine(line);
            var expected = new List<double> { 3.494 };
            CollectionAssert.AreEqual(methodPoint.Criterions, expected);
        }

        [TestMethod()]
        public void ParseIncompleteConstraintsTest()
        {
            ExaminOutputParser parser = new ExaminOutputParser(2, 1,2);
            var line = "0.934814 0.990479 | 0.132043 ";
            var methodPoint = parser.ParseExaminFileLine(line);
            var expected = new List<double> { 0.132043 };
            CollectionAssert.AreEqual(methodPoint.Constraints, expected);
        }

        [TestMethod()]
        public void ParseLineWithoutCriterionsTest()
        {
            ExaminOutputParser parser = new ExaminOutputParser(2, 1,2);
            var line = "0.934814 0.990479 | 0.132043 ";
            var methodPoint = parser.ParseExaminFileLine(line);
            var expected = new List<double>();
            CollectionAssert.AreEqual(methodPoint.Criterions, expected);
        }

        [TestMethod()]
        public void ParseParetoFrontFileLineCriterionsTest()
        {
            ExaminOutputParser parser = new ExaminOutputParser(2, 3, 2);
            var line = "-4.935661 -4.676073 0.402398 -0.927002 -0.560791";
            var methodPoint = parser.ParseParetoFrontFileLine(line);
            var expected = new List<double>() { 0.402398, -0.927002, -0.560791 };
            CollectionAssert.AreEqual(methodPoint.Criterions, expected);
        }

        [TestMethod()]
        public void ParseParetoFrontFileLinePointTest()
        {
            ExaminOutputParser parser = new ExaminOutputParser(2, 1, 2);
            var line = "-4.935661 -4.676073 0.402398 -0.927002 -0.560791";
            var methodPoint = parser.ParseParetoFrontFileLine(line);
            var expected = new List<double>() { -4.935661, -4.676073 };
            CollectionAssert.AreEqual(methodPoint.X, expected);
        }





    }
}