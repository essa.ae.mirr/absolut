﻿namespace AbsolutTest
{
    using System;
    using System.IO;
    using System.Text;

    using Algorithms;

    using Functions;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class ProblemSmokes
    {
        private IProblem p = ProblemFactory.BuildStandartConstrainedProblem("RosenbrokCubeLine");
        [TestMethod]
        public void LeftCountEquals2()
        {
            var newFunction = p.Criterions[0];
            Assert.AreEqual(newFunction.Left.Count, 2);
        }
    }
}