﻿namespace AbsolutTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using Algorithms;
    using Algorithms.Methods;

    using Functions;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using org.moeaframework.core;

    using Executor = org.moeaframework.Executor;

    [TestClass()]
    public class MoeaTest
    {
        [TestMethod()]
        public void IsMoeaWorkingWithMoeaFunction()
        {
            ikvm.runtime.Startup.addBootClassPathAssemby(Assembly.Load("MOEAFramework-2.12"));
            NondominatedPopulation result =
                new Executor().withProblem("UF1").withAlgorithm("NSGAII").withMaxEvaluations(10000).run();
            List<Solution> paretoPoints = new List<Solution>();
            foreach (Solution res in result)
            {
                paretoPoints.Add(res);
            }
            Assert.AreNotEqual(1, paretoPoints.Count);
        }
    }
}