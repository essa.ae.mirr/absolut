﻿using System.Linq;
using Algorithms;
using Algorithms.Methods;
using Executors.Impl;
using Functions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AbsolutTest
{
    [TestClass]
    public class NoExceptionSmokes
    {
        private IProblem p = ProblemFactory.BuildNotConstrainedProblem("Parabaloid");
        [TestMethod]
        public void AgpSmoke()
        {
           ISearchAlg a = new Agp();
           a.Problem = p;
            a.RecalcPoints();
        }

        [TestMethod]
        public void ScanningSmoke()
        {
          ISearchAlg a = new ScanningAlg();
            a.Problem = p;
             a.RecalcPoints();
        }

        [TestMethod]
        public void PiavskyNoParalSmoke()
        {

            AlgExecutor.Create().WithAlgorithm(MethodsNames.Piyavsky).WithProblem(p).Execute();
        }
        [TestMethod]
        public void GASmoke()
        {
            ISearchAlg a = AlgFactory.Build("GeneticSimpleAlg");
            a.Problem = p;
             a.RecalcPoints();
        }
        [TestMethod]
        public void PiyavskySmoke()
        {

            ISearchAlg a = AlgFactory.Build("Piyavskiy");
            a.Problem = p;
             a.RecalcPoints();
        }

        [TestMethod]
        public void AgpParalSmoke()
        {
            ISearchAlg a = AlgFactory.Build("AGP_paral");
            a.Problem = p;
             a.RecalcPoints();
        }
        [TestMethod]
        public void ScanningParalSmoke()
        {

            ISearchAlg a = AlgFactory.Build("Scanning_alg_paral");
            a.Problem = p;
             a.RecalcPoints();
        }

      
        [TestMethod]
        public void DESmoke()
        {

            ISearchAlg a = AlgFactory.Build("DE");
            a.Problem = p;
             a.RecalcPoints();
        }

        [TestMethod]
        public void AlgLibSmoke()
        {

            ISearchAlg a = AlgFactory.Build(MethodsNames.Bleic);
            a.Problem = p;
             a.RecalcPoints();
        }

        [TestMethod]
        public void Nsga2Smoke()
        {
            ISearchAlg a = new MoeaAlg("NSGA2");
            a.Problem = p;
            a.RecalcPoints();
        }

        [TestMethod]
        public void AlglibNlcSmoke()
        {
            ISearchAlg a = new AlglibAul();
            a.Problem = ProblemFactory.BuildStandartConstrainedProblem("RosenbrokCubeLine");
             a.RecalcPoints();
        }
    }
}