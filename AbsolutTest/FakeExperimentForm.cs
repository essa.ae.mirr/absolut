﻿namespace AbsolutTest
{
    using System.Collections.Generic;

    using AbsolutUi;

    using Xceed.Wpf.AvalonDock.Layout;

    public class FakeExperimentForm : IFunctionalTabsView
    {
        public string WhatWasExecutedLast { get; set; } 

        public void AddFunctionalTab(LayoutDocument tab)
        {
            WhatWasExecutedLast = "Add Functional Tab";
        }

        public void ShowAllTabs(List<LayoutDocument> tabs)
        {
            WhatWasExecutedLast = "Show All Tabs";
        }

        public void HideAllTabs()
        {
            WhatWasExecutedLast = "Hide All Tabs";
        }

        public bool DoesAnchorableTabExist(string tabName)
        {
            WhatWasExecutedLast = "DoesAnchorableTabExist";
            return true;
        }

        public bool DoesFunctionalsTabExists(string convolutionMenuItem)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteTabs()
        {
            throw new System.NotImplementedException();
        }

        public void AddAnchorableTab(LayoutAnchorable tab)
        {
            WhatWasExecutedLast = "AddAnchorableTab";
        }
    }
}