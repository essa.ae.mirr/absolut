﻿using Algorithms;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Collections.Generic;

namespace Algorithms.Tests
{
    using Algorithms.Methods;

    using Functions;

    [TestClass()]
    public class ParetoFrontHelperTests
    {
        [TestMethod()]
        public void ApproximateParetoFrontTest()
        {
            ParetoFrontHelper.LazyApproximateParetoFront(new Benchmark1());
        }

        [TestMethod()]
        public void CalculateHVTest()
        {
            MethodPoint a = new MethodPoint();
            a.Criterions = new List<double>() { 5, 1 };
            MethodPoint b = new MethodPoint();
            b.Criterions = new List<double>() { 1, 5 };
            var approximatePareto = new List<MethodPoint> { a, b };

            MethodPoint referencePoint = new MethodPoint();
            referencePoint.Criterions = new List<double>() { 6, 6 };

            var actual = ParetoFrontHelper.CalculateHV(approximatePareto, referencePoint);
            var expected = 9;
            Assert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void CalculateDistanceIndexTest()
        {
            MethodPoint a = new MethodPoint();
            a.Criterions = new List<double>() { 5, 1 };
            MethodPoint b = new MethodPoint();
            b.Criterions = new List<double>() { 1, 5 };
            var approximatePareto = new List<MethodPoint> { a, b };

            var actual = ParetoFrontHelper.CalculateDistanceIndex(approximatePareto);
            var expected = 0;
            Assert.AreEqual(expected, actual);

        }


        [TestMethod()]
        public void CalculateDistanceIndexWithThreePointsTest()
        {
            MethodPoint a = new MethodPoint { Criterions = new List<double>() { 0, 0 } };
            MethodPoint b = new MethodPoint { Criterions = new List<double>() { 0, 3 } };
            MethodPoint c = new MethodPoint { Criterions = new List<double>() { 4, 0 } };

            var approximatePareto = new List<MethodPoint> { a, b, c };

            var actual = ParetoFrontHelper.CalculateDistanceIndex(approximatePareto);
            var d = 10.0 / 3.0;
            var du = (3 - d) * (3 - d) + (3 - d) * (3 - d) + (4 - d) * (4 - d);
            var expected = du;
            Assert.AreEqual(expected, actual);

        }

        [TestMethod()]
        public void ParetoFrontTest()
        {
            MethodPoint a = new MethodPoint { Criterions = new List<double>() { 2, 4 } };
            MethodPoint b = new MethodPoint { Criterions = new List<double>() { 4, 5 } };
            MethodPoint c = new MethodPoint { Criterions = new List<double>() { 5, 2 } };
            var set = new List<MethodPoint> { a, b, c };
            var actual = ParetoFrontHelper.ParetoFront(set);
            var expected = new List<MethodPoint>() { a, c };
            CollectionAssert.AreEqual(expected, actual);
        }


        [TestMethod()]
        public void ParetoFrontOfFourPointsTest()
        {
            MethodPoint a = new MethodPoint { Criterions = new List<double>() { 2, 4 } };
            MethodPoint b = new MethodPoint { Criterions = new List<double>() { 4, 5 } };
            MethodPoint c = new MethodPoint { Criterions = new List<double>() { 5, 2 } };
            MethodPoint d = new MethodPoint { Criterions = new List<double>() { 3, 3 } };
            var set = new List<MethodPoint> { a, b, c, d };
            var actual = ParetoFrontHelper.ParetoFront(set);
            var expected = new List<MethodPoint>() { a, c, d };
            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod()]
        public void LazyPointsSetForParetoFrontLowerPointExistsTest()
        {
            IProblem p = new Benchmark1();
            MethodPoint a = new MethodPoint() { X = p.LowerBound, Criterions = p.CalculateCriterions(p.LowerBound) };
            List<MethodPoint> lazyPointsSetForParetoFront = ParetoFrontHelper.LazyPointsSetForParetoFront(p);
            MethodPoint b = lazyPointsSetForParetoFront[0];
            CollectionAssert.AreEqual(a.Criterions, b.Criterions);
            CollectionAssert.AreEqual(a.X, b.X);
        }

        [TestMethod()]
        public void LazyPointsSetForParetoFrontUpperPointExistsTest()
        {
            IProblem p = new Benchmark1();
            MethodPoint a = new MethodPoint() { X = p.UpperBound, Criterions = p.CalculateCriterions(p.UpperBound) };
            List<MethodPoint> lazyPointsSetForParetoFront = ParetoFrontHelper.LazyPointsSetForParetoFront(p);
            MethodPoint b = lazyPointsSetForParetoFront[lazyPointsSetForParetoFront.Count - 1];
            CollectionAssert.AreEqual(a.Criterions, b.Criterions);
            CollectionAssert.AreEqual(a.X, b.X);
        }



    }
}